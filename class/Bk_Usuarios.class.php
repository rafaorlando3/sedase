<?php

include_once 'BD.class.php';

class Bk_Usuarios {

    private $id;
    private $nome;
    private $email;
    private $senha;
    private $data;
    private $tipo;
    private $cidade;
    private $estado;
    private $imagem;
    private $genero;
    private $ativo;
    private $bd; //conexão com o banco
    private $tabela; //nome da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "bk_usuarios";
    }

    public function __destruct() {
        unset($this->bd);
    }

    public function __get($key) {
        return $this->$key;
    }

    public function __set($key, $value) {
        $this->$key = $value;
    }

    public function inserir() {

        $sql = "INSERT INTO $this->tabela (nome,email,senha,data,tipo,cidade,estado,imagem, genero,ativo) "
                . "values ('$this->nome','$this->email','$this->senha','$this->data',"
                . "$this->tipo,'$this->cidade','$this->estado','$this->imagem','$this->genero','$this->ativo')";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function listar($complemento="") {
        $sql = "SELECT * FROM $this->tabela " .
                $complemento;
        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {

            $obj = new Bk_Usuarios();
            $obj->id = $reg["id"];
            $obj->nome = $reg["nome"];
            $obj->email = $reg["email"];
            $obj->senha = $reg["senha"];
            $obj->data = $reg["data"];
            $obj->tipo = $reg["tipo"];
            $obj->cidade = $reg["cidade"];
            $obj->estado = $reg["estado"];
            $obj->imagem = $reg["imagem"];
            $obj->genero = $reg["genero"];
            $obj->ativo = $reg["ativo"];
            $retorno[] = $obj;
        }
        return $retorno;
    }

    public function excluir() {

        $sql = "delete from $this->tabela where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function atualizar() {
        $retorno = false;
        $sql = "update $this->tabela set
                     nome='$this->nome',email='$this->email',senha='$this->senha',"
                . "data='$this->data',tipo=$this->tipo,cidade='$this->cidade',estado='$this->estado',imagem='$this->imagem',"
                . "genero='$this->genero', ativo='$this->ativo' where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function retornarunico() {
        $sql = "Select * FROM $this->tabela where id=$this->id";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Bk_Usuarios();
            $obj->id = $reg["id"];
            $obj->nome = $reg["nome"];
            $obj->email = $reg["email"];
            $obj->senha = $reg["senha"];
            $obj->data = $reg["data"];
            $obj->tipo = $reg["tipo"];
            $obj->cidade = $reg["cidade"];
            $obj->estado = $reg["estado"];
            $obj->imagem = $reg["imagem"];
            $obj->genero = $reg["genero"];
            $obj->ativo = $reg["ativo"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }

}
