<?php

include_once 'BD.class.php';

class Enquetes {

    private $id;
    private $titulo;
    private $data;
    private $genero;
    private $idusuario;
    private $tipo;
    private $status;
    private $idturma;
    private $bd; //conexão com o banco
    private $tabela; //titulo da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "enquetes";
    }

    public function __destruct() {
        unset($this->bd);
    }

    public function __get($key) {
        return $this->$key;
    }

    public function __set($key, $value) {
        $this->$key = $value;
    }

    public function inserir() {

        $sql = "INSERT INTO $this->tabela (titulo,data,idusuario,genero,tipo,status, idturma) "
                . "values ('$this->titulo','$this->data',$this->idusuario,'$this->genero',$this->tipo,$this->status,$this->idturma)";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function listar($complemento = "") {
        $sql = "SELECT * FROM $this->tabela " .
                $complemento;

        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {

            $obj = new Enquetes();
            $obj->id = $reg["id"];
            $obj->titulo = $reg["titulo"];
            $obj->data = $reg["data"];
            $obj->idusuario = $reg["idusuario"];
            $obj->genero = $reg["genero"];
            $obj->tipo = $reg["tipo"];
            $obj->status = $reg["status"];
            $obj->idturma = $reg["idturma"];
            $retorno[] = $obj;
        }
        return $retorno;
    }

    public function excluir() {

        $sql = "delete from $this->tabela where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function atualizar() {
        $retorno = false;
        $sql = "update $this->tabela set
                     titulo='$this->titulo',data='$this->data',idusuario=$this->idusuario,genero='$this->genero',tipo=$this->tipo,status=$this->status,idturma=$this->idturma where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function retornarunico() {
        $sql = "Select * FROM $this->tabela where id=$this->id";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Enquetes();
            $obj->id = $reg["id"];
            $obj->titulo = $reg["titulo"];
            $obj->data = $reg["data"];
            $obj->idusuario = $reg["idusuario"];
            $obj->genero = $reg["genero"];
            $obj->tipo = $reg["tipo"];
            $obj->status = $reg["status"];
            $obj->idturma = $reg["idturma"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }

}
