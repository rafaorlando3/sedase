<?php

include_once 'BD.class.php';

class Pservicos {

    private $id;
    private $titulo;
    private $texto;
    private $video;
    private $anexo;
    private $idusuario;
    private $categoria;
    private $imagem;
    private $bd; //conexão com o banco
    private $tabela; //titulo da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "pservicos";
    }

    public function __destruct() {
        unset($this->bd);
    }

    public function __get($key) {
        return $this->$key;
    }

    public function __set($key, $value) {
        $this->$key = $value;
    }

    public function inserir() {

        $sql = "INSERT INTO $this->tabela (titulo,texto,video,anexo,idusuario,categoria,imagem) "
                . "values ('$this->titulo','$this->texto','$this->video','$this->anexo',$this->idusuario,"
                . "$this->categoria,'$this->imagem')";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function listar($complemento = "") {
        $sql = "SELECT * FROM $this->tabela " .
                $complemento;

        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {

            $obj = new Pservicos();
            $obj->id = $reg["id"];
            $obj->titulo = $reg["titulo"];
            $obj->texto = $reg["texto"];
            $obj->video = $reg["video"];
            $obj->anexo = $reg["anexo"];
            $obj->idusuario = $reg["idusuario"];
            $obj->categoria = $reg["categoria"];
            $obj->imagem = $reg["imagem"];
            $retorno[] = $obj;
        }
        return $retorno;
    }

    public function excluir() {

        $sql = "delete from $this->tabela where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function atualizar() {
        $retorno = false;
        $sql = "update $this->tabela set 
                     titulo='$this->titulo',texto='$this->texto',video='$this->video',anexo='$this->anexo',"
                . "idusuario=$this->idusuario,categoria=$this->categoria,imagem='$this->imagem' where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function retornarunico() {
        $sql = "Select * FROM $this->tabela where id=$this->id";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Pservicos();
            $obj->id = $reg["id"];
            $obj->titulo = $reg["titulo"];
            $obj->texto = $reg["texto"];
            $obj->video = $reg["video"];
            $obj->anexo = $reg["anexo"];
            $obj->idusuario = $reg["idusuario"];
            $obj->categoria = $reg["categoria"];
            $obj->imagem = $reg["imagem"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }

}
