<?php

include_once 'BD.class.php';

class Turma {

    private $id;
    private $nome;
    private $grau;
    private $ano;
    private $idescola;
    private $idusuario;
    private $bd; //conexão com o banco
    private $tabela; //nome da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "turma";
    }

    public function __destruct() {
        unset($this->bd);
    }

    public function __get($key) {
        return $this->$key;
    }

    public function __set($key, $value) {
        $this->$key = $value;
    }

    public function inserir() {

        $sql = "INSERT INTO $this->tabela (nome,grau,idusuario,ano,idescola) "
                . "values ('$this->nome',$this->grau,$this->idusuario,$this->ano,$this->idescola)";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function listar($complemento = "") {
        $sql = "SELECT * FROM $this->tabela " .
                $complemento;

        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {

            $obj = new Turma();
            $obj->id = $reg["id"];
            $obj->nome = $reg["nome"];
            $obj->grau = $reg["grau"];
            $obj->idusuario = $reg["idusuario"];
            $obj->idescola = $reg["idescola"];
            $obj->ano = $reg["ano"];
            $retorno[] = $obj;
        }
        return $retorno;
    }

    public function excluir() {

        $sql = "delete from $this->tabela where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function atualizar() {
        $retorno = false;
        $sql = "update $this->tabela set
                     nome='$this->nome',grau=$this->grau,idusuario=$this->idusuario,ano=$this->ano,idescola=$this->idescola where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function retornarunico() {
        $sql = "Select * FROM $this->tabela where id=$this->id";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Turma();
            $obj->id = $reg["id"];
            $obj->nome = $reg["nome"];
            $obj->grau = $reg["grau"];
            $obj->idusuario = $reg["idusuario"];
            $obj->idescola = $reg["idescola"];
            $obj->ano = $reg["ano"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }

}
