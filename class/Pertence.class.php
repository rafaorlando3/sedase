<?php

include_once 'BD.class.php';

class Pertence {

    private $id;
    private $idturma;
    private $idusuario;
    private $bd; //conexão com o banco
    private $tabela; //idturma da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "pertence";
    }

    public function __destruct() {
        unset($this->bd);
    }

    public function __get($key) {
        return $this->$key;
    }

    public function __set($key, $value) {
        $this->$key = $value;
    }

    public function inserir() {

        $sql = "INSERT INTO $this->tabela (idturma,idusuario) "
                . "values ($this->idturma,$this->idusuario)";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function listar($complemento = "") {
        $sql = "SELECT * FROM $this->tabela " .
                $complemento;

        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {

            $obj = new Pertence();
            $obj->id = $reg["id"];
            $obj->idturma = $reg["idturma"];
            $obj->idusuario = $reg["idusuario"];
            $retorno[] = $obj;
        }
        return $retorno;
    }

    public function excluir() {

        $sql = "delete from $this->tabela where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function atualizar() {
        $retorno = false;
        $sql = "update $this->tabela set
                     idturma=$this->idturma,idusuario=$this->idusuario where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function retornarunico() {
        $sql = "Select * FROM $this->tabela where idturma=$this->idturma";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Pertence();
            $obj->id = $reg["id"];
            $obj->idturma = $reg["idturma"];
            $obj->idusuario = $reg["idusuario"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }
    
    public function retornarunicoAluno() {
        $sql = "Select * FROM $this->tabela where idusuario=$this->idusuario";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Pertence();
            $obj->id = $reg["id"];
            $obj->idturma = $reg["idturma"];
            $obj->idusuario = $reg["idusuario"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }

}
    

