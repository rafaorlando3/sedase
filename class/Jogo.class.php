<?php

include_once 'BD.class.php';

class Jogo {

    private $id;
    private $data;
    private $pontucao;
    private $idusuario;
    private $bd; //conexão com o banco
    private $tabela; //data da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "jogo";
    }

    public function __destruct() {
        unset($this->bd);
    }

    public function __get($key) {
        return $this->$key;
    }

    public function __set($key, $value) {
        $this->$key = $value;
    }

    public function inserir() {

        $sql = "INSERT INTO $this->tabela (data,idusuario,pontucao) "
                . "values ('$this->data',$this->idusuario,$this->pontucao)";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function listar($complemento = "") {
        $sql = "SELECT * FROM $this->tabela " .
                $complemento;

        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {

            $obj = new Jogo();
            $obj->id = $reg["id"];
            $obj->data = $reg["data"];
            $obj->pontucao = $reg["pontucao"];
            $obj->idusuario = $reg["idusuario"];
            $retorno[] = $obj;
        }
        return $retorno;
    }

    public function excluir() {

        $sql = "delete from $this->tabela where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function atualizar() {
        $retorno = false;
        $sql = "update $this->tabela set 
                     data='$this->data',idusuario=$this->idusuario,pontucao=$this->pontucao where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function retornarunico() {
        $sql = "Select * FROM $this->tabela where id=$this->id";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Jogo();
            $obj->id = $reg["id"];
            $obj->data = $reg["data"];
            $obj->idusuario = $reg["idusuario"];
            $obj->pontucao = $reg["pontucao"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }

}
