<?php

include_once 'BD.class.php';

class Escola {

    private $id;
    private $nome;
    private $codigo;
    private $endereco;
    private $telefone;
    private $estado;
    private $cidade;
    private $email;
    private $bd; //conexão com o banco
    private $tabela; //nome da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "escola";
    }

    public function __destruct() {
        unset($this->bd);
    }

    public function __get($key) {
        return $this->$key;
    }

    public function __set($key, $value) {
        $this->$key = $value;
    }

    public function inserir() {

        $sql = "INSERT INTO $this->tabela (id,nome,codigo,telefone,endereco,estado,cidade,email) "
                . "values ($this->id,'$this->nome','$this->codigo','$this->telefone','$this->endereco','$this->estado','$this->cidade','$this->email')";
       
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function listar($complemento = "") {
        $sql = "SELECT * FROM $this->tabela " .
                $complemento;

        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {

            $obj = new Escola();
            $obj->id = $reg["id"];
            $obj->nome = $reg["nome"];
            $obj->codigo = $reg["codigo"];
            $obj->telefone = $reg["telefone"];
            $obj->endereco = $reg["endereco"];
            $obj->estado = $reg["estado"];
            $obj->cidade = $reg["cidade"];
            $obj->email = $reg["email"];
            $retorno[] = $obj;
        }
        return $retorno;
    }

    public function excluir() {

        $sql = "delete from $this->tabela where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function atualizar() {
        $retorno = false;
        $sql = "update $this->tabela set
                     nome='$this->nome',email='$this->email',codigo='$this->codigo',telefone='$this->telefone',endereco='$this->endereco',estado='$this->estado',cidade='$this->cidade' where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function retornarunico() {
        $sql = "Select * FROM $this->tabela where id=$this->id";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Escola();
            $obj->id = $reg["id"];
            $obj->nome = $reg["nome"];
            $obj->codigo = $reg["codigo"];
            $obj->telefone = $reg["telefone"];
            $obj->endereco = $reg["endereco"];
            $obj->estado = $reg["estado"];
            $obj->cidade = $reg["cidade"];
            $obj->email = $reg["email"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }

}
