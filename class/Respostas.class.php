<?php

include_once 'BD.class.php';

class Respostas {

    private $id;
    private $melhoresposta;
    private $texto;
    private $citacao;
    private $idforum;
    private $idusuario;
    private $data;
    private $bd; //conexão com o banco
    private $tabela; //melhoresposta da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "respostas";
    }

    public function __destruct() {
        unset($this->bd);
    }

    public function __get($key) {
        return $this->$key;
    }

    public function __set($key, $value) {
        $this->$key = $value;
    }

    public function inserir() {

        $sql = "INSERT INTO $this->tabela (melhoresposta,texto,idforum,data,idusuario) "
                . "values ('$this->melhoresposta','$this->texto',$this->idforum,'$this->data',$this->idusuario)";
        $retorno = pg_query($sql);
        return $retorno;
    }
    
    public function inserir2() {

        $sql = "INSERT INTO $this->tabela (melhoresposta,texto,idforum,data,idusuario,citacao) "
                . "values ('$this->melhoresposta','$this->texto',$this->idforum,'$this->data',$this->idusuario,$this->citacao)";
        $retorno = pg_query($sql);
 
        return $retorno;
    }

    public function listar($complemento = "") {
        $sql = "SELECT * FROM $this->tabela " .
                $complemento;
        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {

            $obj = new Respostas();
            $obj->id = $reg["id"];
            $obj->melhoresposta = $reg["melhoresposta"];
            $obj->texto = $reg["texto"];
            $obj->idforum = $reg["idforum"];
            $obj->idusuario = $reg["idusuario"];
            $obj->citacao = $reg["citacao"];
            $obj->data = $reg["data"];
            $retorno[] = $obj;
        }
        return $retorno;
    }

    public function excluir() {

        $sql = "delete from $this->tabela where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function atualizar() {
        $retorno = false;
        $sql = "update $this->tabela set 
                     melhoresposta='$this->melhoresposta',data='$this->data',texto='$this->texto',idforum=$this->idforum,idusuario=$this->idusuario,citacao=$this->citacao where id=$this->id ";
        $retorno = pg_query($sql);
        return $retorno;
    }

    
     public function atualizar2() {
        $retorno = false;
        $sql = "update $this->tabela set 
                     melhoresposta='$this->melhoresposta',data='$this->data',texto='$this->texto',idforum=$this->idforum,idusuario=$this->idusuario where id=$this->id ";
        $retorno = pg_query($sql);
        return $retorno;
    }
    public function retornarunico() {
        $sql = "Select * FROM $this->tabela where id=$this->id";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Respostas();
            $obj->id = $reg["id"];
            $obj->melhoresposta = $reg["melhoresposta"];
            $obj->texto = $reg["texto"];
            $obj->idforum = $reg["idforum"];
            $obj->idusuario = $reg["idusuario"];
            $obj->citacao = $reg["citacao"];
            $obj->data = $reg["data"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }
    
     
    public function retornaUltimo() {
        $sql = "select max(id) FROM $this->tabela";
        $resultado = pg_query($sql);
        $retorno = pg_fetch_row($resultado);
        $ultimo=$retorno[0];

        return $ultimo;
 
    }
    
    public function retornatotal($idforum) {
        $sql = "select count(id) from $this->tabela where idforum=$idforum ";
        $resultado = pg_query($sql);
        $retorno = pg_fetch_row($resultado);
        $ultimo=$retorno[0];

        return $ultimo;
 
    }

}