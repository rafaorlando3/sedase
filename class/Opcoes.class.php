<?php

include_once 'BD.class.php';

class Opcoes {

    private $id;
    private $texto;
    private $idperguntas;
    private $bd; //conexão com o banco
    private $tabela; //texto da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "opcoes";
    }

    public function __destruct() {
        unset($this->bd);
    }

    public function __get($key) {
        return $this->$key;
    }

    public function __set($key, $value) {
        $this->$key = $value;
    }

    public function inserir() {

        $sql = "INSERT INTO $this->tabela (texto,idperguntas) "
                . "values ('$this->texto',$this->idperguntas)";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function listar($complemento = "") {
        $sql = "SELECT * FROM $this->tabela " .
                $complemento;

        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {

            $obj = new Opcoes();
            $obj->id = $reg["id"];
            $obj->texto = $reg["texto"];
            $obj->idperguntas = $reg["idperguntas"];
            $retorno[] = $obj;
        }
        return $retorno;
    }

    public function excluir() {

        $sql = "delete from $this->tabela where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function atualizar() {
        $retorno = false;
        $sql = "update $this->tabela set
                     texto='$this->texto',idperguntas=$this->idperguntas where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function retornarunico() {
        $sql = "Select * FROM $this->tabela where id=$this->id";
        
        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Opcoes();
            $obj->id = $reg["id"];
            $obj->texto = $reg["texto"];
            $obj->idperguntas = $reg["idperguntas"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }
    
    public function retornarunicoP() {
        $sql = "Select * FROM $this->tabela where idperguntas=$this->idperguntas";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Opcoes();
            $obj->id = $reg["id"];
            $obj->texto = $reg["texto"];
            $obj->idperguntas = $reg["idperguntas"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }

}
