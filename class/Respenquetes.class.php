<?php

include_once 'BD.class.php';

class Respenquetes {

    private $id;
    private $idenquete;
    private $idpergunta;
    private $idopcao;
    private $idusuario;
    private $escolha;
    private $cont;
    private $bd; //conexão com o banco
    private $tabela; //idenquete da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "respenquete";
    }

    public function __destruct() {
        unset($this->bd);
    }

    public function __get($key) {
        return $this->$key;
    }

    public function __set($key, $value) {
        $this->$key = $value;
    }

    public function inserir() {

        $sql = "INSERT INTO $this->tabela (idenquete,idpergunta,idusuario,idopcao,escolha) "
                . "values ($this->idenquete,$this->idpergunta,$this->idusuario,$this->idopcao,'$this->escolha')";
        $retorno = pg_query($sql);
        
        return $retorno;
    }
    

    public function listar($complemento = "") {
        $sql = "SELECT * FROM $this->tabela " .
                $complemento;

        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {

            $obj = new Respenquetes();
            $obj->id = $reg["id"];
            $obj->idenquete = $reg["idenquete"];
            $obj->idpergunta = $reg["idpergunta"];
            $obj->idusuario = $reg["idusuario"];
            $obj->idopcao = $reg["idopcao"];
            $obj->escolha = $reg["escolha"];
            $retorno[] = $obj;
        }
        return $retorno;
    }

    public function listar2($complemento = "") {
        $sql = "SELECT idpergunta FROM $this->tabela $complemento GROUP BY idpergunta";

        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {
            $obj = new Respenquetes();
            $obj->idpergunta = $reg["idpergunta"];
            $retorno[] = $obj;
        }
        return $retorno;
    }
    
    public function listaropcao($complemento = "") {
        $sql = "SELECT idopcao, count(idopcao) FROM $this->tabela $complemento";

        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {
            $obj = new Respenquetes();
            $obj->idopcao = $reg["idopcao"];
            $obj->cont =$reg["count"];
            $retorno[] = $obj;
        }
        return $retorno;
    }
    
     
    
    public function excluir() {

        $sql = "delete from $this->tabela where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function atualizar() {
        $retorno = false;
        $sql = "update $this->tabela set 
                     idenquete=$this->idenquete,idpergunta=$this->idpergunta,idusuario=$this->idusuario,idopcao=$this->idopcao,escolha=$this->escolha where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function retornarunico() {
        $sql = "Select * FROM $this->tabela where id=$this->id";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Respenquetes();
            $obj->id = $reg["id"];
            $obj->idenquete = $reg["idenquete"];
            $obj->idpergunta = $reg["idpergunta"];
            $obj->idusuario = $reg["idusuario"];
            $obj->idopcao = $reg["idopcao"];
            $obj->escolha = $reg["escolha"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }

}
