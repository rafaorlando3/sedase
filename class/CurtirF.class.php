<?php

include_once 'BD.class.php';

class CurtirF {

    private $id;
    private $contagem;
    private $curtiu;
    private $data;
    private $idforum;
    private $idusuario;
    private $bd; //conexão com o banco
    private $tabela; //curtirf da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "curtirf";
    }

    public function __destruct() {
        unset($this->bd);
    }

    public function __get($key) {
        return $this->$key;
    }

    public function __set($key, $value) {
        $this->$key = $value;
    }

    public function inserir() {

        $sql = "INSERT INTO $this->tabela (contagem,idforum,idusuario,curtiu,data) values ($this->contagem,$this->idforum,$this->idusuario,'$this->curtiu','$this->data')";
        $retorno = pg_query($sql);
        return $retorno;
    }


    public function listar($complemento = "") {
        $sql = "SELECT * FROM $this->tabela " .
                $complemento;

        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {

            $obj = new CurtirF();
            $obj->id = $reg["id"];
            $obj->contagem = $reg["contagem"];
            $obj->curtiu = $reg["curtiu"];
            $obj->idforum = $reg["idforum"];
            $obj->data = $reg["data"];
            $obj->idusuario = $reg["idusuario"];
            $retorno[] = $obj;
        }
        return $retorno;
    }

    public function excluir() {

        $sql = "delete from $this->tabela where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function atualizar() {
        $retorno = false;
        $sql = "update $this->tabela set
                     contagem=$this->contagem,curtiu='$this->curtiu',idusuario=$this->idusuario,idforum=$this->idforum, data='$this->data' where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function retornarunico() {
        $sql = "Select * FROM $this->tabela where idforum=$this->idforum";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new CurtirF();
            $obj->id = $reg["id"];
            $obj->contagem = $reg["contagem"];
            $obj->curtiu = $reg["curtiu"];
            $obj->idforum = $reg["idforum"];
            $obj->data = $reg["data"];
            $obj->idusuario = $reg["idusuario"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }

}
