<?php

include_once 'BD.class.php';

class Perguntas {

    private $id;
    private $titulo;
    private $idenquetes;
    private $bd; //conexão com o banco
    private $tabela; //titulo da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "perguntas";
    }

    public function __destruct() {
        unset($this->bd);
    }

    public function __get($key) {
        return $this->$key;
    }

    public function __set($key, $value) {
        $this->$key = $value;
    }

    public function inserir() {

        $sql = "INSERT INTO $this->tabela (titulo,idenquetes) "
                . "values ('$this->titulo',$this->idenquetes)";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function listar($complemento = "") {
        $sql = "SELECT * FROM $this->tabela " .
                $complemento;

        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {

            $obj = new Perguntas();
            $obj->id = $reg["id"];
            $obj->titulo = $reg["titulo"];
            $obj->idenquetes = $reg["idenquetes"];
            $retorno[] = $obj;
        }
        return $retorno;
    }
    
    public function excluir() {

        $sql = "delete from $this->tabela where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function atualizar() {
        $retorno = false;
        $sql = "update $this->tabela set 
                     titulo='$this->titulo',idenquetes=$this->idenquetes where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function retornarunico() {
        $sql = "Select * FROM $this->tabela where id=$this->id";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Perguntas();
            $obj->id = $reg["id"];
            $obj->titulo = $reg["titulo"];
            $obj->idenquetes = $reg["idenquetes"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }

}
