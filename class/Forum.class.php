<?php

include_once 'BD.class.php';

class Forum {

    private $id;
    private $titulo;
    private $texto;
    private $imagem;
    private $idusuario;
    private $resolvido;
    private $idturma;
    private $data;
    private $tipo;
    private $bd; //conexão com o banco
    private $tabela; //titulo da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "forum";
    }

    public function __destruct() {
        unset($this->bd);
    }

    public function __get($key) {
        return $this->$key;
    }

    public function __set($key, $value) {
        $this->$key = $value;
    }

    public function inserir() {

        $sql = "INSERT INTO $this->tabela (titulo,texto,idusuario,imagem,resolvido,idturma,data,tipo) "
                . "values ('$this->titulo','$this->texto',$this->idusuario,'$this->imagem','$this->resolvido',$this->idturma,'$this->data',$this->tipo)";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function listar($complemento = "") {
        $sql = "SELECT * FROM $this->tabela " .
                $complemento;
        
        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {

            $obj = new Forum();
            $obj->id = $reg["id"];
            $obj->titulo = $reg["titulo"];
            $obj->texto = $reg["texto"];
            $obj->idusuario = $reg["idusuario"];
            $obj->imagem = $reg["imagem"];
            $obj->resolvido = $reg["resolvido"];
            $obj->idturma = $reg["idturma"];
            $obj->data = $reg["data"];
            $obj->tipo = $reg["tipo"];
            $retorno[] = $obj;
        }
        return $retorno;
    }

    public function excluir() {

        $sql = "delete from $this->tabela where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function atualizar() {
        $retorno = false;
        $sql = "update $this->tabela set 
                     titulo='$this->titulo',data='$this->data',tipo=$this->tipo,texto='$this->texto',idusuario=$this->idusuario,imagem='$this->imagem',idturma=$this->idturma,resolvido='$this->resolvido' where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function retornarunico() {
        $sql = "Select * FROM $this->tabela where id=$this->id";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Forum();
            $obj->id = $reg["id"];
            $obj->titulo = $reg["titulo"];
            $obj->texto = $reg["texto"];
            $obj->idusuario = $reg["idusuario"];
            $obj->imagem = $reg["imagem"];
            $obj->resolvido = $reg["resolvido"];
            $obj->idturma = $reg["idturma"];
            $obj->data = $reg["data"];
            $obj->tipo = $reg["tipo"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }

}
