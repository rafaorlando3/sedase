<?php

include_once 'BD.class.php';

class Noticias {

    private $id;
    private $titulo;
    private $tags;
    private $conteudo;
    private $video;
    private $data;
    private $publicado;
    private $idusuario;
    private $imagem;
    private $bd; //conexão com o banco
    private $tabela; //titulo da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "noticias";
    }

    public function __destruct() {
        unset($this->bd);
    }

    public function __get($key) {
        return $this->$key;
    }

    public function __set($key, $value) {
        $this->$key = $value;
    }

    public function inserir() {

        $sql = "INSERT INTO $this->tabela (titulo,tags,conteudo,video,data,publicado,idusuario,imagem) "
                . "values ('$this->titulo','$this->tags','$this->conteudo','$this->video','$this->data',"
                . "$this->publicado,$this->idusuario,'$this->imagem')";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function listar($complemento = "") {
        $sql = "SELECT * FROM $this->tabela " .
                $complemento;

        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {

            $obj = new Noticias();
            $obj->id = $reg["id"];
            $obj->titulo = $reg["titulo"];
            $obj->tags = $reg["tags"];
            $obj->conteudo = $reg["conteudo"];
            $obj->video = $reg["video"];
            $obj->data = $reg["data"];
            $obj->publicado = $reg["publicado"];
            $obj->idusuario = $reg["idusuario"];
            $obj->imagem = $reg["imagem"];
            $retorno[] = $obj;
        }
        return $retorno;
    }

    public function excluir() {

        $sql = "delete from $this->tabela where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function atualizar() {
        $retorno = false;
        $sql = "update $this->tabela set
                     titulo='$this->titulo',tags='$this->tags',conteudo='$this->conteudo',video='$this->video',"
                . "data='$this->data',publicado=$this->publicado,idusuario=$this->idusuario,imagem='$this->imagem' where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function retornarunico() {
        $sql = "Select * FROM $this->tabela where id=$this->id";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Noticias();
            $obj->id = $reg["id"];
            $obj->titulo = $reg["titulo"];
            $obj->tags = $reg["tags"];
            $obj->conteudo = $reg["conteudo"];
            $obj->video = $reg["video"];
            $obj->data = $reg["data"];
            $obj->publicado = $reg["publicado"];
            $obj->idusuario = $reg["idusuario"];
            $obj->imagem = $reg["imagem"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }

}
