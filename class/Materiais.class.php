<?php

include_once 'BD.class.php';

class Materiais {

    private $id;
    private $titulo;
    private $descricao;
    private $anexo;
    private $idusuario;
    private $idturma;
    private $video;
    private $imagem;
    private $tipo;
    private $bd; //conexão com o banco
    private $tabela; //titulo da tabela

    public function __construct() {
        $this->bd = new BD();
        $this->tabela = "materiais";
    }

    public function __destruct() {
        unset($this->bd);
    }

    public function __get($key) {
        return $this->$key;
    }

    public function __set($key, $value) {
        $this->$key = $value;
    }

    public function inserir() {

        $sql = "INSERT INTO $this->tabela (titulo,descricao,idusuario,anexo,imagem,idturma,video,tipo) "
                . "values ('$this->titulo','$this->descricao',$this->idusuario,'$this->anexo','$this->imagem',$this->idturma,'$this->video',$this->tipo)";
        $retorno = pg_query($sql);
        //echo $sql;
        return $retorno;
    }

    public function listar($complemento = "") {
        $sql = "SELECT * FROM $this->tabela " .
                $complemento;
        $resultado = pg_query($sql);
        $retorno = NULL;

        while ($reg = pg_fetch_assoc($resultado)) {

            $obj = new Materiais();
            $obj->id = $reg["id"];
            $obj->titulo = $reg["titulo"];
            $obj->descricao = $reg["descricao"];
            $obj->idusuario = $reg["idusuario"];
            $obj->idturma = $reg["idturma"];
            $obj->video = $reg["video"];
            $obj->anexo = $reg["anexo"];
            $obj->imagem = $reg["imagem"];
            $obj->tipo = $reg["tipo"];
            $retorno[] = $obj;
        }
        return $retorno;
    }

    public function excluir() {

        $sql = "delete from $this->tabela where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function atualizar() {
        $retorno = false;
        $sql = "update $this->tabela set 
                     titulo='$this->titulo',descricao='$this->descricao',tipo=$this->tipo,idusuario=$this->idusuario,idturma=$this->idturma,video='$this->video',anexo='$this->anexo',imagem='$this->imagem' where id=$this->id";
        $retorno = pg_query($sql);
        return $retorno;
    }

    public function retornarunico() {
        $sql = "Select * FROM $this->tabela where id=$this->id";

        $resultado = pg_query($sql);
        $retorno = NULL;

        $reg = pg_fetch_assoc($resultado);
        if ($reg == true) {
            $obj = new Materiais();
            $obj->id = $reg["id"];
            $obj->titulo = $reg["titulo"];
            $obj->descricao = $reg["descricao"];
            $obj->idusuario = $reg["idusuario"];
            $obj->idturma = $reg["idturma"];
            $obj->video = $reg["video"];
            $obj->anexo = $reg["anexo"];
            $obj->imagem = $reg["imagem"];
            $obj->tipo = $reg["tipo"];
            $retorno = $obj;
        } else {
            $retorno = null;
        }

        return $retorno;
    }

}
