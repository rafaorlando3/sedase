<?php
include_once 'cabecalho.php';

if (isset($_SESSION['dino'])) {
    $idedit = $_SESSION['dino'];
    $objEnquetes = new Enquetes();
    $objEnquetes->id = $idedit;
    $itemenquete = $objEnquetes->retornarunico();

    $objUsuarios = new Usuarios();
    $objUsuarios->id = $itemenquete->idusuario;
    $itemuser = $objUsuarios->retornarunico();
}


if ($_POST['dino']) {
    $objEnquetes = new Enquetes();
    $objEnquetes->id = $_POST['dino'];
    $itemenquete = $objEnquetes->retornarunico();
    session_start();
    $_SESSION['dino'] = $_POST['dino'];

    $objUsuarios = new Usuarios();
    $objUsuarios->id = $itemenquete->idusuario;
    $itemuser = $objUsuarios->retornarunico();
} else {

    header("Location:enquetes.php");
}
?>

<div class="divider"></div>

<div class="content">
    <div class="container">

        <div class="main-content">
            <h1>Enquete: <?= $itemenquete->titulo ?></h1>
            <section class="posts-con">
<?php ?>

                <article>
                    <div class="current-date">
                        <p>Público!</p>
                        <p class="date"></p>
                    </div>
                    <div class="info">


                        <form id="cadastroRE">

<?php
$objPerguntas = new Perguntas();
$lista = $objPerguntas->listar("where idenquetes=" . $itemenquete->id . "");
if ($lista != null) {
    $mensagem = "";
    $i = 1;
    $p = 1;
    foreach ($lista as $itempergunta) {
        ?>
                                    <p>

                                    <h3><?= $i . " - " . $itempergunta->titulo ?></h3>
                                    <input type="hidden" value="<?= $itempergunta->id ?>" name="idpergunta<?= $p ?>"/>

                                    <blockquote>

        <?php
        $objOpcoes = new Opcoes();
        $listaopcoes = $objOpcoes->listar("where idperguntas=" . $itempergunta->id . " order by texto ASC");
        if ($lista != null) {
            foreach ($listaopcoes as $itemopcao) {
                ?>




                                                <input type="radio" value="<?= $itemopcao->id ?>" name="idopcao<?= $p ?>"  required=""/> <?= $itemopcao->texto ?>


                                                </p>
                <?php
            }
        }
        ?>

                                    </blockquote>

                                        <?php
                                        $p++;
                                        $i++;
                                    }
                                } else {
                                    $mensagem = "<div class='alert alert-info'>Nenhum registro cadastrado até o momento.</div>";
                                }
                                ?>  <input type="hidden" name="laco" value="<?= $i ?>"/>
                            <input type="hidden" name="idenquete" value="<?= $itemenquete->id ?>"/>
                            <input type="hidden" name="idusuario" value="<?= $id ?>"/>
                            <div class="btn-holder" id="carregaRE">

                            <?php
                            if ($id != NULL) {
                                $objRespenquetes = new Respenquetes();
                                $listarepenquete = $objRespenquetes->listar("where idusuario=$id and idenquete=$itemenquete->id");
                                if ($listarepenquete != NULL) {
                                    echo "<div class='sucesso'>Você já respondeu essa Enquete! <strong> =)</strong></div>";
                                } else {
                                    ?>



                                        <button class="btn blue"  type="submit">Responder</button>

                                        <?php
                                    }
                                } else {
                                    ?>
                                    <strong>Você precisa estar logado para responder a essa enquete.</strong> <br/>
                                    <a href="#fancy" class="get-contact"><button class="btn blue"  type="button">Logar</button></a>  
    <?php
}
?>
                            </div>
                        </form>



                        <script>

                            $(document).ready(function () {
                                $('#cadastroRE').submit(function () {
                                    var dados = $(this).serialize();
                                    $('#carregaRE').html("<div class='aviso'>Carregando...</div>");
                                    $.ajax({
                                        type: "POST",
                                        url: "acoes/cadastroRE.php",
                                        data: dados,
                                        success: function (data)
                                        {
                                            $("#carregaRE").html(data);

                                        }
                                    });

                                    return false;

                                });
                            });

                        </script>


                    </div>

                </article>




            </section>
        </div>



    </div>
    <!-- / container -->
</div>

<?php
include_once 'rodape.php';
