<?php
header('Content-Type: text/html; charset=UTF-8');
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

include_once '../class/Carrega.class.php';
if (isset($_POST["nome"], $_POST["data"], $_POST["cidade"], $_POST["estado"], $_POST["email"], $_POST["genero"])) {
    $objUsuarios = new Usuarios();
    $nome = pg_escape_string($_POST["nome"]);
    $email = pg_escape_string($_POST["email"]);
    $data = pg_escape_string($_POST["data"]);
    $senha = pg_escape_string($_POST["senha"]);

    $pesquisa = $objUsuarios->listar("where nome ='$nome' or email='$email' ");
    if ($pesquisa == NULL) {        
        require("../class/phpmailer/class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->IsSMTP(); // Define que a mensagem será SMTP
        $mail->Host = "smtp.santanacontabilidade.com"; // Endereço do servidor SMTP
        $mail->SMTPAuth = true; // Autenticação
        $mail->Username = 'sedase@santanacontabilidade.com'; // Usuário do servidor SMTP
        $mail->Password = 'sedase2255'; // Senha da caixa postal utilizada
        $mail->From = "sedase@santanacontabilidade.com";
        $mail->FromName = "Sistema SEDASE";

        $mail->AddAddress($email, $nome);
        $mail->AddAddress($email);
        $mail->AddCC('sedase@getsitesolucoes.com.br', 'Copia');
        //$mail->AddBCC('rafaorlando3@gmail.com', 'Copia Oculta');
        $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
        $mail->CharSet = 'UTF-8';

        $mail->Subject = "Cadastro no Sistema - SEDASE"; // Assunto da mensagem
        $mail->Body = 'Você solicitou cadastrado no sistema <b>SEDASE</b>, abaixo confira suas informacões de login.<br/> <br/> <b>Login:</b> ' . $email . ' <br/> <b>Senha:</b> ' . $senha . ' <br/><br/> Agora você deverá aguardar que sua solicitação seja aceita no sistema para que possa realizar o login, não se preocupe, avisaremos por e-mail quando sua conta estiver ativa.<br/><br/> Acesse o sistema, <br> <center><a href="http://sedase.santanacontabilidade.com" target="_blank"><img src="http://sedase.santanacontabilidade.com/images/logoemailsedase.png" width="30%"/></a></center> <br/> Link SEDASE: http://www.sedase.santanacontabilidade.com <br/><br/> Não responda esse e-mail, ele é automático. ';
        $mail->AltBody = 'Você foi cadastrado no sistema <b>SEDASE</b>, abaixo confira suas informacões de login.<br/> <br/> <b>Login:</b> ' . $email . ' <br/> <b>Senha:</b> ' . $senha . ' <br/><br/> Agora você deverá aguardar que sua solicitação seja aceita no sistema para que possa realizar o login, não se preocupe, avisaremos por e-mail quando sua conta estiver ativa.<br/><br/> Acesse o sistema, <br> <center><a href="http://sedase.santanacontabilidade.com" target="_blank"><img src="http://sedase.santanacontabilidade.com/images/logoemailsedase.png" width="30%"/></a></center> <br/> Link SEDASE: http://www.sedase.santanacontabilidade.com <br/><br/> Não responda esse e-mail, ele é automático.';
        $enviado = $mail->Send();
        $mail->ClearAllRecipients();
        $mail->ClearAttachments();

        if ($enviado) {  
        $objUsuarios->nome = $nome;
        $objUsuarios->genero = pg_escape_string($_POST["genero"]);
        $objUsuarios->data = $data;
        $objUsuarios->cidade = pg_escape_string($_POST["cidade"]);
        $objUsuarios->estado = pg_escape_string($_POST["estado"]);
        $objUsuarios->email = $email;
        $objUsuarios->senha = base64_encode($senha);
        $objUsuarios->tipo = pg_escape_string($_POST["tipo"]);
        $objUsuarios->ativo ='f';
        $objUsuarios->imagem ="user.png";
        $objUsuarios->inserir();
            echo "<div class='sucesso' >Solicitação de cadastro enviada com sucesso. Um e-mail foi enviado para <strong>$email</strong>, verifique.</div>";
            
        } else {
            echo "<div class='aviso'> Usuário não pode ser cadastrado!</div>";
            echo "Informações do erro: " . $mail->ErrorInfo;
        }
    } else {
        echo "<div class='falha'>Usuário já cadastrado.</div>";
    }
}
