<?php
session_start();
include_once '../class/Carrega.class.php';
include_once '../escola/Scriptsphp/gerarsenha.php';

if (isset($_POST["email"])) {
   $objUsuarios = new Usuarios();
   $objUsuarios->email=  pg_escape_string($_POST["email"]);
   $itemuser=$objUsuarios->recuperasenha();
   $senha = geraSenha(6, true, true, true);
        
        require("../class/phpmailer/class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->IsSMTP(); // Define que a mensagem será SMTP
        $mail->Host = "smtp.santanacontabilidade.com"; // Endereço do servidor SMTP
        $mail->SMTPAuth = true; // Autenticação
        $mail->Username = 'sedase@santanacontabilidade.com'; // Usuário do servidor SMTP
        $mail->Password = 'sedase2255'; // Senha da caixa postal utilizada
        $mail->From = "sedase@santanacontabilidade.com";
        $mail->FromName = "Sistema SEDASE";

        $mail->AddAddress($itemuser->email, $itemuser->nome);
        $mail->AddAddress($itemuser->email);
        $mail->AddCC('sedase@getsitesolucoes.com.br', 'Copia');
        //$mail->AddBCC('rafaorlando3@gmail.com', 'Copia Oculta');
        $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
        $mail->CharSet = 'UTF-8';
        $mail->Subject = "Recuperação de Senha - Sistema SEDASE"; // Assunto da mensagem
        $mail->Body = 'Essa senha foi gerada automáticamente, abaixo confira suas novas informacões de login.<br/> <br/> <b>Login:</b> ' . $itemuser->email . ' <br/> <b>Senha:</b> ' . $senha . ' (Senha gerada automática, sugerimos que mude a senha depois que realizar o primeiro login no nosso sistema.) <br/><br/>  Acesse o sistema, <br> <center><a href="http://sedase.santanacontabilidade.com" target="_blank"><img src="http://sedase.santanacontabilidade.com/images/logoemailsedase.png" width="30%"/></a></center> <br/> Link SEDASE: http://www.sedase.santanacontabilidade.com <br/><br/> Não responda esse e-mail, ele é automático. ';
        $mail->AltBody = 'Essa senha foi gerada automáticamente, abaixo confira suas novas informacões de login.<br/> <br/> <b>Login:</b> ' . $itemuser->email . ' <br/> <b>Senha:</b> ' . $senha . ' (Senha gerada automática, sugerimos que mude a senha depois que realizar o primeiro login no nosso sistema.) <br/><br/> Acesse o sistema, <br> <center><a href="http://sedase.santanacontabilidade.com" target="_blank"><img src="http://sedase.santanacontabilidade.com/images/logoemailsedase.png" width="30%"/></a></center> <br/> Link SEDASE: http://www.sedase.santanacontabilidade.com <br/><br/> Não responda esse e-mail, ele é automático.';
        $enviado = $mail->Send();
        $mail->ClearAllRecipients();
        $mail->ClearAttachments();

        if ($enviado) {
        $objUsuarios->id = $itemuser->id;
        $objUsuarios->nome = $itemuser->nome;
        $objUsuarios->data = $itemuser->data;
        $objUsuarios->cidade = $itemuser->cidade;
        $objUsuarios->estado = $itemuser->estado;
        $objUsuarios->genero = $itemuser->genero;
        $objUsuarios->email = $itemuser->email;
        $objUsuarios->senha = base64_encode($senha);
        $objUsuarios->tipo = $itemuser->tipo;
        $objUsuarios->ativo = $itemuser->ativo;
        $objUsuarios->imagem = null;
        $objUsuarios->atualizar();

         echo "<div class='sucesso'> Solicitação realizada com Sucesso! Um e-mail foi enviado para <strong>$itemuser->email</strong> contendo instruções para realizar seu login. </div>";
        }
        else {
            echo "<div class='erro'> A atualização falhou devido a algum problema em nosso servidor de email, brevemente solucionaremos esse problema.</div>";
            echo "Informações do erro: " . $mail->ErrorInfo;
        }
}
 else {
        echo "<div class='erro'>Erro ao atualizar informações. </div>";
    }
