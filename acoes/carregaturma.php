<?php
include_once '../class/Carrega.class.php';
?>           
<script src="js/jquery-3.1.0.js"></script>
<label for="turma">Turma:</label>
<?php
$objTurma = new Turma();
$listaturma = $objTurma->listar("order by grau ASC");
if ($listaturma != null) {
    $update = 1;
    echo "<select name='idturma' required=''>";
    foreach ($listaturma as $itemturma) {
        ?>
        <option value="<?= $itemturma->id ?>"><?= "Turma: ".$itemturma->nome." - Ano: ".$itemturma->ano." - Grau: ".$itemturma->grau ?></option> 

        <?php
    }
    echo "</select> ";
} else {
    $update = 0;
    ?>

        <select disabled>
        <option> Nenhum registro encontrado. </option>
    </select>

    <?php
}
?>  

