<?php
$menu=2;
include_once 'cabecalho.php';

?>


       <!-- Modal Exibe Material -->
<div class="modal fade" id="dinamico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Exbindo Material</h4>
            </div>
            <div class="modal-body">


                <div id="respostadinamica"></div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

            </div>


        </div>
    </div>
</div>
<!--             Fim modal...-->


<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Materiais</h4>

            </div>

        </div>
        <div class="row">
            <div class="col-md-12">

            
           
                <hr/>
                
                                 <div class="text-right pull-right"><label>Buscar aqui: </label> <input  type="text" id="busca" value=""  /> </div>

                
                <h2>Conteúdos<small> Listando</small></h2>
                <div id="listamateriais">


                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
                                var jq = $.noConflict();

                                function atualizamateriais() {
                                    jq.get('Selects/carregamateriais.php', function (resultado) {
                                        jq('#listamateriais').html(resultado);

                                    });
                                }
                         
                                jq(document).ready(function () {
                                      atualizamateriais();
                                
                                jq("#busca").keypress(function () {
                                    var pesquisa = jq('#busca').val();
                                    jq.get('Selects/carregamateriais.php?pesquisa=' + pesquisa, function (resultado) {
                                        jq('#listamateriais').html(resultado);

                                    });

                                });

                                });
                                
                            </script>

<?php
include_once 'rodape.php';
