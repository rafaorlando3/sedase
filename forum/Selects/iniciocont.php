<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');
$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
$tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';


?>
<p class="bg-primary text-center"><strong>Últimos Tópicos para você</strong></p>

<?php
                    $objForum = new Forum();
                    $comp = "where idturma is NULL order by titulo ASC LIMIT 8" ;      
                        $lista = $objForum->listar($comp);
                        if ($lista != null) {
                            $mensagem = "";
                            foreach ($lista as $item) {  
                                ?>

<div class="panel panel-primary" title="Fórum Destaques">
                <div class="panel-heading">

                    <label><?= $item->titulo ?> - Publico!</label> 
                    <label style='float: right;clear: both;'>

                        <?= date("d-m-Y", strtotime($item->data)) ?> às <?= date("H:i:s", strtotime($item->data)) ?>
                    </label>

                </div>
                <div class="panel-body">
                    <?= $item->texto; ?>
                    
                    <label class="text-right pull-right">

                        <form method="post" action="forumenter.php"><input type="hidden" name="idedit" value="<?= $item->id ?>"/><button type="submit" class="btn btn-success">Entrar</button></form>
                    </label>
                </div>
            </div>
          
 <?php
                            }
                        } else {
                            $mensagem = "<div class='alert alert-info'>Nenhum tópico disponível até o momento.</div>";
                        }
                        ?>   


