<?php

include_once '../../class/Carrega.class.php';

$objRespostas = new Respostas();
if (isset($_POST["idresposta"])) {

    $cod = $_POST["idresposta"];

    $objRespostas->id = $cod;
    $objRespostas->excluir();

   echo "<div class='alert alert-danger'> Resposta excluída com Sucesso!  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";

} else {

    echo "<div class='alert alert-danger'>Erro ao tentar excluir essa Resposta.  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
}
