<?php
$menu = 7;
include_once 'cabecalho.php';

  if (isset($_SESSION['idforum'])){
    $idedit= $_SESSION['idforum'];
    $objForum = new Forum();
    $objForum->id = $idedit;
    $item = $objForum->retornarunico();
    $_SESSION['pagina']=$_GET['pagina'];
      
  }

if ($_POST['idedit']) {
    $idedit = $_POST['idedit'];
    $_SESSION["idforum"] = $idedit;
    $objForum = new Forum();
    $objForum->id = $_SESSION["idforum"];
    $item = $objForum->retornarunico();
    $_SESSION['pagina']=$_GET['pagina'];
   
} else {
    header("location:forum.php");
}



?>

<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">
                    <a href="forum.php"><span class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" title="Voltar"></span> </a>   Discussão: <?= $item->titulo; ?>

            </div>

        </div>
        
        
        <!-- Modal das respostas/edição -->
<div class="modal fade" id="dinamico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Central de Respostas</h4>
            </div>
            <div class="modal-body">


                <div id="respostadinamica"></div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

            </div>


        </div>
    </div>
</div>
<!--             Fim modal...-->

        

        <div class="alert alert-info alert-dismissible fade in" role="alert"><strong>Descrição:</strong> <?= $item->texto; ?></div>
        <div class="row">
            <div class="col-md-12" style="background-color:#eee;">

                <p></p>
                
                <div id="resposta2"></div>

                <div class="row">
                    <div class="col-sm-offset-5 col-sm-2 text-center">

                        <button id="mostraesconde" class="btn btn-success btn-lg" type="button" data-toggle="collapse" data-target="#visualizaresponda" aria-expanded="false" aria-controls="mostraocultaresponda" title="Responda esse tópico clicando aqui.">
                            <span class="glyphicon glyphicon-sort" aria-hidden="true" title="Mostrar/Ocultar"></span> Responder
                        </button>

                    </div>
                </div>
                <div class="collapse" id="visualizaresponda">
                    <div class="well">

                        <form id="cadastroR">
                            <div class="form-group">
                                <textarea name="respostas" id="editor1" class="form-control"></textarea>
                            </div>
                            
                            <input type="hidden" name="idforum" id="idforum" value="<?= $idedit; ?>"/>
                            <input type="hidden" name="idusuario" id="idusuario" value="<?= $id; ?>"/>

                            <div class="row">
                                <div class="col-sm-offset-5 col-sm-2 text-center">

                                    <button type="button" class="btn btn-primary" id="carregarR">Publicar</button>

                                </div>
                            </div>
                            
                        </form>
                        
                    </div>

                </div>

                <p></p>

                <div id="carregarespostas"></div>
                
                 <div id="respostaR2"></div>

            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    var jq = $.noConflict();
    jq(document).ready(function () {
        jq('#carregarR').click(function () {
            jq('#carregarR').attr('disabled', true);
            jq("#carregarR").html('publicando...');
            
            for ( instance in CKEDITOR.instances ) {
        CKEDITOR.instances[instance].updateElement();
    }
            var dados = jq('#cadastroR').serialize();
            jq.ajax({
                type: "POST",
                url: "Inserts/cadastroR.php",
                data: dados,
                success: function (data)
                {
                    jq('#resposta2').html(data);
                    jq('#carregarR').attr('disabled', false);
                    jq("#carregarR").html('Publicar');
                    atualiza();
                    jq('#visualizaresponda').toggleClass("collapsing");
                    //jq('#visualizaresponda').toggleClass("collapse");
                    setTimeout(function(){ jq('#visualizaresponda').removeClass("collapsing"); jq('#visualizaresponda').removeClass("in"); }, 3000);
                    
                    
                }
            });

            return false;

        });
        
        atualiza();
       
    });
    
    
    function atualiza() {

            
            jq.get('Selects/carregarespostas.php', function (resultado) {
                jq('#carregarespostas').html(resultado);

            });
        }
        
    CKEDITOR.replace( 'editor1' );	

</script>


<?php
include_once 'rodape.php';
