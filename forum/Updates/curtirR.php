<?php
include_once '../../class/Carrega.class.php';
date_default_timezone_set("Brazil/East");
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);
$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';


if (isset($_POST["idresposta"])) {

    $idresposta = pg_escape_string($_POST["idresposta"]);
    
    $objCurtirR = new CurtirR();
    $objCurtirR->idresposta = $idresposta;
    $itemcurtir = $objCurtirR->retornarunico();

    $complemento = "where idresposta=$itemcurtir->idresposta and idusuario=$id and curtiu=FALSE";
    $complemento2 = "where idresposta=$itemcurtir->idresposta and idusuario=$id and curtiu=TRUE";

    $lista = $objCurtirR->listar($complemento);
    $lista2 = $objCurtirR->listar($complemento2);
    if ($lista != null) {
        $objCurtirR->id = $itemcurtir->id;
        $objCurtirR->contagem = $itemcurtir->contagem + 1;
        $objCurtirR->curtiu = 't';
        $objCurtirR->data = date('Y-m-d H:i');
        $objCurtirR->idresposta = $idresposta;
        $objCurtirR->idusuario = $id;
        $objCurtirR->atualizar();
        echo $objCurtirR->retornatotal($idresposta);
        
    } elseif($lista2 != null) {
        echo $objCurtirR->retornatotal($idresposta);
    }
    else{
       
    $objCurtirR->contagem = $itemcurtir->contagem + 1;
    $objCurtirR->curtiu = 't';
    $objCurtirR->data = date('Y-m-d H:i');
    $objCurtirR->idresposta = $idresposta;
    $objCurtirR->idusuario = $id;
    $objCurtirR->inserir();
    echo $objCurtirR->retornatotal($idresposta);
    }
    
   
}