<?php
include_once '../../class/Carrega.class.php';
session_start();
date_default_timezone_set("Brazil/East");


if (isset($_POST["respostas"], $_POST["idresposta"], $_POST["idforum"], $_POST["idusuario"], $_POST["melhoresposta"])) {

    $respostas = htmlspecialchars_decode($_POST["respostas"]);

    $objRespostas = new Respostas();
    $objRespostas->id = pg_escape_string($_POST["idresposta"]);
    $itemresposta=$objRespostas->retornarunico();
   
    
    if($itemresposta->citacao!=NULL){
    
    $objRespostas->texto = $respostas;
    $objRespostas->idforum = pg_escape_string($_POST["idforum"]);
    $objRespostas->idusuario = $itemresposta->idusuario;
    $objRespostas->melhoresposta = pg_escape_string($_POST["melhoresposta"]);
    $objRespostas->citacao = $itemresposta->citacao;
    $objRespostas->data = $itemresposta->data;
    $objRespostas->atualizar();
    
    } else{
    $objRespostas->texto = $respostas;
    $objRespostas->idforum = pg_escape_string($_POST["idforum"]);
    $objRespostas->idusuario = $itemresposta->idusuario;
    $objRespostas->melhoresposta = pg_escape_string($_POST["melhoresposta"]);
    $objRespostas->data = $itemresposta->data;
    $objRespostas->atualizar2();
    }

    echo "<hr/><div class='alert alert-success'> Resposta atualizada com sucesso! Se sua resposta não aparecer nesta página, navegue nos links de paginação para encontrá-lo. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
}