<?php
include_once 'cabecalho.php';

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);
$filtro="";
$escolha=10;

if(isset($_GET['filtro'])){
    if($_GET['filtro']<=2){
         
      $escolha =$_GET['filtro'];
      $filtro="where categoria=$escolha";
    }
    else{
    
    $escolha =10;
    $filtro="";
    
    
    }
}


?>
<div class="divider"></div>
	
	<div class="content">
		<div class="container">

			<div class="main-content">
				<h1>Prestação de Serviços</h1>
                                
                                
                         
                                <label for="categoria"><strong>Categorias:</strong></label>
                                <input type="radio" id="categoria0" name="categoria" value="10" <?php if($escolha==10){ echo "checked='true'";} ?>   /> Todas
                             <input type="radio" id="categoria1" name="categoria" value="0" <?php if($escolha==0){ echo "checked='true'";} ?> /> Informações
                             <input type="radio" id="categoria2" name="categoria" value="1" <?php if($escolha==1){ echo "checked='true'";} ?> />Denúncias
                             <input type="radio" id="categoria3" name="categoria" value="2" <?php if($escolha==2){ echo "checked='true'";} ?>/>Dicas
                                        
                                <hr/>
				<section class="posts-con">
                                     <?php
                        $objServicos = new Pservicos();
                        $lista = $objServicos->listar("$filtro order by id DESC");
                        if ($lista != null) {
                            $mensagem = "";

                            foreach ($lista as $item) {
                                    $objUsuarios = new Usuarios();
                                    $objUsuarios->id = $item->idusuario;
                                    $itemuser = $objUsuarios->retornarunico();
                                ?>
                                    
					<article>
						<div class="current-date">
							<p>Público!</p>
                                                        
                                                        <?php 
                                    
                                    if($item->categoria==0){
                                        $textos= "Informações";
                                    } elseif($item->categoria==1){
                                        $textos="Denúncias";
                                    } else{
                                        $textos="Dicas";
                                    }
                                            
                                            ?>
                                                        
                                                        <p class="date" style="font-size: 14px;"><?=$textos?></p>
                                                        
						</div>
						<div class="info">
                                                    <h3><a href="servico.php?data=<?=$item->id?>"><?=$item->titulo?></a></h3>
                                                        <p class="info-line"><span class="time">Por <?=$itemuser->nome?> </span></p>
                                                        <p><?php  echo $conteudo =limitarTexto($item->texto, $limite = 300); ?> <a href="servico.php?data=<?=$item->id?>">Visualizar...</a></p>
						</div>
					</article>
                                    
                                    
                                    
                                <?php
                            }
                        } else {
                            $mensagem = "<div class='alert alert-info'>Nenhum registro encontrado até o momento.</div>";
                        }
                        ?>   

                <?= $mensagem ?>
					
				</section>
			</div>
			
<!--			<aside id="sidebar">
				<div class="widget clearfix calendar">
					<h2>Event calendar</h2>
					<div class="head">
						<a class="prev" href="#"></a>
						<a class="next" href="#"></a>
						<h4>April 2014</h4>
					</div>
					<div class="table">
						<table>
							<tr>
								<th class="col-1">Mon</th>
								<th class="col-2">Tue</th>
								<th class="col-3">Wed</th>
								<th class="col-4">Thu</th>
								<th class="col-5">Fri</th>
								<th class="col-6">Sat</th>
								<th class="col-7">Sun</th>
							</tr>
							<tr>
								<td class="col-1 disable"><div>31</div></td>
								<td class="col-2"><div>1</div></td>
								<td class="col-3"><div>2</div></td>
								<td class="col-4"><div>3</div></td>
								<td class="col-5 archival"><div>4</div></td>
								<td class="col-6"><div>5</div></td>
								<td class="col-7"><div>6</div></td>
							</tr>
							<tr>
								<td class="col-1"><div>7</div></td>
								<td class="col-2"><div>8</div></td>
								<td class="col-3 archival"><div>9</div></td>
								<td class="col-4"><div>10</div></td>
								<td class="col-5"><div>11</div></td>
								<td class="col-6"><div>12</div></td>
								<td class="col-7"><div>13</div></td>
							</tr>
							<tr>
								<td class="col-1"><div>14</div></td>
								<td class="col-2 upcoming"><div><div class="tooltip"><div class="holder">
									<h4>Omnis iste natus error sit voluptatem dolor</h4>
									<p class="info-line"><span class="time">10:30 AM</span><span class="place">Lincoln High School</span></p>
									<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident similique.</p>
								</div></div>15</div></td>
								<td class="col-3"><div>16</div></td>
								<td class="col-4 upcoming"><div><div class="tooltip"><div class="holder">
									<h4>Omnis iste natus error sit voluptatem dolor</h4>
									<p class="info-line"><span class="time">10:30 AM</span><span class="place">Lincoln High School</span></p>
									<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident similique.</p>
								</div></div>16</div></td>
								<td class="col-5"><div>18</div></td>
								<td class="col-6"><div>19</div></td>
								<td class="col-7"><div>20</div></td>
							</tr>
							<tr>
								<td class="col-1"><div>21</div></td>
								<td class="col-2"><div>22</div></td>
								<td class="col-3"><div>23</div></td>
								<td class="col-4"><div>24</div></td>
								<td class="col-5 upcoming"><div><div class="tooltip"><div class="holder">
									<h4>Omnis iste natus error sit voluptatem dolor</h4>
									<p class="info-line"><span class="time">10:30 AM</span><span class="place">Lincoln High School</span></p>
									<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident similique.</p>
								</div></div>25</div></td>
								<td class="col-6"><div>26</div></td>
								<td class="col-7"><div>27</div></td>
							</tr>
							<tr>
								<td class="col-1"><div>28</div></td>
								<td class="col-2 upcoming"><div><div class="tooltip"><div class="holder">
									<h4>Omnis iste natus error sit voluptatem dolor</h4>
									<p class="info-line"><span class="time">10:30 AM</span><span class="place">Lincoln High School</span></p>
									<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident similique.</p>
								</div></div>29</div></td>
								<td class="col-3"><div>30</div></td>
								<td class="col-4 disable"><div>1</div></td>
								<td class="col-5 disable"><div>2</div></td>
								<td class="col-6 disable"><div>3</div></td>
								<td class="col-7 disable"><div>4</div></td>
							</tr>
						</table>
					</div>
					<div class="note">
						<p class="upcoming-note">Upcoming event</p>
						<p class="archival-note">Archival event</p>
					</div>
				</div>
				<div class="widget list">
					<h2>Photo gallery</h2>
					<ul>
						<li><a href="#"><img src="images/4.png" alt=""></a></li>
						<li><a href="#"><img src="images/4_2.png" alt=""></a></li>
						<li><a href="#"><img src="images/4_3.png" alt=""></a></li>
						<li><a href="#"><img src="images/4_4.png" alt=""></a></li>
						<li><a href="#"><img src="images/4_5.png" alt=""></a></li>
						<li><a href="#"><img src="images/4_6.png" alt=""></a></li>
					</ul>
					<div class="btn-holder">
						<a class="btn blue" href="#">Show more photos</a>
					</div>
				</div>
			</aside>-->
			<!-- / sidebar -->
	
		</div>
		<!-- / container -->
	</div>
                
                
<script type="text/javascript">
    $(document).ready(function () {
        $('#categoria1').click(function () {
            var escolha = this.value;
            
            window.location.href="servicos.php?filtro="+escolha;
            
        });
        
        
        $('#categoria2').click(function () {
            var escolha = this.value;
         window.location.href="servicos.php?filtro="+escolha;

        });
        
        $('#categoria3').click(function () {
            var escolha = this.value;
           window.location.href="servicos.php?filtro="+escolha;

        });
        
        $('#categoria0').click(function () {
            var escolha = this.value;
           window.location.href="servicos.php?filtro="+escolha;

        });
        

    });
</script>

<?php 

include_once 'rodape.php';
