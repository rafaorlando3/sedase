<?php
include_once 'cabecalho.php';
include_once 'class/Noticias.class.php';
include_once 'testemes.php';

if ($_GET['data']) {
    $objServico = new Pservicos();
    $objServico->id = $_GET['data'];
    $item = $objServico->retornarunico();

    $objUsuarios = new Usuarios();
    $objUsuarios->id = $item->idusuario;
    $itemuser = $objUsuarios->retornarunico();

    if ($item->categoria == 0) {
        $textos = "Informações";
    } elseif ($item->categoria == 1) {
        $textos = "Denúncias";
    } else {
        $textos = "Dicas";
    }
} else {

    header("Location:servicos.php");
}
?>

<div class="divider"></div>

<div class="content">
    <div class="container">

        <div class="main-content">
            <h1><?= $textos.": ".$item->titulo ?></h1>
            <section class="posts-con">
<?php ?>

                <article>
                    <div class="current-date">
                        <p>Público!</p>
                        <p class="date"></p>
                    </div>
                    <div class="info">

                        <p class="info-line"><span class="time">Por <?= $itemuser->nome ?> </span></p>
                        <br/>

<?php
if ($item->video != NULL) {
    ?>


                            <h3>Esse material tem um anexo, faça o download clicando: <a href="uploads/anexos/<?= $item->anexo ?>" target="_blank">Aqui</a></h3>



                            <?php
                        }
                        ?>

<?php
if ($item->video != NULL) {
    ?>
                            <h3>Vídeo:</h3>
                            <div id="ytplayer"></div> 

                            <script>
                                // Load the IFrame Player API code asynchronously.
                                var tag = document.createElement('script');
                                tag.src = "https://www.youtube.com/player_api";
                                var firstScriptTag = document.getElementsByTagName('script')[0];
                                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                                // Replace the 'ytplayer' element with an <iframe> and
                                // YouTube player after the API code downloads.
                                var player;
                                function onYouTubePlayerAPIReady() {
                                    player = new YT.Player('ytplayer', {
                                        height: "100%",
                                        width: "100%",
                                        videoId: '<?= $item->video ?>'
                                    });
                                }
                            </script>

    <?php
}
?>
                        <p><?= $item->texto ?></p>


<?php
if ($item->imagem != NULL) {
    ?>
                            <img src="uploads/imagens/<?= $item->imagem ?>" width="80%" />
                            <?php
                        }
                        ?>
                    </div>




                </article>




            </section>
        </div>



    </div>
    <!-- / container -->
</div>

<?php
include_once 'rodape.php';
