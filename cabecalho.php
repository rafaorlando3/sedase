<?php session_start();?>
<?php
include_once 'class/Carrega.class.php';
include_once 'testemes.php';
date_default_timezone_set('America/Sao_Paulo');

ini_set('display_errors', 0);
ini_set('display_startup_erros', 0);
error_reporting(E_ALL);

if (isset($_SESSION['acesso'])){
    $_SESSION['acesso']=1;
  }

$cadastro=FALSE;
$objEscola = new Escola();
$existe = $objEscola->listar();

if ($existe != null) {
    
    foreach ($existe as $item) {
        $escola = $item->nome;
        $codescola = $item->codigo;
        $idescola = $item->id;
        $email = $item->email;
        $endereco = $item->endereco;
        $telefone = $item->telefone;
    }

    $cadastro = TRUE;
    
$nome = isset($_SESSION['nome']) ? $_SESSION['nome'] : '';
$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
if (isset($_SESSION["nome"], $_SESSION["iduser"], $_SESSION["tipo"])) {
    $id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
    $nome = isset($_SESSION['nome']) ? $_SESSION['nome'] : '';
    $tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';
    
    $nome2=explode(" ",$nome);

    $objUsuarios = new Usuarios();
    $objUsuarios->id = $id;
    $itemuser = $objUsuarios->retornarunico();

  
}
   
    
}elseif($cadastro==FALSE){
    
    header("Location:escola/index.php");
}


?>

<!--[if IE 8]> <html class="ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>SEDASE</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
	<link rel="stylesheet" media="all" href="css/style.css">
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
              <script src="js/jquery-3.1.0.js"></script>
        <link rel="stylesheet" href="css/redmond/jquery-ui-1.10.1.custom.css" />
        <script src="js/jquery-ui.js" type="text/javascript"></script>
</head>
<body>

	<header id="header">
		<div class="container">
			<a href="#fancy2" class="get-contact" id="logo" title="Sistema de Ensino da Sexualidade na Escola">SEDASE</a>
                        <div class="menu-trigger">
                            
                        </div>
			<nav id="menu">
				
                            
                            <ul>
                            
                                    <li><a href="index.php">Início</a></li>
                                        <li><a href="enquetes.php">Enquetes</a></li>
                                        <li><a href="materiais.php">Materiais</a></li>
                                        
                                       
                                       
 				</ul>
                            
				<ul>
                                    <li><a href="forum.php">Fórum</a></li>
                                    <li><a href="servicos.php">Serviços</a></li>
                                    <li ><?php if($nome!=NULL){ echo "<a href='#fancy' class='get-contact'> >>Olá, ".$nome2[0]."<< </a>";} else{ ?><a href="#fancy" class="get-contact">Login</a><?php }?></li>
  
   				</ul>
                            
                     
			</nav>
			<!-- / navigation -->
                        
                                    
                          
		</div>
		<!-- / container -->
	
	</header>
	<!-- / header -->
