<?php
include_once 'cabecalho.php';
?>
	
	<div class="slider">
		<ul class="bxslider">
			<li>
				<div class="container">
					<div class="info">
						<h2>Conheça o Sistema de <br><span>Ensino da Sexualidade na Escola</span></h2>
						<a href="sobre.php">Saiba mais</a>
					</div>
				</div>
				<!-- / content -->
			</li>
			<li>
				<div class="container">
					<div class="info">
						<h2>Extra!<br><span>Últimas notícias</span></h2>
                                                <a href="noticias.php">Ver mais</a>
					</div>
				</div>
				
			</li>
			<li>
				<div class="container">
					<div class="info">
						<h2>Confira as nossas <br><span>Enquetes</span></h2>
						<a href="enquetes.php">Ver mais</a>
					</div>
				</div>
			 
			</li>
		</ul>
		<div class="bg-bottom"></div>
	</div>
	
	<section class="posts">
		<div class="container">
			
                      <?php
                        $objNoticias = new Noticias();
                        $lista = $objNoticias->listar("order by data DESC LIMIT 6");
                        if ($lista != null) {
                            $mensagem = "";

                            foreach ($lista as $item) {
                                $mesagora=date("m", strtotime($item->data));
                                $dianoticia=date("d", strtotime($item->data));
                                ?>
                    
                    
                            <article>
                                <div class="pic"><img width="121" src="images/2.png" alt=""></div>
				<div class="info">
					<h3><a href="noticia.php?data=<?=$item->id?>"><?=$item->titulo?></a></h3>
					<p><?php  echo $conteudo =limitarTexto($item->conteudo, $limite = 100); ?></p>
				</div>
			</article>
			
                           <?php
                            }
                        } else {
                            $mensagem = "<div class='alert alert-info'>Nenhuma Notícia encontrada até o momento.</div>";
                        }
                        ?>   

                <?= $mensagem ?>
					
                </div>
		<!-- / container -->
	</section>

<?php 

include_once 'rodape.php';
