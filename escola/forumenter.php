<?php
$menu = 7;
include_once 'cabecalho.php';

  if (isset($_SESSION['idforum'])){
    $idedit= $_SESSION['idforum'];
    $objForum = new Forum();
    $objForum->id = $idedit;
    $item = $objForum->retornarunico();
    $_SESSION['pagina']=$_GET['pagina'];
      
  }

if ($_POST['idedit']) {
    $idedit = $_POST['idedit'];
    $_SESSION["idforum"] = $idedit;
    $objForum = new Forum();
    $objForum->id = $_SESSION["idforum"];
    $item = $objForum->retornarunico();
    $_SESSION['pagina']=$_GET['pagina'];
   
} else {
    header("location:forum.php");
}



?>

<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">
                    <a href="forum.php"><span class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true" title="Voltar"></span> </a>   Discussão: <?= $item->titulo; ?>

                    <?php
                    if ($id == 1 || $item->idusuario == $id) {

                        echo '<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                    Editar
                </button>';
                        ?>


                    </h4>
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Editando</h4>
                                </div>
                                <div class="modal-body">

                                    <form id="editaF" >
                                        <div class="form-group">
                                            <label for="titulo">Título:</label>
                                            <input type="text" name="titulo" class="form-control" id="titulo" placeholder="Digite o Título..." value="<?= $item->titulo; ?>" required="" />
                                        </div>
                                        <div class="form-group">
                                            <label for="conteudo">Conteúdo:</label> <br/>
                                            <textarea name="descricao" id="conteudo" required="" class="form-control"><?= $item->texto; ?></textarea>
                                        </div>
                                        <div class="form-group" id="carregaturma">

                                        </div>

                                        <ul class="nav nav-tabs" role="tablist" id="myTab">
                                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Vazio</a></li>
                                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Foto</a></li>

                                        </ul>

                                        <div class="tab-content">
                                            <link href="assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
                                            <script src="assets/js/fileinput.js" type="text/javascript"></script>
                                            <script src="assets/js/fileinput.min.js" type="text/javascript"></script>
                                            <div role="tabpanel" class="tab-pane active" id="home">Não atualizar nenhum arquivo. </div>
                                            <div role="tabpanel" class="tab-pane" id="profile">
                                                <div class="form-group">
                                                    <label>Selecione a Foto:</label>
                                                    <input name="imagem" id="file-3" type="file" />
                                                    <p></p>
                                                    <?php
                                                    if ($item->imagem != "" || $item->imagem != NULL) {
                                                        ?>

                                                        Imagem Atual: <img src="../uploads/imagens/<?= $item->imagem ?>" width="20%"alt="..." class="img-thumbnail">

                                                        <?php
                                                    }
                                                    ?>


                                                </div>

                                            </div>


                                            <script>

                                                $("#file-3").fileinput({
                                                    allowedFileExtensions: ['jpg', 'png', 'gif'],
                                                    showUpload: false,
                                                    showCaption: false,
                                                    browseClass: "btn btn-primary btn-lg",
                                                    fileType: "any",
                                                    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
                                                });
                                            </script>
                                        </div>

                                        <script>
                                            $(function () {
                                                $('#myTab a:last').tab('show')
                                            })
                                        </script>
                                        <br/>
                                        <div class="form-group">
                                            <label for="conteudo">Tópico Concluído?: </label>
                                            Sim <input type="radio" name="resolvido" value="t" <?php
                                            if ($item->resolvido == "t") {
                                                echo "checked";
                                            }
                                            ?> /> - Não <input type="radio" name="resolvido" value="f" <?php
                                                       if ($item->resolvido == "f") {
                                                           echo "checked";
                                                       }
                                                       ?> />

                                        </div>

                                        <input type="hidden" name="idusuario" value="<?= $item->idusuario; ?>"/>
                                        <input type="hidden" name="id" value="<?= $item->id; ?>"/>


                                        <button type="submit" class="btn btn-primary" id="carregar"  >Atualizar</button>
                                    </form> <br/>

                                    <div id="resposta"></div> <a href="forum.php"><button type="button" class="btn btn-default">Voltar</button></a>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

                                </div>
                            </div>
                        </div>
                    </div>



                    <?php
                }
                ?>



            </div>

        </div>
        
        
        <!-- Modal das respostas/edição -->
<div class="modal fade" id="dinamico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Central de Respostas</h4>
            </div>
            <div class="modal-body">


                <div id="respostadinamica"></div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

            </div>


        </div>
    </div>
</div>
<!--             Fim modal...-->

        

        <div class="alert alert-info alert-dismissible fade in" role="alert"><strong>Descrição:</strong> <?= $item->texto; ?></div>
        <div class="row">
            <div class="col-md-12" style="background-color:#eee;">

                <p></p>
                
                <div id="resposta2"></div>

                <div class="row">
                    <div class="col-sm-offset-5 col-sm-2 text-center">

                        <button id="mostraesconde" class="btn btn-success btn-lg" type="button" data-toggle="collapse" data-target="#visualizaresponda" aria-expanded="false" aria-controls="mostraocultaresponda" title="Responda esse tópico clicando aqui.">
                            <span class="glyphicon glyphicon-sort" aria-hidden="true" title="Mostrar/Ocultar"></span> Responder
                        </button>

                    </div>
                </div>
                <div class="collapse" id="visualizaresponda">
                    <div class="well">

                        <form id="cadastroR">
                            <div class="form-group">
                                <textarea name="respostas" id="editor1" class="form-control"></textarea>
                            </div>
                            
                            <input type="hidden" name="idforum" id="idforum" value="<?= $idedit; ?>"/>
                            <input type="hidden" name="idusuario" id="idusuario" value="<?= $id; ?>"/>

                            <div class="row">
                                <div class="col-sm-offset-5 col-sm-2 text-center">

                                    <button type="button" class="btn btn-primary" id="carregarR">Publicar</button>

                                </div>
                            </div>
                            
                        </form>
                        
                    </div>

                </div>

                <p></p>

                <div id="carregarespostas"></div>
                
                 <div id="respostaR2"></div>

            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    var jq = $.noConflict();
    jq(document).ready(function () {
        jq('#editaF').submit(function () {
            jq('#carregar').attr('disabled', true);
            jq("#carregar").html('Carregando...');
            jq.ajax({
                type: "POST",
                url: "Updates/editaF.php",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (data)
                {
                    jq('#resposta').html(data);
                    jq('#carregar').attr('disabled', false);
                    jq("#carregar").html('Atualizar');
                }
            });

            return false;

        });
        
        
        jq('#carregarR').click(function () {
            jq('#carregarR').attr('disabled', true);
            jq("#carregarR").html('publicando...');
            
            for ( instance in CKEDITOR.instances ) {
        CKEDITOR.instances[instance].updateElement();
    }
            var dados = jq('#cadastroR').serialize();
            jq.ajax({
                type: "POST",
                url: "Inserts/cadastroR.php",
                data: dados,
                success: function (data)
                {
                    jq('#resposta2').html(data);
                    jq('#carregarR').attr('disabled', false);
                    jq("#carregarR").html('Publicar');
                    atualiza();
                    jq('#visualizaresponda').toggleClass("collapsing");
                    //jq('#visualizaresponda').toggleClass("collapse");
                    setTimeout(function(){ jq('#visualizaresponda').removeClass("collapsing"); jq('#visualizaresponda').removeClass("in"); }, 3000);
                    
                    
                }
            });

            return false;

        });
        
        atualiza();
       
    });
    
    
    function atualiza() {

            
            jq.get('Selects/carregarespostas.php', function (resultado) {
                jq('#carregarespostas').html(resultado);

            });
        }
        
    CKEDITOR.replace( 'editor1' );	

</script>


<?php
include_once 'rodape.php';
