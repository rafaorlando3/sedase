<?php
$menu = 9;
include_once 'cabecalho.php';
?>

<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Tutorial</h4>

            </div>

        </div>
        
        <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#tuto1" aria-expanded="false" aria-controls="mostraocultaperguntas" title="Veja como está ficando essa enquete">
                    <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true" title="Mostrar/Ocultar"></span> Cadastros/Listagens/Edições/Exclusões
                </button>
                <div class="collapse" id="tuto1">
                    <div class="well">

                        <div class="panel panel-default">
  <div class="panel-body">

      <p><u><strong><span style="font-family:Arial,sans-serif">1. Cadastros</span></strong></u></p>

<p>&nbsp;</p>

<p><span style="font-family:Arial,sans-serif">Para cadastrar qualquer conte&uacute;do no sistema voc&ecirc; entrar&aacute; no item que deseja no menu (ex: Enquetes), e clicar&aacute; no bot&atilde;o azul &ldquo;Cadastrar&rdquo;. <strong>Obs:</strong> O texto do bot&atilde;o mudar&aacute; de acordo com a p&aacute;gina no qual voc&ecirc; estiver, podendo ser &ldquo;Cadastrar novo Usu&aacute;rio&rdquo;, &ldquo;Cadastrar nova Turma&rdquo;, &ldquo;Cadastrar nova Enquete&rdquo;, &ldquo;Cadastrar nova Not&iacute;cia&rdquo;, &ldquo;Cadastrar novo Servi&ccedil;o&rdquo;, &ldquo;Cadastrar novo Material&rdquo; e Cadastrar novo T&oacute;p&iacute;co.</span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>Instru&ccedil;&otilde;es:</strong></span></p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>1</strong> &ndash; Clique em &ldquo;Cadastrar (nome espec&iacute;fico)&rdquo;.</span></p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>2</strong> &ndash; Preencha o formul&aacute;rio.</span></p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>3</strong> &ndash; Clique em &ldquo; Cadastrar&rdquo;.</span></p>

<p>&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><img src="tutorial/tuto1.png" width="100%" /></span></p>

<p>&nbsp;</p>

<p><u><strong><span style="font-family:Arial,sans-serif">2. Listagens</span></strong></u></p>

<p>&nbsp;</p>

<p><span style="font-family:Arial,sans-serif">Em todos os links do menu exceto o &ldquo;site&rdquo;, aparecer&atilde;o primeiramente listagens do conte&uacute;do acessado. Nessas listagens ser&aacute; poss&iacute;vel realizar buscas instant&acirc;neas no campo &ldquo;Buscar aqui&rdquo; e visualizar informa&ccedil;&otilde;es do conte&uacute;do listado. </span></p>

<p>&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><img src="tutorial/tuto2.png" width="100%" /></span></p>

<p>&nbsp;</p>

<p><u><strong><span style="font-family:Arial,sans-serif">3. Edi&ccedil;&otilde;es</span></strong></u></p>

<p>&nbsp;</p>

<p><span style="font-family:Arial,sans-serif">Em todas as listagens, aparecer&aacute; o bot&atilde;o &ldquo;Editar&rdquo;, quando voc&ecirc; clicar no mesmo ser&aacute; poss&iacute;vel abrir o formul&aacute;rio espec&iacute;fico retornando todos os dados anteriormente cadastrados e poder&aacute; ser realiza a atualiza&ccedil;&atilde;o dos dados e para confirmar clique no bot&atilde;o &ldquo;Atualizar&rdquo;.</span></p>

<p>&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><img src="tutorial/tuto3.png" width="100%" /></span></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><span style="font-family:Arial,sans-serif">Voc&ecirc; poder&aacute; atualizar as suas informa&ccedil;&otilde;es pessoais indo no painel superior do sistema e clicando no &iacute;cone de &ldquo;engrenagem&rdquo;, como mostra a imagem a seguir.</span></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><img src="tutorial/tuto3-1.png" width="100%" /></span></p>

<p>&nbsp;</p>

<p><strong><span style="font-family:Arial,sans-serif">4. Exclus&otilde;es</span></strong></p>

<p>&nbsp;</p>

<p><span style="font-family:Arial,sans-serif">Para realizar exclus&atilde;o de algum item do sistema voc&ecirc; dever&aacute; entrar em &ldquo;Editar&rdquo; e clicar no bot&atilde;o &ldquo;Excluir&rdquo;. <span style="color:#ff0000">Aten&ccedil;&atilde;o! Tome cuidado com as exclus&otilde;es, s&oacute; exclua algum registro se realmente tiver certeza.</span> </span></p>

<p>&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><img src="tutorial/tuto4.png" width="100%" /></span></p>

<p>&nbsp;</p>
      
      
      
      
      
      
  </div>
</div>
                    </div>

                </div>
        <hr/>
        
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#tuto2" aria-expanded="false" aria-controls="mostraocultaperguntas" title="Veja como está ficando essa enquete">
                    <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true" title="Mostrar/Ocultar"></span> Turmas
                </button>
                <div class="collapse" id="tuto2">
                    <div class="well">

                        <div class="panel panel-default">
  <div class="panel-body">
   
      
      
      <p><span style="font-family:Arial,sans-serif">No Menu &gt; Turmas existe um bot&atilde;o &ldquo;vincular alunos a uma turma&rdquo; que realizar&aacute; exatamente o que ele prop&otilde;e. </span></p>

<p>&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>Instru&ccedil;&otilde;es:</strong></span></p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>1</strong> &ndash; Clique sobre o bot&atilde;o.</span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>2</strong> &ndash; Selecione a Turma.</span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>3</strong> &ndash; Clique no bot&atilde;o &ldquo;Vincular&rdquo;.</span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>4</strong> &ndash; Caso voc&ecirc; tenha vinculado um aluno em uma turma errada &eacute; s&oacute; clicar no bot&atilde;o &ldquo;Desvincular&rdquo;, n&atilde;o esquecendo de selecionar a turma correta como &eacute; descrito no item <strong>2</strong>.</span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>Obs:</strong> (Caso n&atilde;o apare&ccedil;a nenhum aluno &eacute; porque todos os mesmos est&atilde;o vinculados a uma turma ou n&atilde;o existe nenhum aluno dispon&iacute;vel).</span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><img src="tutorial/tuto5.png" width="100%" /></span></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

      
      
      
  </div>
</div>
                    </div>

                </div>
        <hr/>
        
        
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#tuto3" aria-expanded="false" aria-controls="mostraocultaperguntas" title="Veja como está ficando essa enquete">
                    <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true" title="Mostrar/Ocultar"></span> Enquentes
                </button>
                <div class="collapse" id="tuto3">
                    <div class="well">

                        <div class="panel panel-default">
  <div class="panel-body">

      
      
      <p><span style="font-family:Arial,sans-serif">No Menu &gt; Enquetes voc&ecirc; tem uma visualiza&ccedil;&atilde;o diferente em sua listagem, a mesma possui 2 bot&otilde;es diferentes ao editar da maioria um &eacute; o &ldquo;Explorar&rdquo; e o outro &eacute; &ldquo;Exibir Gr&aacute;ficos&rdquo;.</span></p>

<p>&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><img src="tutorial/tuto6.png" width="100%" /></span></p>

<p>&nbsp;</p>

<p><u><strong><span style="font-family:Arial,sans-serif">1. Explorar</span></strong></u></p>

<p>&nbsp;</p>

<p><span style="font-family:Arial,sans-serif">Quando voc&ecirc; clicar em &ldquo;Explorar&rdquo; voc&ecirc; ter&aacute; 3 op&ccedil;&otilde;es diferentes: A primeira &eacute; a possibilita de editar a enquete e deix&aacute;-la <span style="color:#009933"><strong>online</strong></span> ou <span style="color:#ff3333"><strong>offline</strong></span>. A segunda &eacute; voc&ecirc; poder cadastrar perguntas e op&otilde;es de respostas para a enquete. A terceira e &uacute;ltima &eacute; de poder visualizar como esta enquete ficar&aacute; e como o aluno ir&aacute; enxerg&aacute;-la pra responder.</span></p>

<p>&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>Instru&ccedil;&otilde;es:</strong></span></p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>1.1 </strong>&ndash;<strong> </strong>Clique no bot&atilde;o &ldquo;Edi&ccedil;&atilde;o de Enquete&rdquo; para atualizar as informa&ccedil;&otilde;es sobre ela.</span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><img src="tutorial/tuto7.png" width="100%" /></span></p>

<p>&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>1.2 </strong>&ndash; Clique no bot&atilde;o &ldquo;Perguntas dessa Enquete&rdquo; para cadastrar perguntas. Voc&ecirc; poder&aacute; tamb&eacute;m exclu&iacute;-las apenas clicando no bot&atilde;o &ldquo;Excluir&rdquo;. </span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><img src="tutorial/tuto8.png" width="100%" /></span></p>

<p>&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>1.2.1</strong> &ndash; Cadastre Respostas para perguntas j&aacute; inseridas:</span></p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>1.2.1.1</strong> &ndash; Selecione uma Pergunta.</span></p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>1.2.2.2</strong> &ndash; Inclua op&ccedil;&otilde;es a Pergunta.</span></p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>1.</strong><strong>2.</strong><strong>3</strong><strong>.3</strong> &ndash; Exclua op&ccedil;&otilde;es da pergunta selecionada.</span></p>

<p>&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><img src="tutorial/tuto10.png" width="100%" /></span></p>

<p>&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>1.3 </strong> - Clique no bot&atilde;o &ldquo;Preview Enquete&rdquo; para visualizar como a enquete est&aacute; ficando.</span></p>

<p>&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><img src="tutorial/tuto9.png" width="100%" /></span></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><u><strong><span style="font-family:Arial,sans-serif">2. Exibir Gr&aacute;ficos</span></strong></u></p>

<p>&nbsp;</p>

<p><span style="font-family:Arial,sans-serif">Em Menu &gt; Enquetes, na listagem de enquetes al&eacute;m do bot&atilde;o &ldquo;Explorar&rdquo; existe o bot&atilde;o &ldquo;Exibir Gr&aacute;ficos&rdquo; no qual &eacute; poss&iacute;vel visualizar gr&aacute;ficos que ser&atilde;o gerados a medida que os alunos estiverem respondendo as mesmas.</span></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><img src="tutorial/tuto13.png" width="100%" /></span></p>

      
      
      
  </div>
</div>
                    </div>

                </div>
        <hr/>

        
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#tuto4" aria-expanded="false" aria-controls="mostraocultaperguntas" title="Veja como está ficando essa enquete">
                    <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true" title="Mostrar/Ocultar"></span> Fórum
                </button>
                <div class="collapse" id="tuto4">
                    <div class="well">

                        <div class="panel panel-default">
  <div class="panel-body">

      
      <p><span style="font-family:Arial,sans-serif">Em Menu &gt; F&oacute;rum, na listagem cont&eacute;m um bot&atilde;o chamado &ldquo;Entrar&rdquo; no qual ser&aacute; poss&iacute;vel entrar na discuss&atilde;o do t&oacute;pico criado e interagir na discuss&atilde;o.</span></p>

<p>&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>Funcionalidades do F&oacute;rum:</strong></span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>1</strong> &ndash; Responder ao T&oacute;pico Principal</span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>2</strong> &ndash; &ldquo;Editar&rdquo; voc&ecirc; poder&aacute; editar sua resposta ou se voc&ecirc; for administrador do sistema poder&aacute; ter controle da edi&ccedil;&atilde;o de todas as postagens.</span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>3</strong> &ndash; &ldquo;Responder&rdquo; voc&ecirc; poder&aacute; responder a todos os usu&aacute;rios que desejar.</span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>4</strong> &ndash; Quando o quadro da postagem estiver <span style="color:#007826"><strong>verde</strong></span> &eacute; porque ele foi marcado como uma das melhores respostas.</span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><strong>5 </strong>&ndash; &ldquo;Melhor&rdquo;, esse bot&atilde;o s&oacute; aparecer&aacute; para o professor ou administrador do sistema para marcar as melhores respostas.</span></p>

<p style="text-align:center"><span style="font-family:Arial,sans-serif"><img src="tutorial/tuto12.png" width="100%" /></span></p>

<p>&nbsp;</p>

      
      
  </div>
</div>
                    </div>

                </div>
        <hr/>



        <!--            <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                This is a simple admin template that can be used for your small project or may be large projects. This is free for personal and commercial use.
                            </div>
                        </div>
        
                    </div>
                    <div class="row">
                         <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="dashboard-div-wrapper bk-clr-one">
                                <i  class="fa fa-venus dashboard-div-icon" ></i>
                                <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
          </div>
                                   
        </div>
                                 <h5>Simple Text Here </h5>
                            </div>
                        </div>
                         <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="dashboard-div-wrapper bk-clr-two">
                                <i  class="fa fa-edit dashboard-div-icon" ></i>
                                <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
          </div>
                                   
        </div>
                                 <h5>Simple Text Here </h5>
                            </div>
                        </div>
                         <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="dashboard-div-wrapper bk-clr-three">
                                <i  class="fa fa-cogs dashboard-div-icon" ></i>
                                <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
          </div>
                                   
        </div>
                                 <h5>Simple Text Here </h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="dashboard-div-wrapper bk-clr-four">
                                <i  class="fa fa-bell-o dashboard-div-icon" ></i>
                                <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
          </div>
                                   
        </div>
                                 <h5>Simple Text Here </h5>
                            </div>
                        </div>
        
                    </div>
                   
                    <div class="row">
                        <div class="col-md-6">
                              <div class="notice-board">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                   Active  Notice Panel 
                                        <div class="pull-right" >
                                            <div class="dropdown">
          <button class="btn btn-success dropdown-toggle btn-xs" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
            <span class="glyphicon glyphicon-cog"></span>
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Refresh</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Logout</a></li>
          </ul>
        </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                       
                                        <ul >
                                           
                                             <li>
                                                    <a href="#">
                                             <span class="glyphicon glyphicon-align-left text-success" ></span> 
                                                          Lorem ipsum dolor sit amet ipsum dolor sit amet
                                                         <span class="label label-warning" > Just now </span>
                                                    </a>
                                            </li>
                                             <li>
                                                  <a href="#">
                                             <span class="glyphicon glyphicon-info-sign text-danger" ></span>  
                                                  Lorem ipsum dolor sit amet ipsum dolor sit amet
                                                  <span class="label label-info" > 2 min chat</span>
                                                    </a>
                                            </li>
                                             <li>
                                                  <a href="#">
                                             <span class="glyphicon glyphicon-comment  text-warning" ></span>  
                                                  Lorem ipsum dolor sit amet ipsum dolor sit amet
                                                  <span class="label label-success" >GO ! </span>
                                                    </a>
                                            </li>
                                            <li>
                                                  <a href="#">
                                             <span class="glyphicon glyphicon-edit  text-danger" ></span>  
                                                  Lorem ipsum dolor sit amet ipsum dolor sit amet
                                                  <span class="label label-success" >Let's have it </span>
                                                    </a>
                                            </li>
                                           </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="panel-footer">
                                        <a href="#" class="btn btn-default btn-block"> <i class="glyphicon glyphicon-repeat"></i> Just A Small Footer Button</a>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="text-center alert alert-warning">
                                <a href="#" class="btn btn-social btn-facebook">
                                    <i class="fa fa-facebook"></i>&nbsp; Facebook</a>
                                <a href="#" class="btn btn-social btn-google">
                                    <i class="fa fa-google-plus"></i>&nbsp; Google</a>
                                <a href="#" class="btn btn-social btn-twitter">
                                    <i class="fa fa-twitter"></i>&nbsp; Twitter </a>
                                <a href="#" class="btn btn-social btn-linkedin">
                                    <i class="fa fa-linkedin"></i>&nbsp; Linkedin </a>
                            </div>
                             
                            <hr />
                             
                        </div>
                        <div class="col-md-6">
                            <div class="alert alert-danger">
                                This is a simple admin template that can be used for your small project or may be large projects. This is free for personal and commercial use.
                            </div>
                            <hr />
                             <div class="Compose-Message">               
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                Compose New Message 
                            </div>
                            <div class="panel-body">
                                
                                <label>Enter Recipient Name : </label>
                                <input type="text" class="form-control" />
                                <label>Enter Subject :  </label>
                                <input type="text" class="form-control" />
                                <label>Enter Message : </label>
                                <textarea rows="9" class="form-control"></textarea>
                                <hr />
                                <a href="#" class="btn btn-warning"><span class="glyphicon glyphicon-envelope"></span> Send Message </a>&nbsp;
                              <a href="#" class="btn btn-success"><span class="glyphicon glyphicon-tags"></span>  Save To Drafts </a>
                            </div>
                            <div class="panel-footer text-muted">
                                <strong>Note : </strong>Please note that we track all messages so don't send any spams.
                            </div>
                        </div>
                             </div>
                        </div>
                    </div>-->
    </div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->

<?php
include_once 'rodape.php';
