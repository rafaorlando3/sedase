<?php
$menu = 1;
include_once 'cabecalho.php';
?>

<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Resumos do Sistema</h4>

            </div>
            <div class="text-right pull-right"><label>Buscar aqui: </label> <input  type="text" id="busca" value=""  /> </div>


        </div>


        <h4>Pedidos de Solicitações de Cadastro </h4>



        <div id="resposta"></div> 


        <div id="listausuarios">


        </div> 

        <hr/>
        <div class="alert alert-warning alert-dismissible fade in" role="alert"><strong>Aviso:</strong> Os gráficos aparecerão a medida que o sistema receba novos cadastros e informações.</div>
      
        <div class="row">
        <div class="text-center pull-center">
            
        <div class="col-md-6 pull-left" style="width:32.5%">
            <h3 class="text-center">Usuários Ativos</h3>
            <canvas id="usuariosativos"></canvas>
        </div>

        <div class="col-md-9 pull-center" style="width:32.5%">
            <h3 class="text-center">Turmas no Sistema</h3>
            <canvas id="turmas"></canvas> 

        </div>
                
        <div class="col-md-6 pull-right" style="width:32.5%">
            <h3 class="text-center">Enquetes</h3>
            <canvas id="enquetes"></canvas>
        </div>
            
             <div class="col-md-6 pull-center" style="width:32.5%">
            <h3 class="text-center">Materiais</h3>
            <canvas id="materiais"></canvas>
        </div>
            
       <div class="col-md-6 pull-center" style="width:32.5%">
            <h3 class="text-center">Fórum</h3>
            <canvas id="forum"></canvas>
        </div>
            
        <div class="col-md-6 pull-center" style="width:32.5%">
            <h3 class="text-center">Respostas do Fórum</h3>
            <canvas id="respostas"></canvas>
        </div>

        <script>
            var usuariosa = {
                type: 'pie',
                data: {
                    datasets: [{
                            data: [
                                <?= quantUsu(1, 't'); ?>,
                                <?= quantUsu(2, 't'); ?>,
                                <?= quantUsu(0, 't'); ?>
                            ],
                            backgroundColor: [
                                window.chartColors.red,
                                window.chartColors.orange,
                                window.chartColors.yellow
                            ],
                            label: 'Dataset 1'
                        }],
                    labels: [
                        "Administrador",
                        "Professor",
                        "Aluno"
                    ]
                },
                options: {
                    responsive: true
                }
            };

            var turmas = {
                type: 'pie',
                data: {
                    datasets: [{
                            data: [
                                <?=  quantTurmas(1)?>,
                                <?=  quantTurmas(2)?>
                            ],
                            backgroundColor: [
                                window.chartColors.purple,
                                window.chartColors.blue
                            ],
                            label: 'Dataset 2'
                        }],
                    labels: [
                        "Ensino Fundamental",
                        "Ensino médio",
                    ]
                },
                options: {
                    responsive: true
                }
            };
            
            
            var enquetes = {
                type: 'pie',
                data: {
                    datasets: [{
                            data: [
                                <?= quantEnquetes(1)?>,
                                <?= quantEnquetes(0)?>
                            ],
                            backgroundColor: [
                                window.chartColors.green,
                                window.chartColors.red
                            ],
                            label: 'Dataset 2'
                        }],
                    labels: [
                        "Online",
                        "Offline",
                    ]
                },
                options: {
                    responsive: true
                }
            };
            
            var materiais = {
                type: 'pie',
                data: {
                    datasets: [{
                            data: [
                                <?= quantMateriais("where tipo=1 or tipo=0")?>,
                                <?= quantMateriais("where idturma is NULL")?>
                            ],
                            backgroundColor: [
                                window.chartColors.grey,
                                window.chartColors.orange
                            ],
                            label: 'Dataset 2'
                        }],
                    labels: [
                        "Privado",
                        "Público",
                    ]
                },
                options: {
                    responsive: true
                }
            };
            
            var forum = {
                type: 'pie',
                data: {
                    datasets: [{
                            data: [
                                <?= quantForum("")?>,
                                <?=quantMateriais("where tipo=1 or tipo=0")?>,
                                <?=quantMateriais("where idturma is NULL")?>                        
                            ],
                            backgroundColor: [
                                window.chartColors.green,
                                window.chartColors.yellow,
                                window.chartColors.red
                            ],
                            label: 'Dataset 2'
                        }],
                    labels: [
                        "Total de Cadastros",
                        "Quantidade para Publico",
                        "Quantidade para Privado",
                    ]
                },
                options: {
                    responsive: true
                }
            };
            
            
            var respostas = {
                type: 'pie',
                data: {
                    datasets: [{
                            data: [
                                <?= quantRespostas("")?>,
                                <?= quantRespostas("where melhoresposta='t'")?>,
                                <?= quantRespostas("where citacao is not NULL")?>                        
                            ],
                            backgroundColor: [
                                window.chartColors.blue,
                                window.chartColors.green,
                                window.chartColors.yellow
                            ],
                            label: 'Dataset 2'
                        }],
                    labels: [
                        "Total de Respostas",
                        "Melhores Respostas",
                        "Citacões",
                    ]
                },
                options: {
                    responsive: true
                }
            };
            


            window.onload = function () {
                var ctxUa = document.getElementById("usuariosativos").getContext("2d");
                var turm = document.getElementById("turmas").getContext("2d");
                var enqu = document.getElementById("enquetes").getContext("2d");
                var mat = document.getElementById("materiais").getContext("2d");
                var foru = document.getElementById("forum").getContext("2d");
                var resp = document.getElementById("respostas").getContext("2d");

                window.myPie = new Chart(ctxUa, usuariosa);
                window.myPie = new Chart(turm, turmas);
                window.myPie = new Chart(enqu, enquetes);
                window.myPie = new Chart(mat, materiais);
                window.myPie = new Chart(foru, forum);
                window.myPie = new Chart(resp, respostas);
            };

        </script>

</div>
            
            </div>


        <!--            <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                This is a simple admin template that can be used for your small project or may be large projects. This is free for personal and commercial use.
                            </div>
                        </div>
        
                    </div>
                    <div class="row">
                         <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="dashboard-div-wrapper bk-clr-one">
                                <i  class="fa fa-venus dashboard-div-icon" ></i>
                                <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
          </div>
                                   
        </div>
                                 <h5>Simple Text Here </h5>
                            </div>
                        </div>
                         <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="dashboard-div-wrapper bk-clr-two">
                                <i  class="fa fa-edit dashboard-div-icon" ></i>
                                <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
          </div>
                                   
        </div>
                                 <h5>Simple Text Here </h5>
                            </div>
                        </div>
                         <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="dashboard-div-wrapper bk-clr-three">
                                <i  class="fa fa-cogs dashboard-div-icon" ></i>
                                <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
          </div>
                                   
        </div>
                                 <h5>Simple Text Here </h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="dashboard-div-wrapper bk-clr-four">
                                <i  class="fa fa-bell-o dashboard-div-icon" ></i>
                                <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
          </div>
                                   
        </div>
                                 <h5>Simple Text Here </h5>
                            </div>
                        </div>
        
                    </div>
                   
                    <div class="row">
                        <div class="col-md-6">
                              <div class="notice-board">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                   Active  Notice Panel 
                                        <div class="pull-right" >
                                            <div class="dropdown">
          <button class="btn btn-success dropdown-toggle btn-xs" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
            <span class="glyphicon glyphicon-cog"></span>
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Refresh</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Logout</a></li>
          </ul>
        </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                       
                                        <ul >
                                           
                                             <li>
                                                    <a href="#">
                                             <span class="glyphicon glyphicon-align-left text-success" ></span> 
                                                          Lorem ipsum dolor sit amet ipsum dolor sit amet
                                                         <span class="label label-warning" > Just now </span>
                                                    </a>
                                            </li>
                                             <li>
                                                  <a href="#">
                                             <span class="glyphicon glyphicon-info-sign text-danger" ></span>  
                                                  Lorem ipsum dolor sit amet ipsum dolor sit amet
                                                  <span class="label label-info" > 2 min chat</span>
                                                    </a>
                                            </li>
                                             <li>
                                                  <a href="#">
                                             <span class="glyphicon glyphicon-comment  text-warning" ></span>  
                                                  Lorem ipsum dolor sit amet ipsum dolor sit amet
                                                  <span class="label label-success" >GO ! </span>
                                                    </a>
                                            </li>
                                            <li>
                                                  <a href="#">
                                             <span class="glyphicon glyphicon-edit  text-danger" ></span>  
                                                  Lorem ipsum dolor sit amet ipsum dolor sit amet
                                                  <span class="label label-success" >Let's have it </span>
                                                    </a>
                                            </li>
                                           </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="panel-footer">
                                        <a href="#" class="btn btn-default btn-block"> <i class="glyphicon glyphicon-repeat"></i> Just A Small Footer Button</a>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="text-center alert alert-warning">
                                <a href="#" class="btn btn-social btn-facebook">
                                    <i class="fa fa-facebook"></i>&nbsp; Facebook</a>
                                <a href="#" class="btn btn-social btn-google">
                                    <i class="fa fa-google-plus"></i>&nbsp; Google</a>
                                <a href="#" class="btn btn-social btn-twitter">
                                    <i class="fa fa-twitter"></i>&nbsp; Twitter </a>
                                <a href="#" class="btn btn-social btn-linkedin">
                                    <i class="fa fa-linkedin"></i>&nbsp; Linkedin </a>
                            </div>
                             
                            <hr />
                             
                        </div>
                        <div class="col-md-6">
                            <div class="alert alert-danger">
                                This is a simple admin template that can be used for your small project or may be large projects. This is free for personal and commercial use.
                            </div>
                            <hr />
                             <div class="Compose-Message">               
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                Compose New Message 
                            </div>
                            <div class="panel-body">
                                
                                <label>Enter Recipient Name : </label>
                                <input type="text" class="form-control" />
                                <label>Enter Subject :  </label>
                                <input type="text" class="form-control" />
                                <label>Enter Message : </label>
                                <textarea rows="9" class="form-control"></textarea>
                                <hr />
                                <a href="#" class="btn btn-warning"><span class="glyphicon glyphicon-envelope"></span> Send Message </a>&nbsp;
                              <a href="#" class="btn btn-success"><span class="glyphicon glyphicon-tags"></span>  Save To Drafts </a>
                            </div>
                            <div class="panel-footer text-muted">
                                <strong>Note : </strong>Please note that we track all messages so don't send any spams.
                            </div>
                        </div>
                             </div>
                        </div>
                    </div>-->
    </div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->

<script type="text/javascript">

    function atualizausuarios() {
        $.get('Selects/carregasolicitacoes.php', function (resultado) {
            $('#listausuarios').html(resultado);

        });
    }
    atualizausuarios();


    $("#busca").keypress(function () {
        var pesquisa = $('#busca').val();

        $.get('Selects/carregasolicitacoes.php?pesquisa=' + pesquisa, function (resultado) {
            $('#listausuarios').html(resultado);

        });

    });


</script>


<?php
include_once 'rodape.php';
