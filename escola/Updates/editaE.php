<?php
include_once '../../class/Carrega.class.php';

if (isset($_POST["titulo"], $_POST["descricao"], $_POST["data"], $_POST["idusuario"], $_POST["tipo"], $_POST["status"], $_POST["id"], $_POST["idturma"])) {

        $objEnquetes = new Enquetes();
        $objEnquetes->id = pg_escape_string($_POST["id"]);
        $objEnquetes->titulo = pg_escape_string($_POST["titulo"]);
        $objEnquetes->genero = pg_escape_string($_POST["descricao"]);
        $objEnquetes->data = pg_escape_string($_POST["data"]);
        $objEnquetes->tipo = $_POST["tipo"];
        $objEnquetes->idturma = $_POST["idturma"];
        $objEnquetes->status = $_POST["status"];
        $objEnquetes->idusuario = $_POST["idusuario"];
        $objEnquetes->atualizar();
       
        echo "<div class='alert alert-success'> Enquete Atualizada com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
    }