<?php
include_once '../../class/Carrega.class.php';

if (isset($_POST["idusuario"], $_POST["idescola"], $_POST["idturma"], $_POST["nome"], $_POST["grau"], $_POST["ano"])) {

        $objTurmas = new Turma();
        $objTurmas->id = pg_escape_string($_POST["idturma"]);
        $objTurmas->nome = pg_escape_string($_POST["nome"]);
        $objTurmas->grau = pg_escape_string($_POST["grau"]);
        $objTurmas->ano = pg_escape_string($_POST["ano"]);
        $objTurmas->idescola = $_POST["idescola"];
        $objTurmas->idusuario = $_POST["idusuario"];
        $objTurmas->atualizar();
       
        echo "<div class='alert alert-success'> Turma Atualizada com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
    }