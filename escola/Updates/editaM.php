<?php

date_default_timezone_set("Brazil/East"); 

include_once '../../class/Carrega.class.php';

$objMateriais = new Materiais();

if (isset($_POST["titulo"], $_POST["conteudo"], $_POST["idturma"], $_POST["idusuario"],$_POST["id"])) {
    
    $objMateriais->id=$_POST["id"];
    $item = $objMateriais->retornarunico();
            
    
    if (!empty($_FILES['imagem']['name']) && !empty($_FILES['anexo']['name'])) {

      $ext = strtolower(substr($_FILES['imagem']['name'],-4)); //Pegando extensão do arquivo
      $new_name = date("Y.m.d-H.i.s") . $ext; //Definindo um novo nome para o arquivo
      $dir = '../../uploads/imagens/'; //Diretório para uploads
      move_uploaded_file($_FILES['imagem']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
      
      $excluiImagem = "../../uploads/imagens/" . $item->imagem . "";
            if (file_exists($excluiImagem)) {
                unlink($excluiImagem);
            }
      
        
      $ext2 = strtolower(substr($_FILES['anexo']['name'],-4)); //Pegando extensão do arquivo
      $new_name2 = date("Y.m.d-H.i.s") . $ext2; //Definindo um novo nome para o arquivo
      $dir2 = '../../uploads/anexos/'; //Diretório para uploads
      move_uploaded_file($_FILES['anexo']['tmp_name'], $dir2.$new_name2); //Fazer upload do arquivo
      
         $excluiAnexo = "../../uploads/anexos/" . $item->anexo . "";
            if (file_exists($excluiAnexo)) {
                unlink($excluiAnexo);
            }
            
            
            $objMateriais->titulo = pg_escape_string($_POST["titulo"]);
            $objMateriais->descricao = htmlspecialchars_decode($_POST["conteudo"]);
            $objMateriais->idturma = pg_escape_string($_POST["idturma"]);
            $objMateriais->anexo = $new_name2;
            $objMateriais->imagem = $new_name;
            $objMateriais->video = pg_escape_string($_POST["video"]);
            $objMateriais->idusuario = pg_escape_string($_POST["idusuario"]);
            $objMateriais->tipo = pg_escape_string($_POST["tipo"]);
            $objMateriais->atualizar();      
      
      
     echo "<div class='alert alert-success'> Conteúdo atualizado com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button></div>";
        
        
    } elseif (!empty($_FILES['anexo']['name'])) {
             
      $ext2 = strtolower(substr($_FILES['anexo']['name'],-4)); //Pegando extensão do arquivo
      $new_name2 = date("Y.m.d-H.i.s") . $ext2; //Definindo um novo nome para o arquivo
      $dir2 = '../../uploads/anexos/'; //Diretório para uploads
      move_uploaded_file($_FILES['anexo']['tmp_name'], $dir2.$new_name2); //Fazer upload do arquivo 
   
         $excluiAnexo = "../../uploads/anexos/" . $item->anexo . "";
            if (file_exists($excluiAnexo)) {
                unlink($excluiAnexo);
            }
            
            $objMateriais->titulo = pg_escape_string($_POST["titulo"]);
            $objMateriais->descricao = htmlspecialchars_decode($_POST["conteudo"]);
            $objMateriais->idturma = pg_escape_string($_POST["idturma"]);
            $objMateriais->anexo = $new_name2;
            $objMateriais->imagem = $item->imagem;
            $objMateriais->video = pg_escape_string($_POST["video"]);
            $objMateriais->idusuario = pg_escape_string($_POST["idusuario"]);
            $objMateriais->tipo = pg_escape_string($_POST["tipo"]);
            $objMateriais->atualizar();         
      
      
    echo "<div class='alert alert-success'> Conteúdo atualizado com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button></div>";
        
      
    } elseif (!empty($_FILES['imagem']['name'])) {

      $ext = strtolower(substr($_FILES['imagem']['name'],-4)); //Pegando extensão do arquivo
      $new_name = date("Y.m.d-H.i.s") . $ext; //Definindo um novo nome para o arquivo
      $dir = '../../uploads/imagens/'; //Diretório para uploads
      move_uploaded_file($_FILES['imagem']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
      
            $excluiImagem = "../../uploads/imagens/" . $item->imagem . "";
            if (file_exists($excluiImagem)) {
                unlink($excluiImagem);
            }
      
      
            $objMateriais->titulo = pg_escape_string($_POST["titulo"]);
            $objMateriais->descricao = htmlspecialchars_decode($_POST["conteudo"]);
            $objMateriais->idturma = pg_escape_string($_POST["idturma"]);
            $objMateriais->anexo =$item->anexo;
            $objMateriais->imagem = $new_name;
            $objMateriais->video = pg_escape_string($_POST["video"]);
            $objMateriais->idusuario = pg_escape_string($_POST["idusuario"]);
            $objMateriais->tipo = pg_escape_string($_POST["tipo"]);
            $objMateriais->atualizar();   
      
      
            echo "<div class='alert alert-success'> Conteúdo atualizado com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button></div>";
        
    } else {
        
       
            $objMateriais->titulo = pg_escape_string($_POST["titulo"]);
            $objMateriais->descricao = htmlspecialchars_decode($_POST["conteudo"]);
            $objMateriais->idturma = pg_escape_string($_POST["idturma"]);
            $objMateriais->anexo =$item->anexo;
            $objMateriais->imagem = $item->imagem;
            $objMateriais->video = pg_escape_string($_POST["video"]);
            $objMateriais->idusuario = pg_escape_string($_POST["idusuario"]);
            $objMateriais->tipo = pg_escape_string($_POST["tipo"]);
            $objMateriais->atualizar();   


        echo "<div class='alert alert-success'> Conteúdo atualizado com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button></div>";
    }
}
