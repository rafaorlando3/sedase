<?php
session_start();
include_once '../../class/Carrega.class.php';

if (isset($_POST["nome"], $_POST["data"], $_POST["cidade"], $_POST["estado"], $_POST["email"], $_POST["senha"], $_POST["idusuario"],$_POST["ativo"])) {
   $objUsuarios = new Usuarios();
    $nome = pg_escape_string($_POST["nome"]);
    $email = pg_escape_string($_POST["email"]);
    $data = pg_escape_string($_POST["data"]);
    $senha = pg_escape_string($_POST["senha"]);
    $idusuario=pg_escape_string($_POST["idusuario"]);
        
        require("../../class/phpmailer/class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->IsSMTP(); // Define que a mensagem será SMTP
        $mail->Host = "smtp.santanacontabilidade.com"; // Endereço do servidor SMTP
        $mail->SMTPAuth = true; // Autenticação
        $mail->Username = 'sedase@santanacontabilidade.com'; // Usuário do servidor SMTP
        $mail->Password = 'sedase2255'; // Senha da caixa postal utilizada
        $mail->From = "sedase@santanacontabilidade.com";
        $mail->FromName = "Sistema SEDASE";

        $mail->AddAddress($email, $nome);
        $mail->AddAddress($email);
        $mail->AddCC('sedase@getsitesolucoes.com.br', 'Copia');
        //$mail->AddBCC('rafaorlando3@gmail.com', 'Copia Oculta');
        $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
        $mail->CharSet = 'UTF-8';

        $mail->Subject = "Atualização de cadastro no Sistema - SEDASE"; // Assunto da mensagem
        $mail->Body = 'O Administrador do sistema <b>SEDASE</b> atualizou seu cadastro, abaixo confira suas informacões de login.<br/> <br/> <b>Login:</b> ' . $email . ' <br/> <b>Senha:</b> ' . $senha . ' <br/><br/>  Acesse o sistema, <br> <center><a href="http://sedase.santanacontabilidade.com" target="_blank"><img src="http://sedase.santanacontabilidade.com/images/logoemailsedase.png" width="30%"/></a></center> <br/> Link SEDASE: http://www.sedase.santanacontabilidade.com <br/><br/> Não responda esse e-mail, ele é automático. ';
        $mail->AltBody = 'Você atualizou seu cadastrado no sistema <b>SEDASE</b>, abaixo confira suas informacões de login.<br/> <br/> <b>Login:</b> ' . $email . ' <br/> <b>Senha:</b> ' . $senha . ' <br/><br/> Acesse o sistema, <br> <center><a href="http://sedase.santanacontabilidade.com" target="_blank"><img src="http://sedase.santanacontabilidade.com/images/logoemailsedase.png" width="30%"/></a></center> <br/> Link SEDASE: http://www.sedase.santanacontabilidade.com <br/><br/> Não responda esse e-mail, ele é automático.';
        $enviado = $mail->Send();
        $mail->ClearAllRecipients();
        $mail->ClearAttachments();

        if ($enviado) {
        $objUsuarios->id = $idusuario;
        $objUsuarios->nome = $nome;
        $objUsuarios->data = $data;
        $objUsuarios->cidade = pg_escape_string($_POST["cidade"]);
        $objUsuarios->estado = pg_escape_string($_POST["estado"]);
        $objUsuarios->genero = pg_escape_string($_POST["genero"]);
        $objUsuarios->email = $email;
        $objUsuarios->senha = base64_encode($senha);
        $objUsuarios->tipo = pg_escape_string($_POST["tipo"]);
        $objUsuarios->ativo = pg_escape_string($_POST["ativo"]);
        $objUsuarios->imagem = null;
        $objUsuarios->atualizar();

         echo "<div class='alert alert-success'> Atualização realizada com Sucesso! Um e-mail foi enviado para <strong>$email</strong> registrando essa atualização. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
        }
        else {
            echo "<div class='alert alert-danger'> A atualização falhou devido a algum problema em nosso servidor de email, brevemente solucionaremos esse problema. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
            echo "Informações do erro: " . $mail->ErrorInfo;
        }
}
 else {
        echo "<div class='alert alert-danger'>Erro ao atualizar informações. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
    }
