<?php

include_once '../../class/Carrega.class.php';
date_default_timezone_set("Brazil/East");

if (isset($_POST["titulo"], $_POST["descricao"], $_POST["idusuario"], $_POST["idturma"],$_POST["id"],$_POST["resolvido"])) {
    $objForum = new Forum();
    $objForum->id=pg_escape_string($_POST['id']);
    $item = $objForum->retornarunico();
    
    if (!empty($_FILES['imagem']['name'])) {

        $ext = strtolower(substr($_FILES['imagem']['name'], -4)); //Pegando extensão do arquivo
        $new_name = date("Y.m.d-H.i.s") . $ext; //Definindo um novo nome para o arquivo
        $dir = '../../uploads/imagens/'; //Diretório para uploads
        move_uploaded_file($_FILES['imagem']['tmp_name'], $dir . $new_name); //Fazer upload do arquivo
        
         $excluiImagem = "../../uploads/imagens/" . $item->imagem . "";
            if (file_exists($excluiImagem)) {
                unlink($excluiImagem);
            }
      

        $objForum->titulo = pg_escape_string($_POST["titulo"]);
        $objForum->texto = pg_escape_string($_POST["descricao"]);
        $objForum->imagem = $new_name;
        $objForum->data = $item->data;
        $objForum->idturma = pg_escape_string($_POST["idturma"]);
        $objForum->resolvido = pg_escape_string($_POST["resolvido"]);
        $objForum->idusuario = pg_escape_string($_POST["idusuario"]);
        $objForum->tipo = $_POST["tipo"];
        $objForum->atualizar();

         echo "<div class='alert alert-success'> Enquete Atualizada com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
    } else {

        $objForum->titulo = pg_escape_string($_POST["titulo"]);
        $objForum->texto = pg_escape_string($_POST["descricao"]);
        $objForum->imagem = $item->imagem;
        $objForum->data = $item->data;
        $objForum->idturma = pg_escape_string($_POST["idturma"]);
        $objForum->resolvido = pg_escape_string($_POST["resolvido"]);
        $objForum->idusuario = pg_escape_string($_POST["idusuario"]);
        $objForum->tipo = $_POST["tipo"];
        $objForum->atualizar();

        echo "<div class='alert alert-success'> Enquete Atualizada com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
    }
}