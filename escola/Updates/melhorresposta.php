<?php
include_once '../../class/Carrega.class.php';
session_start();
date_default_timezone_set("Brazil/East");


if (isset($_POST["idresposta"], $_POST["idusuario"])) {

    $respostas = htmlspecialchars_decode($_POST["respostas"]);

    $objRespostas = new Respostas();
    $objRespostas->id = pg_escape_string($_POST["idresposta"]);
    $itemresposta=$objRespostas->retornarunico();
   
   if($itemresposta->citacao!=NULL){
    
    $objRespostas->texto = $itemresposta->texto;
    $objRespostas->idforum = $itemresposta->idforum;
    $objRespostas->idusuario = $itemresposta->idusuario;
    $objRespostas->melhoresposta ='t';
    $objRespostas->citacao = $itemresposta->citacao;
    $objRespostas->data = $itemresposta->data;
    $objRespostas->atualizar();
    
    echo "<div class='alert alert-success'>Melhor resposta destacada com sucesso! Você encontra as melhores respostas a partir da Página 1 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
    
    } else{
    $objRespostas->texto = $itemresposta->texto;
    $objRespostas->idforum =$itemresposta->idforum;
    $objRespostas->idusuario = $itemresposta->idusuario;
    $objRespostas->melhoresposta = 't';
    $objRespostas->data = $itemresposta->data;
    $objRespostas->atualizar2();
    
    
    echo "<div class='alert alert-success'>Melhor resposta destacada com sucesso! Você encontra as melhores respostas a partir da Página 1 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
    
    }
    
}