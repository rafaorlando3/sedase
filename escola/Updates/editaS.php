<?php

date_default_timezone_set("Brazil/East"); 

include_once '../../class/Carrega.class.php';

$objServico = new Pservicos();

if (isset($_POST["titulo"], $_POST["conteudo"], $_POST["categoria"], $_POST["idusuario"],$_POST["idservico"])) {
    
    $objServico->id=$_POST["idservico"];
    $item = $objServico->retornarunico();
            
    
    if (!empty($_FILES['imagem']['name']) &&  !empty($_FILES['anexo']['name'])) {

      $ext = strtolower(substr($_FILES['imagem']['name'],-4)); //Pegando extensão do arquivo
      $new_name = date("Y.m.d-H.i.s") . $ext; //Definindo um novo nome para o arquivo
      $dir = '../../uploads/imagens/'; //Diretório para uploads
      move_uploaded_file($_FILES['imagem']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
      
      $excluiImagem = "../../uploads/imagens/" . $item->imagem . "";
            if (file_exists($excluiImagem)) {
                unlink($excluiImagem);
            }
      
        
      $ext2 = strtolower(substr($_FILES['anexo']['name'],-4)); //Pegando extensão do arquivo
      $new_name2 = date("Y.m.d-H.i.s") . $ext2; //Definindo um novo nome para o arquivo
      $dir2 = '../../uploads/anexos/'; //Diretório para uploads
      move_uploaded_file($_FILES['anexo']['tmp_name'], $dir2.$new_name2); //Fazer upload do arquivo
      
         $excluiAnexo = "../../uploads/anexos/" . $item->anexo . "";
            if (file_exists($excluiAnexo)) {
                unlink($excluiAnexo);
            }
            
            
            $objServico->titulo = pg_escape_string($_POST["titulo"]);
            $objServico->texto = pg_escape_string($_POST["conteudo"]);
            $objServico->categoria = pg_escape_string($_POST["categoria"]);
            $objServico->anexo = $new_name2;
            $objServico->imagem = $new_name;
            $objServico->video = pg_escape_string($_POST["video"]);
            $objServico->idusuario = pg_escape_string($_POST["idusuario"]);
            $objServico->atualizar();      
      
      
     echo "<div class='alert alert-success'> Serviço atualizado com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button></div>";
        
        
    } elseif (!empty($_FILES['anexo']['name'])) {
             
      $ext2 = strtolower(substr($_FILES['anexo']['name'],-4)); //Pegando extensão do arquivo
      $new_name2 = date("Y.m.d-H.i.s") . $ext2; //Definindo um novo nome para o arquivo
      $dir2 = '../../uploads/anexos/'; //Diretório para uploads
      move_uploaded_file($_FILES['anexo']['tmp_name'], $dir2.$new_name2); //Fazer upload do arquivo 
   
         $excluiAnexo = "../../uploads/anexos/" . $item->anexo . "";
            if (file_exists($excluiAnexo)) {
                unlink($excluiAnexo);
            }
            
            $objServico->titulo = pg_escape_string($_POST["titulo"]);
            $objServico->texto = pg_escape_string($_POST["conteudo"]);
            $objServico->categoria = pg_escape_string($_POST["categoria"]);
            $objServico->anexo = $new_name2;
            $objServico->imagem = $item->imagem;
            $objServico->video = pg_escape_string($_POST["video"]);
            $objServico->idusuario = pg_escape_string($_POST["idusuario"]);
            $objServico->atualizar();         
      
      
    echo "<div class='alert alert-success'> Serviço atualizado com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button></div>";
        
      
    } elseif (!empty($_FILES['imagem']['name'])) {

      $ext = strtolower(substr($_FILES['imagem']['name'],-4)); //Pegando extensão do arquivo
      $new_name = date("Y.m.d-H.i.s") . $ext; //Definindo um novo nome para o arquivo
      $dir = '../../uploads/imagens/'; //Diretório para uploads
      move_uploaded_file($_FILES['imagem']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
      
            $excluiImagem = "../../uploads/imagens/" . $item->imagem . "";
            if (file_exists($excluiImagem)) {
                unlink($excluiImagem);
            }
      
      
            $objServico->titulo = pg_escape_string($_POST["titulo"]);
            $objServico->texto = pg_escape_string($_POST["conteudo"]);
            $objServico->categoria = pg_escape_string($_POST["categoria"]);
            $objServico->anexo =$item->anexo;
            $objServico->imagem = $new_name;
            $objServico->video = pg_escape_string($_POST["video"]);
            $objServico->idusuario = pg_escape_string($_POST["idusuario"]);
            $objServico->atualizar();   
      
      
            echo "<div class='alert alert-success'> Serviço atualizado com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button></div>";
        
    } else {
        
       
            $objServico->titulo = pg_escape_string($_POST["titulo"]);
            $objServico->texto = pg_escape_string($_POST["conteudo"]);
            $objServico->categoria = pg_escape_string($_POST["categoria"]);
            $objServico->anexo =$item->anexo;
            $objServico->imagem = $item->imagem;
            $objServico->video = pg_escape_string($_POST["video"]);
            $objServico->idusuario = pg_escape_string($_POST["idusuario"]);
            $objServico->atualizar();   


        echo "<div class='alert alert-success'> Serviço atualizado com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button></div>";
    }
}
