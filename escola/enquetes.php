<?php
$menu = 3;
include_once 'cabecalho.php';
?>
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Enquetes</h4>

            </div>

        </div>
        <div class="row">
            <div class="col-md-12">

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                    Cadastrar nova Enquete
                </button>

                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Cadastro de Enquete</h4>
                            </div>
                            <div class="modal-body">

                                <form id="cadastroE" >
                                    <div class="form-group">
                                        <label for="titulo">Título:</label>
                                        <input type="text" name="titulo" class="form-control" id="titulo" placeholder="Digite aqui o Título para a Enquete" required="" />
                                    </div>
                                    <div class="form-group">
                                        <label for="descricao">Descrição:</label> <br/>
                                        <textarea name="descricao" id="descricao" required="" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="data">Data:</label>
                                        <input type="date" name="data" class="form-control" id="data" placeholder="" required="" />
                                        <input type="hidden" name="idusuario" value="<?= $id ?>"/>
                                    </div>

                                    <div class="form-group" id="carregaturma">

                                    </div>



                                    <div class="text-center pull-center">   <button type="submit" class="btn btn-primary" id="carregar"  >Cadastrar</button> </div>
                                </form> <br/>
                                <div id="resposta"></div> 

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

                            </div>

                            <script type="text/javascript">
                                var jq = $.noConflict();

                                function atualizaenquetes() {
                                    jq.get('Selects/carregaenquetes.php', function (resultado) {
                                        jq('#listaenquetes').html(resultado);

                                    });
                                }

                                jq(document).ready(function () {
                                    jq('#cadastroE').submit(function () {
                                        var dados = jq(this).serialize();
                                        jq('#carregar').attr('disabled', true);
                                        jq("#carregar").html('Carregando...');
                                        jq.ajax({
                                            type: "POST",
                                            url: "Inserts/cadastroE.php",
                                            data: dados,
                                            success: function (data)
                                            {
                                                atualizaenquetes();
                                                jq('#resposta').html(data);
                                                jq('#carregar').attr('disabled', false);
                                                jq("#carregar").html('Cadastrar');
                                            }
                                        });

                                        return false;

                                    });
                                    atualiza();
                                    function atualiza() {
                                        jq.get('Selects/carregaturma.php', function (resultado) {
                                            jq('#carregaturma').html(resultado);

                                        });
                                    }




                                });


                                jq(document).ready(function () {
                                    atualizaenquetes();
                                    jq("#data").datepicker({
                                        dateFormat: 'dd/mm/yy',
                                        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
                                        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                                        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                                        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                                        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                                        nextText: 'Próximo',
                                        prevText: 'Anterior'
                                    });
                                    
                                    
                                    jq("#busca").keypress(function () {
                                    var pesquisa = jq('#busca').val();

                                    jq.get('Selects/carregaenquetes.php?pesquisa=' + pesquisa, function (resultado) {
                                        jq('#listaenquetes').html(resultado);

                                    });

                                });
                                    
                                });

                               
                            </script>

                        </div>
                    </div>
                </div>
                <!--             Fim modal...-->
                <hr/>

                <div class="text-right pull-right"><label>Buscar aqui: </label> <input  type="text" id="busca" value=""  /> </div>

                <h2>Enquetes<small> Listando</small></h2>

                <div id="listaenquetes">

                </div>


            </div>

        </div>
    </div>
</div>

<?php
include_once 'rodape.php';
