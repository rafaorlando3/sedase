<?php
$menu = 6;
include_once 'cabecalho.php';


if (isset($_SESSION["idmaterial"])) {
    $idedit = $_SESSION['idmaterial'];
    $objMateriais = new Materiais();
    $objMateriais->id = $idedit;
    $item = $objMateriais->retornarunico();
    $_SESSION["idturma"] = $item->idturma;
    $_SESSION["tipooutro"] = $item->tipo;
}

if ($_POST['idedit']) {

    $idedit = $_POST['idedit'];
    $objMateriais = new Materiais();
    $objMateriais->id = $idedit;
    $item = $objMateriais->retornarunico();
    $_SESSION["idmaterial"] = $item->id;
    $_SESSION["idturma"] = $item->idturma;
    $_SESSION["tipooutro"] = $item->tipo;
} else {
    header("location:usuarios.php");
}
?>
<div class="content-wrapper">
    <div class="container">
        
        <script language="Javascript">
var jq = $.noConflict();

jq(document).ready(function () {
            jq('#excluir').click(function () {
            var resposta = confirm("Você realmente deseja excluir esse Material?");
            if (resposta == true) {
                jq('#excluir').attr('disabled', true);
                var dados = jq('#excluirM').serialize();


                jq.ajax({
                    type: "POST",
                    url: "Delets/material-del.php",
                    data: dados,
                    success: function (data)
                    {
                        jq("#excluir").html('Excluindo...');
                        jq("#excluir").html('Excluído');
                        jq( "#excluiresp").empty();
                        jq('#excluiresp').html(data);

                        
                    }
                });
            } else {
                jq('#excluir').attr('disabled', false);
                jq("#excluir").html('Excluir');
            }

            return false;


        });
        
});



        </script>        
        
        <div id="excluiresp">
        
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Conteúdo: <?= $item->titulo ?></h4>  
            </div>

        </div>

        <button id="excluir" class="btn btn-large btn-danger" >Excluir</button> 

        <form id="excluirM"><input type="hidden" name="idedit" value="<?= $idedit; ?>"></form>

       
        
        <hr/>
        <div class="row">
            <div class="col-md-12">     
                <form id="editaM" >
                    <div class="form-group">
                        <label for="titulo">Título:</label>
                        <input type="text" name="titulo" class="form-control" id="titulo" placeholder="Digite o Título..." value="<?= $item->titulo; ?>" required="" />
                    </div>
                    <div class="form-group">
                        <label for="conteudo">Conteúdo:</label> <br/>
                        <textarea name="conteudo" id="material" required="" class="form-control"><?= $item->descricao; ?></textarea>
                    </div>
                    <div class="form-group" id="carregaturma">

                    </div>

                    <ul class="nav nav-tabs" role="tablist" id="myTab">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Vazio</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Foto</a></li>
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Youtube</a></li>
                        <li role="presentation"><a href="#outro" aria-controls="outro" role="tab" data-toggle="tab">Anexo</a></li>

                    </ul>

                    <div class="tab-content">
                        <link href="assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
                        <script src="assets/js/fileinput.js" type="text/javascript"></script>
                        <script src="assets/js/fileinput.min.js" type="text/javascript"></script>
                        <div role="tabpanel" class="tab-pane active" id="home">Não atualizar nenhum arquivo. </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="form-group">
                                <label>Selecione a Foto:</label>
                                <input name="imagem" id="file-3" type="file" />
                                <p></p>
                                <?php
                                if ($item->imagem != "" || $item->imagem != NULL) {
                                    ?>

                                    Imagem Atual: <img src="../uploads/imagens/<?= $item->imagem ?>" width="20%"alt="..." class="img-thumbnail">

                                    <?php
                                }
                                ?>


                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane" id="messages"><input type="text" class="form-control" name="video" value="<?= $item->video ?>" placeholder="Insira aqui o ID do vídeo do Youtube"/></div>

                        <div role="tabpanel" class="tab-pane" id="outro">
                            <div class="form-group">
                                <label>Selecione o anexo:</label>
                                <input name="anexo" id="file-4" type="file" />
                                <p></p>
<?php
if ($item->anexo != 0) {
    ?>

                                    Arquivo Atual: <a href="../uploads/anexos/<?= $item->anexo ?>" target="_blank">Visualizar</a>

                                    <?php
                                }
                                ?>
                            </div>
                        </div>

                        <script>

            $("#file-3").fileinput({
                allowedFileExtensions: ['jpg', 'png', 'gif'],
                showUpload: false,
                showCaption: false,
                browseClass: "btn btn-primary btn-lg",
                fileType: "any",
                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
            });

            $("#file-4").fileinput({
                allowedFileExtensions: ['pdf', 'doc', 'docx', 'odt', 'txt'],
                showUpload: false,
                showCaption: false,
                browseClass: "btn btn-primary btn-lg",
                fileType: "any",
                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
            });
                        </script>
                    </div>

                    <script>
                        $(function () {
                            $('#myTab a:last').tab('show')
                        })
                    </script>
                    <br/>


                    <input type="hidden" name="idusuario" value="<?= $item->idusuario; ?>"/>
                    <input type="hidden" name="id" value="<?= $item->id; ?>"/>


                    <button type="submit" class="btn btn-primary" id="carregar"  >Atualizar</button>
                </form> <br/>

                <div id="resposta"></div> 



                <script type="text/javascript">
                    jq(document).ready(function () {
                        jq('#editaM').submit(function () {
                            jq('#carregar').attr('disabled', true);
                            jq("#carregar").html('Carregando...');
                            for (instance in CKEDITOR.instances) {
                                CKEDITOR.instances[instance].updateElement();
                            }
                            jq.ajax({
                                type: "POST",
                                url: "Updates/editaM.php",
                                data: new FormData(this),
                                processData: false,
                                contentType: false,
                                success: function (data)
                                {
                                    jq('#resposta').html(data);
                                    jq('#carregar').attr('disabled', false);
                                    jq("#carregar").html('Atualizar');
                                }
                            });

                            return false;

                        });


                        atualiza();
                        function atualiza() {
                            jq.get('Selects/carreturmaupdate.php', function (resultado) {
                                jq('#carregaturma').html(resultado);

                            });
                        }
                        CKEDITOR.replace('material');

                    });

                </script>


            </div>

        </div>
        </div>
    <a href="materiais.php"><button type="button" class="btn btn-default">Voltar</button></a>
    </div>
</div>

<?php
include_once 'rodape.php';
