<?php
$menu = 4;
include_once 'cabecalho.php';
?>
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Notícias</h4>

            </div>

        </div>
        <div class="row">
            <div class="col-md-12">

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                    Cadastrar nova Notícia
                </button>

                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Cadastro de Notícia</h4>
                            </div>
                            <div class="modal-body">

                                <form id="cadastroN"  >
                                    <div class="form-group">
                                        <label for="titulo">Título:</label>
                                        <input type="text" name="titulo" class="form-control" id="titulo" placeholder="Digite seu Nome..." required="" />
                                    </div>
                                    <div class="form-group">
                                        <label for="conteudo">Conteúdo:</label> <br/>
                                        <textarea name="conteudo" id="noticia" required="" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="data">Data:</label>
                                        <input type="date" name="data" class="form-control" id="data" placeholder="" required="" />
                                    </div>

                                    <ul class="nav nav-tabs" role="tablist" id="myTab">
                                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Vazio</a></li>
                                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Foto</a></li>
                                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Youtube</a></li>

                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="home">Não cadastrar nada. </div>
                                        <div role="tabpanel" class="tab-pane" id="profile">



                                            <link href="assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
                                            <script src="assets/js/fileinput.js" type="text/javascript"></script>
                                            <script src="assets/js/fileinput.min.js" type="text/javascript"></script>
                                            <div class="form-group">
                                                <label>Selecione a Foto:</label>
                                                <input name="imagem" id="imagem" type="file" />
                                            </div>
                                            <script>

                                                $("#imagem").fileinput({
                                                    allowedFileExtensions: ['jpg', 'png', 'gif'],
                                                    showUpload: false,
                                                    showCaption: false,
                                                    browseClass: "btn btn-primary btn-lg",
                                                    fileType: "any",
                                                    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
                                                });
                                            </script>


                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="messages"><input type="text" class="form-control" name="video" placeholder="Insira aqui o ID do vídeo do Youtube"/></div>
                                    </div>

                                    <script>
                                        $(function () {
                                            $('#myTab a:last').tab('show')
                                        })
                                    </script>
                                    <br/>

                                    <div class="form-group">
                                        <label for="publicado">Publicar?</label>
                                        Sim <input type="radio"  name="publicado" value="true" checked="checked" /> - Não <input type="radio" name="publicado" value="false" />
                                    </div>  

                                    <div class="alert alert-info alert-dismissible fade in" role="alert"><strong>Tags</strong> são a mesma coisa que palavras-chave, escolha palavras que tenham algo a ver com sua postagem e separe-as por vírgula. Ex: AIDS, DSTs, Saúde.</div>

                                    <div class="form-group">
                                        <label for="tags">Tags:</label> 
                                        <input type="text" name="tags" class="form-control" id="titulo" placeholder="Digite seu Nome..." />
                                        <input type="hidden" name="idusuario" value="<?= $id ?>"/>
                                    </div>



                                    <button type="submit" class="btn btn-primary" id="carregar"  >Cadastrar</button>
                                </form> <br/>
                                <div id="resposta"></div> 

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

                            </div>

                            <script type="text/javascript">
                                var jq = $.noConflict();
                                function atualizanoticias() {
                                    jq.get('Selects/carreganoticias.php', function (resultado) {
                                        jq('#listanoticias').html(resultado);

                                    });
                                }
                                jq(document).ready(function () {


                                    jq('#cadastroN').submit(function () {
                                        //var dados = jq(this).serialize(); 
                                        for (instance in CKEDITOR.instances) {
                                            CKEDITOR.instances[instance].updateElement();
                                        }
                                        jq('#carregar').attr('disabled', true);
                                        jq("#carregar").html('Carregando...');
                                        jq.ajax({
                                            type: "POST",
                                            url: "Inserts/cadastroN.php",
                                            data: new FormData(this),
                                            processData: false,
                                            contentType: false,
                                            success: function (data)
                                            {
                                                atualizanoticias();
                                                jq('#resposta').html(data);
                                                jq('#carregar').attr('disabled', false);
                                                jq("#carregar").html('Cadastrar');

                                            }
                                        });

                                        return false;

                                    });

                                    CKEDITOR.replace('noticia');
                                });

                                jq(document).ready(function () {
                                    atualizanoticias();
                                    jq("#data").datepicker({
                                        dateFormat: 'dd/mm/yy',
                                        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
                                        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                                        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                                        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                                        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                                        nextText: 'Próximo',
                                        prevText: 'Anterior'
                                    });

                                    jq("#busca").keypress(function () {
                                        var pesquisa = jq('#busca').val();

                                        jq.get('Selects/carreganoticias.php?pesquisa=' + pesquisa, function (resultado) {
                                            jq('#listanoticias').html(resultado);

                                        });

                                    });

                                });


                            </script>

                        </div>
                    </div>
                </div>
                <!--             Fim modal...-->
                <hr/>

                <div class="text-right pull-right"><label>Buscar aqui: </label> <input  type="text" id="busca" value=""  /> </div>

                <h2>Notícias<small> Listando</small></h2>
                <div id="listanoticias">

                </div>
            </div>

        </div>
    </div>
</div>

<?php
include_once 'rodape.php';
