<?php
$menu = 2;
include_once 'cabecalho.php';
$delete = "";
?>


<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Usuários</h4>
<!--                <h3> <a style="color: black;" class="navbar-right" data-toggle="modal" data-target="#lixeira" href="#"><span class="glyphicon glyphicon-trash" title="Usuários Excluidos"></span></a> </h3>-->
            </div>


        </div>

        <div class="row">

            <div class="col-md-12">

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                    Cadastrar novo Usuário
                </button>






                <div class="modal fade" id="lixeira" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel"> Usuários na Lixeira</h4>
                            </div>
                            <div class="modal-body">


                                <div id="listausuarioslixeira">

                                </div> 
                                <div id="respostaexcluidos"></div> 


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

                            </div>
                        </div>
                    </div>
                </div>




                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Cadastro de Usuário - 1ª Etapa </h4>
                            </div>
                            <div class="modal-body">

                                <form id="cadastroU" >
                                    <div class="form-group">
                                        <label for="nome">Nome:</label>
                                        <input type="text" name="nome" class="form-control" id="nome" placeholder="Digite seu Nome..." required="" />
                                    </div>
                                    <div class="form-group">
                                        <label for="data">Nascimento:</label>
                                        <input type="text" name="data" class="form-control" id="data" placeholder="" required="" />
                                    </div>

                                    <div class="form-group">
                                        <label for="estado">Estado:</label>
                                        <select name="estado" required=""> 
                                            <option value="AC">Acre</option> 
                                            <option value="AL">Alagoas</option> 
                                            <option value="AM">Amazonas</option> 
                                            <option value="AP">Amapá</option> 
                                            <option value="BA">Bahia</option> 
                                            <option value="CA">Ceará</option> 
                                            <option value="DF">Distrito Federal</option> 
                                            <option value="ES">Espírito Santo</option> 
                                            <option value="GO">Goiás</option> 
                                            <option value="MA">Maranhão</option> 
                                            <option value="MT">Mato Grosso</option> 
                                            <option value="MS">Mato Grosso do Sul</option> 
                                            <option value="MG">Minas Gerais</option> 
                                            <option value="PA">Pará</option> 
                                            <option value="PB">Paraíba</option> 
                                            <option value="PR">Paraná</option> 
                                            <option value="PE">Pernambuco</option> 
                                            <option value="PI">Piauí</option> 
                                            <option value="RJ">Rio de Janeiro</option> 
                                            <option value="RN">Rio Grande do Norte</option> 
                                            <option value="RO">Rondônia</option> 
                                            <option value="RS" selected="selected">Rio Grande do Sul</option> 
                                            <option value="RR">Roraima</option> 
                                            <option value="SC">Santa Catarina</option> 
                                            <option value="SE">Sergipe</option> 
                                            <option value="SP">São Paulo</option> 
                                            <option value="TO">Tocantins</option> 
                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <label for="cidade">Cidade:</label>
                                        <input type="text" name="cidade" class="form-control" id="cidade" placeholder="Digite seu Cidade..." required="" />
                                    </div>

                                    <div class="form-group">
                                        <label for="genero">Gênero:</label>
                                        <select name="genero" required=""> 
                                            <option value="Feminino">Feminino</option> 
                                            <option value="Masculino">Masculino</option>
                                            <option value="Trans Feminino">Trans Feminino</option>
                                            <option value="Trans Masculino">Trans Masculino</option>
                                            <option value="Não Binário">Não Binário</option>
                                            <option value="Outro">Outro</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" class="form-control" id="email" placeholder="Digite seu Email..." required="" />
                                    </div>

                                    <div class="form-group">
                                        <label for="tipo">Tipo de Usuário:</label>
                                        <select name="tipo">
                                            <?php if ($tipo == 1) { ?>
                                                <option value="1">Administrador</option>
                                            <?php } ?>
                                            <option value="0">Aluno</option>
                                            <option value="2">Professor</option>
                                        </select>

                                    </div>

                                    <div class="text-center pull-center">  <button type="submit" class="btn btn-primary" id="carregaruser"  >Cadastrar</button></div>
                                </form> <br/>
                                <div id="respostauser"></div> 

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

                            </div>
                        </div>
                    </div>
                </div>
                <!--             Fim modal...-->
                <hr/>

                <?php echo $delete ?>

                <div class="text-right pull-right"><label>Buscar aqui: </label> <input  type="text" id="busca" value=""  /> </div>


                <h2>Usuários<small> Listando</small></h2>



                <div id="listausuarios">


                </div>  
            </div>

        </div>
    </div>
</div>


<script type="text/javascript">
    var jq = $.noConflict();
    function atualizausuarios() {
        jq.get('Selects/carregausuarios.php', function (resultado) {
            jq('#listausuarios').html(resultado);

        });
    }

    function atualizausuarioslixeira() {
        jq.get('Selects/carregausuarioslixo.php', function (resultado2) {
            jq('#listausuarioslixeira').html(resultado2);

        });
    }

    atualizausuarioslixeira();

    jq(document).ready(function () {
        jq('#cadastroU').submit(function () {
            var dados = jq(this).serialize();
            jq('#carregaruser').attr('disabled', true);
            jq("#carregaruser").html('Carregando...');
            jq.ajax({
                type: "POST",
                url: "Inserts/cadastroU.php",
                data: dados,
                success: function (data)
                {
                    jq('#respostauser').html(data);
                    jq('#carregaruser').attr('disabled', false);
                    jq("#carregaruser").html('Cadastrar');
                    atualizausuarioslixeira();
                    atualizausuarios();
                }
            });

            return false;

        });

    });



    jq(document).ready(function () {
        atualizausuarios();
        atualizausuarioslixeira();
        jq("#data").datepicker({
            dateFormat: 'dd/mm/yy',
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
            dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            nextText: 'Próximo',
            prevText: 'Anterior'
        });
    });

    jq("#busca").keypress(function () {
        var pesquisa = jq('#busca').val();

        jq.get('Selects/carregausuarios.php?pesquisa=' + pesquisa, function (resultado) {
            jq('#listausuarios').html(resultado);

        });

    });


</script>


<?php
include_once 'rodape.php';


