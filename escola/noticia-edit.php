<?php
$menu = 4;
include_once 'cabecalho.php';


if ($_POST['idedit']) {

    $idedit = $_POST['idedit'];
    $objNoticias = new Noticias();
    $objNoticias->id = $idedit;
    $item = $objNoticias->retornarunico();
} else {
    header("location:usuarios.php");
}
?>
<div class="content-wrapper">
    <div class="container">
        
        <script language="Javascript">
var jq = $.noConflict();

jq(document).ready(function () {
            jq('#excluir').click(function () {
            var resposta = confirm("Você realmente deseja excluir essa Notícia?");
            if (resposta == true) {
                jq('#excluir').attr('disabled', true);
                var dados = jq('#excluirN').serialize();


                jq.ajax({
                    type: "POST",
                    url: "Delets/noticia-del.php",
                    data: dados,
                    success: function (data)
                    {
                        jq("#excluir").html('Excluindo...');
                        jq("#excluir").html('Excluído');
                        jq( "#excluiresp").empty();
                        jq('#excluiresp').html(data);

                        
                    }
                });
            } else {
                jq('#excluir').attr('disabled', false);
                jq("#excluir").html('Excluir');
            }

            return false;


        });
        
});



        </script>        
        
        <div id="excluiresp">
        
        
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Notícia: <?= $item->titulo ?></h4>  
            </div>

        </div>
            
        <button id="excluir" class="btn btn-large btn-danger" >Excluir</button> 

        <form id="excluirN"><input type="hidden" name="idedit" value="<?= $idedit; ?>"></form>

       
<hr/>
       
        <div class="row">
            <div class="col-md-12">
                <form id="editaN" >
                    <div class="form-group">
                        <label for="titulo">Título:</label>
                        <input type="text" name="titulo" class="form-control" id="titulo" value="<?= $item->titulo ?>" placeholder="Digite seu Nome..." required="" />
                    </div>
                    <div class="form-group">
                        <label for="conteudo">Conteúdo:</label> <br/>
                        <textarea name="conteudo" id="noticia" required="" class="form-control"><?= $item->conteudo ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="data">Data:</label>
                        <input type="date" name="data" class="form-control" id="data" value="<?= $item->data ?>" placeholder="" required="" />
                    </div>

                    <ul class="nav nav-tabs" role="tablist" id="myTab">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Vazio</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Foto</a></li>
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Youtube</a></li>

                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">Não cadastrar nada. </div>
                        <div role="tabpanel" class="tab-pane" id="profile">



                            <link href="assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
                            <script src="assets/js/fileinput.js" type="text/javascript"></script>
                            <script src="assets/js/fileinput.min.js" type="text/javascript"></script>
                            <div class="form-group">
                                <label>Selecione a Foto:</label>
                                <input name="imagem" id="file-3" type="file" />

                                <br/>
                                <?php
                                if ($item->imagem != "" || $item->imagem != NULL) {
                                    ?>

                                    Imagem Atual: <img src="../uploads/imagens/<?= $item->imagem ?>" width="20%"alt="..." class="img-thumbnail">

                                    <?php
                                }
                                ?>

                            </div>
                            <script>

            $("#file-3").fileinput({
                allowedFileExtensions: ['jpg', 'png', 'gif'],
                showUpload: false,
                showCaption: false,
                browseClass: "btn btn-primary btn-lg",
                fileType: "any",
                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
            });
                            </script>


                        </div>
                        <div role="tabpanel" class="tab-pane" id="messages"><input type="text" class="form-control" name="video" value="<?= $item->video ?>" placeholder="Insira aqui o ID do vídeo do Youtube"/></div>
                    </div>

                    <script>
                        $(function () {
                            $('#myTab a:last').tab('show')
                        })
                    </script>
                    <br/>

                    <div class="form-group">
                        <label for="publicado">Publicar?</label>
                        Sim <input type="radio"  name="publicado" value="true" <?php if ($item->publicado == TRUE) {
                                    echo "checked";
                                } ?> /> - Não <input type="radio" name="publicado" value="false" <?php if ($item->publicado == FALSE) {
                                    echo "checked";
                                } ?> />
                    </div>  

                    <div class="form-group">
                        <label for="tags">Tags:</label>
                        <input type="text" name="tags" class="form-control" id="tags" value="<?= $item->tags ?>" placeholder="Digite seu Nome..." />
                        <input type="hidden" name="idusuario" value="<?= $item->idusuario; ?>"/>
                        <input type="hidden" name="idnoticia" value="<?= $item->id; ?>"/>
                    </div>

                    <button type="submit" class="btn btn-primary" id="carregar"  >Atualizar</button>
                </form> <br/>
                <div id="resposta"></div> 



                <script type="text/javascript">
                    
                    jq(document).ready(function () {
                        jq('#editaN').submit(function () {
                            jq('#carregar').attr('disabled', true);
                            jq("#carregar").html('Carregando...');

                            for (instance in CKEDITOR.instances) {
                                CKEDITOR.instances[instance].updateElement();
                            }

                            jq.ajax({
                                type: "POST",
                                url: "Updates/editaN.php",
                                data: new FormData(this),
                                processData: false,
                                contentType: false,
                                success: function (data)
                                {
                                    jq('#resposta').html(data);
                                    jq('#carregar').attr('disabled', false);
                                    jq("#carregar").html('Atualizar');
                                }
                            });

                            return false;

                        });

                        CKEDITOR.replace('noticia');
                    });

                    jq(document).ready(function () {
                        jq("#data").datepicker({
                            dateFormat: 'dd/mm/yy',
                            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
                            dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                            nextText: 'Próximo',
                            prevText: 'Anterior'
                        });
                    });
                </script>


            </div>

        </div>
        </div>
        
        <a href="noticias.php"><button type="button" class="btn btn-default">Voltar</button></a>
    </div>
</div>

<?php
include_once 'rodape.php';
