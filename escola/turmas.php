<?php
$menu = 8;
include_once 'cabecalho.php';
$delete = "";
?>

<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Turmas da escola <?= $escola; ?></h4>
            </div>



        </div>

        <div class="row">

            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary btn-lg text-left " data-toggle="modal" data-target="#myModal">
                Cadastrar nova Turma
            </button>

            <button type="button" class="btn btn-warning btn-lg text-right pull-right" data-toggle="modal" data-target="#modalvincula">
                Vincular alunos a uma Turma
            </button>


        </div>



        <div class="row">

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Cadastro de Turmas </h4>
                        </div>
                        <div class="modal-body">

                            <form id="cadastroT" >
                                <div class="form-group">

                                    <label for="escola">Escola: <?= $escola; ?></label>
                                </div>

                                <div class="form-group">
                                    <label for="nometruma">Turma:</label>
                                    <input type="text" name="nometurma" class="form-control" id="nome" placeholder="Digite o nome da Turma..." required="" />
                                </div>
                                <div class="form-group">
                                    <label for="grau">Grau:</label>
                                    <select name="grau" id="grau" required="">
                                        <option value="1" >Ensino Fundamental</option>
                                        <option value="2" >Ensino Médio</option>
                                    </select>
                                </div>

                                <div id="div1" class="form-group">
                                    <label for="ano">Ano:</label> 
                                    <select name="ano" required="">
                                        <option value="1">1º</option>
                                        <option value="2">2º</option>
                                        <option value="3">3º</option>
                                        <option value="4">4º</option>
                                        <option value="5">5º</option>
                                        <option value="6">6º</option>
                                        <option value="7">7º</option>
                                        <option value="8">8º</option>
                                        <option value="9">9º</option>
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-primary" id="carregar"  >Cadastrar</button>

                            </form>
                            <div id="resposta2"></div> 

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

                        </div>
                    </div>
                </div>
            </div>
            <!--             Fim modal...-->

            <!-- Modal -->
            <div class="modal fade" id="modalvincula" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Vinculação de Alunos a Turmas</h4>
                        </div>
                        <div class="modal-body">

                            <label for="turma">Selecione a Turma:</label>
                            <div class="pull-center text-center" id="carregaturma2"></div>         
                                
                            <hr/>

                            <div id="respostavinculacao"></div>

                            <div class="row">
                                <div class=" text-center">

                                    <button id="mostraesconde" class="btn btn-success btn-lg " type="button" data-toggle="collapse" data-target="#visualizavincula" aria-expanded="false" aria-controls="mostraocultavincula" >
                                        <span class="glyphicon glyphicon-sort" aria-hidden="true" title="Mostrar/Ocultar"></span> Vincular
                                    </button>

                                </div>
                            </div>


                            <div class="collapse" id="visualizavincula">
                                <div class="well">

                                    <div id="respostavincula">Selecione uma Turma</div>


                                </div>

                            </div>


                            <hr/>

                            <div class="row">
                                <div class=" text-center">

                                    <button id="mostraesconde" class="btn btn-danger btn-lg " type="button" data-toggle="collapse" data-target="#visualizadesvincula" aria-expanded="false" aria-controls="mostraocultadesvincula" >
                                        <span class="glyphicon glyphicon-sort" aria-hidden="true" title="Mostrar/Ocultar"></span> Desvincular
                                    </button>

                                </div>
                            </div>


                            <div class="collapse" id="visualizadesvincula">
                                <div class="well">


                                    <div id="respostavincula2"> Selecione uma Turma</div>

                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

                        </div>
                    </div>
                </div>
            </div>
            <!--             Fim modal...-->

            <hr/>

            <?php echo $delete ?>
            
           <div class="text-right pull-right"><label>Buscar aqui: </label> <input  type="text" id="busca" value=""  /> </div>


            <h2>Turmas<small> Listando</small></h2>
            <div id="listaturmas">


            </div>  


        </div>
    </div>
</div>


<script type="text/javascript">
    var jq = $.noConflict();
   
    function atualizaturmas() {
        jq.get('Selects/carregaturmas.php', function (resultado) {
            jq('#listaturmas').html(resultado);

        });
    }
    
    function atualizaturmas2() {
        jq.get('Selects/listaturma2.php', function (resultado) {
            jq('#carregaturma2').html(resultado);

        });
    }

    atualizaturmas();
    atualizaturmas2();

    jq(document).ready(function () {
        jq('#cadastroT').submit(function () {
            var dados = jq(this).serialize();
            jq('#carregar').attr('disabled', true);
            jq("#carregar").html('Carregando...');
            jq.ajax({
                type: "POST",
                url: "Inserts/cadastroT.php",
                data: dados,
                success: function (data)
                {
                    jq('#resposta2').html(data);
                    jq('#carregar').attr('disabled', false);
                    jq("#carregar").html('Cadastrar');
                    atualizaturmas();
                    atualizaturmas2();

                }
            });

            return false;

        });


        jq("#grau").change(function () {
            var grau = this.value;
            if (grau === '1') {
                var conteudo = '<label for="ano">Ano:</label> <select name="ano" required=""><option value="1">1º</option><option value="2">2º</option><option value="3">3º</option><option value="4">4º</option><option value="5">5º</option><option value="6">6º</option><option value="7">7º</option><option value="8">8º</option><option value="9">9º</option></select>';
                jq('#div1').html(conteudo);
            } else if (grau === '2') {
                var conteudo = '<label for="ano">Ano:</label> <select name="ano" required=""><option value="1">1º</option><option value="2">2º</option><option value="3">3º</option></select>';
                jq('#div1').html(conteudo);
            }

        });

    });
    
jq("#busca").keypress(function () {
        var pesquisa = jq('#busca').val();

        jq.get('Selects/carregaturmas.php?pesquisa=' + pesquisa, function (resultado) {
            jq('#listaturmas').html(resultado);

        });

    });
</script>


<?php
include_once 'rodape.php';


