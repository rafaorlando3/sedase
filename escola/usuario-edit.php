<?php
$menu=2;
include_once 'cabecalho.php';


if ($_POST['idedit']) {

    $idedit = $_POST['idedit'];
    $objUsuarios = new Usuarios();
    $objUsuarios->id = $idedit;
    $item = $objUsuarios->retornarunico();
} else {
    header("location:usuarios.php");
}
?>
<div class="content-wrapper">
    <div class="container">

<script language="Javascript">
var jq = $.noConflict();

jq(document).ready(function () {
            jq('#excluir').click(function () {
            var resposta = confirm("Você realmente deseja excluir esse Usuário?");
            if (resposta == true) {
                jq('#excluir').attr('disabled', true);
                var dados = jq('#excluirU').serialize();


                jq.ajax({
                    type: "POST",
                    url: "Delets/usuario-del.php",
                    data: dados,
                    success: function (data)
                    {
                        jq("#excluir").html('Excluindo...');
                        jq("#excluir").html('Excluído');
                        jq( "#excluiresp").empty();
                        jq('#excluiresp').html(data);


                    }
                });
            } else {
                jq('#excluir').attr('disabled', false);
                jq("#excluir").html('Excluir');
            }

            return false;


        });
        
});



        </script>        
        
        <div id="excluiresp">
        
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Usuário <?= $item->nome ?></h4>  
            </div>

        </div>

  
        <button id="excluir" class="btn btn-large btn-danger" >Excluir</button> 

        <form id="excluirU"><input type="hidden" name="idedit" value="<?= $idedit; ?>"></form>

       
        
        <div class="row">
            <div class="col-md-12">
                <form id="editaU" >
                    <div class="form-group">
                        <label for="nome">Nome:</label>
                        <input type="text" name="nome" class="form-control" id="nome" value="<?= $item->nome ?>" placeholder="Digite seu Nome..." required="" />
                    </div>
                    
                    <div class="form-group">
                        <label for="data">Nascimento:</label>
                        <input type="date" name="data" class="form-control" id="data" value="<?= date("d-m-Y", strtotime($item->data)) ?>" placeholder="" required="" />
                    </div>

                    <div class="form-group">
                        <label for="estado">Estado:</label>
                        <select name="estado" required=""> 
                            <option value="AC" <?php
                            if ($item->estado == "AC") {
                                echo "selected='selected'";
                            }
                            ?>>Acre</option> 
                            <option value="AL" <?php
                            if ($item->estado == "AL") {
                                echo "selected='selected'";
                            }
                            ?>>Alagoas</option> 
                            <option value="AM" <?php
                            if ($item->estado == "AM") {
                                echo "selected='selected'";
                            }
                            ?>>Amazonas</option> 
                            <option value="AP" <?php
                            if ($item->estado == "AP") {
                                echo "selected='selected'";
                            }
                            ?>>Amapá</option> 
                            <option value="BA" <?php
                            if ($item->estado == "BA") {
                                echo "selected='selected'";
                            }
                            ?>>Bahia</option> 
                            <option value="CA" <?php
                            if ($item->estado == "CA") {
                                echo "selected='selected'";
                            }
                            ?>>Ceará</option> 
                            <option value="DF" <?php
                            if ($item->estado == "DF") {
                                echo "selected='selected'";
                            }
                            ?>>Distrito Federal</option> 
                            <option value="ES" <?php
                            if ($item->estado == "ES") {
                                echo "selected='selected'";
                            }
                            ?>>Espírito Santo</option> 
                            <option value="GO" <?php
                            if ($item->estado == "GO") {
                                echo "selected='selected'";
                            }
                            ?>>Goiás</option> 
                            <option value="MA" <?php
                            if ($item->estado == "MA") {
                                echo "selected='selected'";
                            }
                            ?>>Maranhão</option> 
                            <option value="MT" <?php
                            if ($item->estado == "MT") {
                                echo "selected='selected'";
                            }
                            ?>>Mato Grosso</option> 
                            <option value="MS" <?php
                            if ($item->estado == "MS") {
                                echo "selected='selected'";
                            }
                            ?>>Mato Grosso do Sul</option> 
                            <option value="MG" <?php
                            if ($item->estado == "MG") {
                                echo "selected='selected'";
                            }
                            ?>>Minas Gerais</option> 
                            <option value="PA" <?php
                            if ($item->estado == "PA") {
                                echo "selected='selected'";
                            }
                            ?>>Pará</option> 
                            <option value="PB" <?php
                            if ($item->estado == "PB") {
                                echo "selected='selected'";
                            }
                            ?>>Paraíba</option> 
                            <option value="PR" <?php
                            if ($item->estado == "PR") {
                                echo "selected='selected'";
                            }
                            ?>>Paraná</option> 
                            <option value="PE" <?php
                            if ($item->estado == "PE") {
                                echo "selected='selected'";
                            }
                            ?>>Pernambuco</option> 
                            <option value="PI" <?php
                            if ($item->estado == "PI") {
                                echo "selected='selected'";
                            }
                            ?>>Piauí</option> 
                            <option value="RJ" <?php
                            if ($item->estado == "RJ") {
                                echo "selected='selected'";
                            }
                            ?>>Rio de Janeiro</option> 
                            <option value="RN" <?php
                            if ($item->estado == "RN") {
                                echo "selected='selected'";
                            }
                            ?>>Rio Grande do Norte</option> 
                            <option value="RO" <?php
                            if ($item->estado == "RO") {
                                echo "selected='selected'";
                            }
                            ?>>Rondônia</option> 
                            <option value="RS" <?php
                            if ($item->estado == "RS") {
                                echo "selected='selected'";
                            }
                            ?> >Rio Grande do Sul</option> 
                            <option value="RR" <?php
                            if ($item->estado == "RR") {
                                echo "selected='selected'";
                            }
                            ?>>Roraima</option> 
                            <option value="SC" <?php
                            if ($item->estado == "SC") {
                                echo "selected='selected'";
                            }
                            ?>>Santa Catarina</option> 
                            <option value="SE" <?php
                            if ($item->estado == "SE") {
                                echo "selected='selected'";
                            }
                            ?>>Sergipe</option> 
                            <option value="SP" <?php
                            if ($item->estado == "SP") {
                                echo "selected='selected'";
                            }
                            ?>>São Paulo</option> 
                            <option value="TO" <?php
                            if ($item->estado == "TO") {
                                echo "selected='selected'";
                            }
                            ?>>Tocantins</option> 
                        </select>

                    </div>

                    <div class="form-group">
                        <label for="genero">Gênero:</label>
                        <select name="genero" required=""> 
                            <option value="Feminino"  <?php
                            if ($item->genero == "Feminino") {
                                echo "selected='selected'";
                            }
                            ?>>Feminino</option> 
                            <option value="Masculino" <?php
                            if ($item->genero == "Masculino") {
                                echo "selected='selected'";
                            }
                            ?> >Masculino</option>
                            <option value="Trans Feminino" <?php
                            if ($item->genero == "Trans Feminino") {
                                echo "selected='selected'";
                            }
                            ?>>Trans Feminino</option>
                            <option value="Trans Masculino" <?php
                            if ($item->genero == "Trans Masculino") {
                                echo "selected='selected'";
                            }
                            ?>>Trans Masculino</option>
                            <option value="Não Binário"  <?php
                            if ($item->genero == "Não Binário") {
                                echo "selected='selected'";
                            }
                            ?>>Não Binário</option>
                            <option value="Outro" <?php
                            if ($item->genero == "Outro") {
                                echo "selected='selected'";
                            }
                            ?>>Outro</option>
                        </select>

                    </div>
                    <div class="form-group">
                        <label for="cidade">Cidade:</label>
                        <input type="text" name="cidade" class="form-control" id="cidade" value="<?= $item->cidade ?>" placeholder="Digite seu Cidade..." required="" />
                    </div>

                    <div class="form-group">
                        <label for="tipo">Tipo de Usuário:</label>
                        <select name="tipo">
                            <?php if ($tipo == 1) { ?>
                                <option value="1" <?php
                                if ($item->tipo == 1) {
                                    echo "selected='selected'";
                                }
                                ?>>Administrador</option>
                                    <?php } ?>
                            <option value="0" <?php
                            if ($item->tipo == 0) {
                                echo "selected='selected'";
                            }
                            ?>>Aluno</option>
                            <option value="2" <?php
                            if ($item->tipo == 2) {
                                echo "selected='selected'";
                            }
                            ?>>Professor</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" name="email" class="form-control" id="email" value="<?= $item->email ?>" placeholder="Digite seu Email..." required="" />
                    </div>
                    <div class="form-group">
                        <label for="senha">Senha:</label>
                        <input type="password" name="senha" class="form-control" id="senha" value="<?= base64_decode($item->senha) ?>"  required="" />
                    </div>
       
                    <input type="hidden" name="idusuario" value="<?= $idedit ?>"/>
                    <input type="hidden" name="ativo" value="<?=$item->ativo?>"/>
                    <button type="submit" class="btn btn-primary" id="carregar"  >Atualizar</button>
                </form>  <br/>
                <div id="resposta"></div> 



                <script type="text/javascript">
               
                    jq(document).ready(function () {
                        jq('#editaU').submit(function () {
                            var dados = jq(this).serialize();
                            jq('#carregar').attr('disabled', true);
                            jq("#carregar").html('Carregando...');
                            jq.ajax({
                                type: "POST",
                                url: "Updates/editaU.php",
                                data: dados,
                                success: function (data)
                                {
                                    jq('#resposta').html(data);
                                    jq('#carregar').attr('disabled', false);
                                    jq("#carregar").html('Atualizar');
                                }
                            });

                            return false;

                        });


                    });
                </script>


            </div>

        </div>
      </div>
        <a href="usuarios.php"><button type="button" class="btn btn-default">Voltar</button></a>
    </div>
</div>

<?php
include_once 'rodape.php';
