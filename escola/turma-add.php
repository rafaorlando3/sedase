<?php
session_start();
include_once '../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');

if (isset($_SESSION["nome"],$_SESSION["iduser"],$_SESSION["tipo"])) {   
$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
$nome = isset($_SESSION['nome']) ? $_SESSION['nome'] : '';
$tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';

 $objUsuarios = new Usuarios();
 $objUsuarios->id = $id;
 $itemuser = $objUsuarios->retornarunico();

    
} else {
echo "<script>window.location.href ='index.php';</script>";
}

?>
<script src="assets/js/jquery-3.1.0.js"></script>
<hr/>

<button class="btn btn-danger pull-right" id="fechar" title="fechar"><span  class="glyphicon glyphicon-remove-circle"  aria-hidden="true" ></span></button>
<form id="cadastroT" >
     <div class="form-group">
                <?php   
          $objEscola = new Escola();
                        $lista = $objEscola->listar();
                        if ($lista != null) {

                            foreach ($lista as $item) {
                                $escola = $item->nome;
                                $codescola = $item->codigo;
                            }
                            
                        }
           ?> 
      <label for="escola">Escola: <?=$escola;?></label>
     </div>
    
    <div class="form-group">
        <label for="nometruma">Turma:</label>
        <input type="text" name="nometurma" class="form-control" id="nome" placeholder="Digite o nome da Turma..." required="" />
    </div>
    <div class="form-group">
        <label for="grau">Grau:</label>
        <select name="grau" id="grau" required="">
            <option value="1" >Ensino Fundamental</option>
            <option value="2" >Ensino Médio</option>
        </select>
    </div>

    <div id="div1" class="form-group">
        
    <label for="ano">Ano:</label>
    <select name="ano" required="">
        <option value="1">1º</option>
        <option value="2">2º</option>
        <option value="3">3º</option>
        <option value="4">4º</option>
        <option value="5">5º</option>
        <option value="6">6º</option>
        <option value="7">7º</option>
        <option value="8">8º</option>
        <option value="9">9º</option>
    </select>
    </div>

    <div class="text-center pull-center">  <button type="submit" class="btn btn-success" id="carregart"  >Incluir</button></div>

</form>
<div id="resposta2"></div> 
<hr/>
<script type="text/javascript">
    var jq2 = $.noConflict();
    jq2(document).ready(function () {
        jq2('#carregar').attr('disabled', true);
        
        jq2('#cadastroT').submit(function () {
            var dados = jq(this).serialize();
            jq2('#carregart').attr('disabled', true);
            jq2("#carregart").html('Carregando...');
            jq2.ajax({
                type: "POST",
                url: "Inserts/cadastroT.php",
                data: dados,
                success: function (data)
                {
                    jq2('#resposta2').html(data);
                    jq2('#carregart').attr('disabled', false);
                    jq2("#carregart").html('Incluir');
                     atualiza();
            function atualiza() {
                jq2.get('Selects/carregaturma.php', function (resultado) {
                    jq2('#carregaturma').html(resultado);

                });
            }

                    
                }
            });

            return false;

        });
        
        
    jq( "#grau" ).change(function() {
    var grau = this.value;
    if(grau==='1'){
      var conteudo='<label for="ano">Ano:</label> <select name="ano" required=""><option value="1">1º</option><option value="2">2º</option><option value="3">3º</option><option value="4">4º</option><option value="5">5º</option><option value="6">6º</option><option value="7">7º</option><option value="8">8º</option><option value="9">9º</option></select>';  
      jq('#div1').html(conteudo);  
    } else if(grau==='2'){
      var conteudo='<label for="ano">Ano:</label> <select name="ano" required=""><option value="1">1º</option><option value="2">2º</option><option value="3">3º</option></select>';  
      jq('#div1').html(conteudo); 
    }

   });
        
        
           
   jq('#fechar').click(function () {
            jq('#cadastroturma').html("");
            jq2('#carregar').attr('disabled', false);
        });
 
    });
    

      
   
</script>
