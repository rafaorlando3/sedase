<?php

include_once '../../class/Carrega.class.php';


if (isset($_POST['aprovado'], $_POST['id'])) {

    $idedit = $_POST['id'];
    $objUsuarios = new Usuarios();
    $objUsuarios->id = $idedit;
    $item = $objUsuarios->retornarunico();

    $objUsuarios->id = $item->id;
    $objUsuarios->nome = $item->nome;
    $objUsuarios->data = $item->data;
    $objUsuarios->cidade = $item->cidade;
    $objUsuarios->estado = $item->estado;
    $objUsuarios->genero = $item->genero;
    $objUsuarios->email = $item->email;
    $objUsuarios->senha = $item->senha;
    $objUsuarios->tipo = $item->tipo;
    $objUsuarios->ativo = 't';
    $objUsuarios->imagem = $item->imagem;
    $objUsuarios->atualizar();


    echo "<div class='alert alert-success'> Usuário aceito com Sucesso! <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
}

if (isset($_POST['recusado'], $_POST['id'])) {
    
    $idedit = $_POST['id'];
    $objUsuarios = new Usuarios();
    $objUsuarios->id = $idedit;
    $objUsuarios->excluir();

    echo "<div class='alert alert-danger'> Usuário recusado com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
}
