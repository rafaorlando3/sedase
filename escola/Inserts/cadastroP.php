<?php
include_once '../../class/Carrega.class.php';

if (isset($_POST["titulo"], $_POST["idenquete"])) {
    
        $objPerguntas = new Perguntas();
        $objPerguntas->titulo = pg_escape_string($_POST["titulo"]);
        $objPerguntas->idenquetes = pg_escape_string($_POST["idenquete"]);
        $objPerguntas->inserir();
       
        echo "<div class='alert alert-success'> Pergunta Cadastrada com Sucesso! <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
    }