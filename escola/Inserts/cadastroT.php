<?php
session_start();
include_once '../../class/Carrega.class.php';

$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
if (isset($_POST["nometurma"], $_POST["grau"], $_POST["ano"])) {

    $objEscola = new Escola();
    $lista = $objEscola->listar($comp);
    if ($lista != null) {
        foreach ($lista as $item) {
            $idescola = $item->id;
        }
    }
    
    $objTurma = new Turma();
    $objTurma->idescola = $idescola;
    $objTurma->nome = pg_escape_string($_POST["nometurma"]);
    $objTurma->grau = pg_escape_string($_POST["grau"]);
    $objTurma->ano = pg_escape_string($_POST["ano"]);
    $objTurma->idusuario = $id;
    $objTurma->inserir();

    echo "<div class='alert alert-success'> Turma Cadastrada com Sucesso! <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
}