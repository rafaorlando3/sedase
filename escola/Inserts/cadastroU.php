<?php
include_once '../../class/Carrega.class.php';
include_once '../Scriptsphp/gerarsenha.php';

ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

if (isset($_POST["nome"], $_POST["data"], $_POST["cidade"], $_POST["estado"], $_POST["email"], $_POST["genero"], $_POST["tipo"])) {
    $objUsuarios = new Usuarios();
    $objUsuarioslixo = new Bk_Usuarios();
    $nome = pg_escape_string($_POST["nome"]);
    $email = pg_escape_string($_POST["email"]);
    $data = pg_escape_string($_POST["data"]);
    $senha = geraSenha(6, true, true, true);

    $pesquisa1 = $objUsuarios->listar("where nome ='$nome' or email='$email'");
    $pesquisa2 = $objUsuarioslixo->listar("where nome ='$nome' or email='$email'");
    if ($pesquisa1 == NULL && $pesquisa2 == NULL) {        
        require("../../class/phpmailer/class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->IsSMTP(); // Define que a mensagem será SMTP
        $mail->Host = "smtp.santanacontabilidade.com"; // Endereço do servidor SMTP
        $mail->SMTPAuth = true; // Autenticação
        $mail->Username = 'sedase@santanacontabilidade.com'; // Usuário do servidor SMTP
        $mail->Password = 'sedase2255'; // Senha da caixa postal utilizada
        $mail->From = "sedase@santanacontabilidade.com";
        $mail->FromName = "Sistema SEDASE";

        $mail->AddAddress($email, $nome);
        $mail->AddAddress($email);
        $mail->AddCC('sedase@getsitesolucoes.com.br', 'Copia');
        //$mail->AddBCC('rafaorlando3@gmail.com', 'Copia Oculta');
        $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
        $mail->CharSet = 'UTF-8';

        $mail->Subject = "Cadastro no Sistema - SEDASE"; // Assunto da mensagem
        $mail->Body = 'Você foi cadastrado no sistema <b>SEDASE</b>, abaixo confira suas informacões de login.<br/> <br/> <b>Login:</b> ' . $email . ' <br/> <b>Senha:</b> ' . $senha . ' (Senha gerada automática, sugerimos que mude a senha depois que realizar o primeiro login no nosso sistema.) <br/><br/>  Acesse o sistema, <br> <center><a href="http://sedase.santanacontabilidade.com" target="_blank"><img src="http://sedase.santanacontabilidade.com/images/logoemailsedase.png" width="30%"/></a></center> <br/> Link SEDASE: http://www.sedase.santanacontabilidade.com <br/><br/> Não responda esse e-mail, ele é automático. ';
        $mail->AltBody = 'Você foi cadastrado no sistema <b>SEDASE</b>, abaixo confira suas informacões de login.<br/> <br/> <b>Login:</b> ' . $email . ' <br/> <b>Senha:</b> ' . $senha . ' (Senha gerada automática, sugerimos que mude a senha depois que realizar o primeiro login no nosso sistema.)<br/><br/> Acesse o sistema, <br> <center><a href="http://sedase.santanacontabilidade.com" target="_blank"><img src="http://sedase.santanacontabilidade.com/images/logoemailsedase.png" width="30%"/></a></center> <br/> Link SEDASE: http://www.sedase.santanacontabilidade.com <br/><br/> Não responda esse e-mail, ele é automático.';
        $enviado = $mail->Send();
        $mail->ClearAllRecipients();
        $mail->ClearAttachments();

        if ($enviado) {  
        $objUsuarios->nome = $nome;
        $objUsuarios->genero = pg_escape_string($_POST["genero"]);
        $objUsuarios->data = $data;
        $objUsuarios->cidade = pg_escape_string($_POST["cidade"]);
        $objUsuarios->estado = pg_escape_string($_POST["estado"]);
        $objUsuarios->email = $email;
        $objUsuarios->senha = base64_encode($senha);
        $objUsuarios->tipo = pg_escape_string($_POST["tipo"]);
        $objUsuarios->ativo = 't';
        $objUsuarios->imagem ="user.png";
        $objUsuarios->inserir();
            echo "<div class='alert alert-success'> Usuário Cadastrado com Sucesso! Um e-mail foi enviado para <strong>$email</strong>, verifique. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
            
        } else {
            echo "<div class='alert alert-danger'> Usuário não pode ser cadastrado! <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
            echo "Informações do erro: " . $mail->ErrorInfo;
        }
    } else {
        echo "<div class='alert alert-danger'>Usuário já cadastrado ou Usuário se encontra na Lixeira. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
    }
}
