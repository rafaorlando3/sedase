<?php

include_once '../../class/Carrega.class.php';
date_default_timezone_set("Brazil/East");

if (isset($_POST["titulo"], $_POST["descricao"],$_POST["idusuario"],$_POST["tipo"])) {
    $objForum = new Forum();
    
    if (!empty($_FILES['imagem']['name'])) {

        $ext = strtolower(substr($_FILES['imagem']['name'], -4)); //Pegando extensão do arquivo
        $new_name = date("Y.m.d-H.i.s") . $ext; //Definindo um novo nome para o arquivo
        $dir = '../../uploads/imagens/'; //Diretório para uploads
        move_uploaded_file($_FILES['imagem']['tmp_name'], $dir . $new_name); //Fazer upload do arquivo

        $objForum->titulo = pg_escape_string($_POST["titulo"]);
        $objForum->texto = pg_escape_string($_POST["descricao"]);
        $objForum->imagem = $new_name;
        $objForum->data = date('Y-m-d H:i');
        $objForum->curtir = 0;
        $objForum->idturma = $_POST["idturma"];
        $objForum->resolvido = "f";
        $objForum->idusuario = $_POST["idusuario"];
        $objForum->tipo = $_POST["tipo"];
        $objForum->inserir();

        echo "<div class='alert alert-success'> Tópico cadastrado com sucesso, para entrar na discussão clique no botão <b>Entrar</b>. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
    } else {

        $objForum->titulo = pg_escape_string($_POST["titulo"]);
        $objForum->texto = pg_escape_string($_POST["descricao"]);
        $objForum->imagem = "0";
        $objForum->data = date('Y-m-d H:i');
        $objForum->idturma = $_POST["idturma"];
        $objForum->resolvido = "f";
        $objForum->idusuario = $_POST["idusuario"];
        $objForum->tipo = $_POST["tipo"];
        $objForum->inserir();

        echo "<div class='alert alert-success'> Tópico cadastrado com sucesso, para entrar na discussão clique no botão <b>Entrar</b>.<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
    }
}