<?php
include_once '../../class/Carrega.class.php';

if (isset($_POST['restaurar'], $_POST['id'])) {

    $idedit = $_POST['id'];
    $objBk_Usuarios = new Bk_Usuarios();
    $objBk_Usuarios->id = $idedit;
    $itembkusuario = $objBk_Usuarios->retornarunico();

    $objUsuarios = new Usuarios();
    $objUsuarios->nome = $itembkusuario->nome;
    $objUsuarios->data = $itembkusuario->data;
    $objUsuarios->cidade = $itembkusuario->cidade;
    $objUsuarios->estado = $itembkusuario->estado;
    $objUsuarios->genero = $itembkusuario->genero;
    $objUsuarios->email = $itembkusuario->email;
    $objUsuarios->senha = $itembkusuario->senha;
    $objUsuarios->tipo = $itembkusuario->tipo;
    $objUsuarios->ativo = $itembkusuario->ativo;
    $objUsuarios->imagem = $itembkusuario->imagem;
    $objUsuarios->inserir();

    $objBk_Usuarios->excluir();



    echo "<div class='alert alert-success'> Usuário restaurado com Sucesso! <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
}

if (isset($_POST['excluir'], $_POST['id'])) {

    $idedit = $_POST['id'];
    $objBk_Usuarios = new Bk_Usuarios();
    $objBk_Usuarios->id = $idedit;
    $objBk_Usuarios->excluir();

    echo "<div class='alert alert-danger'> Usuário excluído com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
}
