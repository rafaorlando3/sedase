<?php

date_default_timezone_set("Brazil/East"); 

include_once '../../class/Carrega.class.php';

$objMaterial = new Materiais();
if (isset($_POST["titulo"], $_POST["descricao"], $_POST["idturma"], $_POST["idusuario"], $_POST["tipo"])) {
    
    if (!empty($_FILES['imagem']['name']) && !empty($_FILES['anexo']['name'])) {

      $ext = strtolower(substr($_FILES['imagem']['name'],-4)); //Pegando extensão do arquivo
      $new_name = date("Y.m.d-H.i.s") . $ext; //Definindo um novo nome para o arquivo
      $dir = '../../uploads/imagens/'; //Diretório para uploads
      move_uploaded_file($_FILES['imagem']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
        
      $ext2 = strtolower(substr($_FILES['anexo']['name'],-4)); //Pegando extensão do arquivo
      $new_name2 = date("Y.m.d-H.i.s") . $ext2; //Definindo um novo nome para o arquivo
      $dir2 = '../../uploads/anexos/'; //Diretório para uploads
      move_uploaded_file($_FILES['anexo']['tmp_name'], $dir2.$new_name2); //Fazer upload do arquivo  
            
            
            $objMaterial->titulo = pg_escape_string($_POST["titulo"]);
            $objMaterial->descricao = htmlspecialchars_decode($_POST["descricao"]);
            $objMaterial->idturma = pg_escape_string($_POST["idturma"]);
            $objMaterial->anexo = $new_name2;
            $objMaterial->imagem = $new_name;
            $objMaterial->video = pg_escape_string($_POST["video"]);
            $objMaterial->idusuario = pg_escape_string($_POST["idusuario"]);
            $objMaterial->tipo = pg_escape_string($_POST["tipo"]);
            $objMaterial->inserir();      
      
      
      echo "<div class='alert alert-success'> Material cadastrado com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button></div>";
        
        
    } elseif (!empty($_FILES['anexo']['name'])) {
             
      $ext2 = strtolower(substr($_FILES['anexo']['name'],-4)); //Pegando extensão do arquivo
      $new_name2 = date("Y.m.d-H.i.s") . $ext2; //Definindo um novo nome para o arquivo
      $dir2 = '../../uploads/anexos/'; //Diretório para uploads
      move_uploaded_file($_FILES['anexo']['tmp_name'], $dir2.$new_name2); //Fazer upload do arquivo 
   
            
            $objMaterial->titulo = pg_escape_string($_POST["titulo"]);
            $objMaterial->descricao = htmlspecialchars_decode($_POST["descricao"]);
            $objMaterial->idturma = pg_escape_string($_POST["idturma"]);
            $objMaterial->anexo = $new_name2;
            $objMaterial->imagem = "0";
            $objMaterial->video = pg_escape_string($_POST["video"]);
            $objMaterial->idusuario = pg_escape_string($_POST["idusuario"]);
            $objMaterial->tipo = pg_escape_string($_POST["tipo"]);
            $objMaterial->inserir();         
      
      
      echo "<div class='alert alert-success'> Material cadastrado com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button></div>";
        
      
    } elseif (!empty($_FILES['imagem']['name'])) {

      $ext = strtolower(substr($_FILES['imagem']['name'],-4)); //Pegando extensão do arquivo
      $new_name = date("Y.m.d-H.i.s") . $ext; //Definindo um novo nome para o arquivo
      $dir = '../../uploads/imagens/'; //Diretório para uploads
      move_uploaded_file($_FILES['imagem']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
      
      
            $objMaterial->titulo = pg_escape_string($_POST["titulo"]);
            $objMaterial->descricao = htmlspecialchars_decode($_POST["descricao"]);
            $objMaterial->idturma = pg_escape_string($_POST["idturma"]);
            $objMaterial->anexo = "0";
            $objMaterial->imagem = $new_name;
            $objMaterial->video = pg_escape_string($_POST["video"]);
            $objMaterial->idusuario = pg_escape_string($_POST["idusuario"]);
            $objMaterial->tipo = pg_escape_string($_POST["tipo"]);
            $objMaterial->inserir();   
      
      
            echo "<div class='alert alert-success'> Material cadastrado com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button></div>";
        
    } else {
        
       
            $objMaterial->titulo = pg_escape_string($_POST["titulo"]);
            $objMaterial->descricao = htmlspecialchars_decode($_POST["descricao"]);
            $objMaterial->idturma = pg_escape_string($_POST["idturma"]);
            $objMaterial->anexo = "0";
            $objMaterial->imagem = "0";
            $objMaterial->video = pg_escape_string($_POST["video"]);
            $objMaterial->idusuario = pg_escape_string($_POST["idusuario"]);
            $objMaterial->tipo = pg_escape_string($_POST["tipo"]);
            $objMaterial->inserir();   


        echo "<div class='alert alert-success'> Material cadastrado com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button></div>";
    }
}


