<?php
$menu=7;
include_once 'cabecalho.php';

?>
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Forum</h4>

            </div>

        </div>
        <div class="row">
            <div class="col-md-12">

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                    Cadastrar novo Tópico
                </button>

                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Cadastro de Tópico</h4>
                            </div>
                            <div class="modal-body">

                                <form id="cadastroF" >
                                    <div class="form-group">
                                        <label for="titulo">Título:</label>
                                        <input type="text" name="titulo" class="form-control" id="titulo" placeholder="Digite seu Nome..." required="" />
                                    </div>
                                    <div class="form-group">
                                        <label for="conteudo">Descrição:</label> <br/>
                                        <textarea name="descricao" id="escola" required="" class="form-control"></textarea>
                                    </div>
                                    

                                    <ul class="nav nav-tabs" role="tablist" id="myTab">
                                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Foto</a></li>
                                        
                                    </ul>

                                    <div class="tab-content">
                                        
                                        <div role="tabpanel" class="tab-pane" id="profile">


                                            
                                            <link href="assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
                                            <script src="assets/js/fileinput.js" type="text/javascript"></script>
                                            <script src="assets/js/fileinput.min.js" type="text/javascript"></script>
                                                <div class="form-group">
                                                    <label>Selecione a Foto:</label>
                                                    <input name="imagem" id="file-3" type="file" />
                                                </div>
                                            <script>

                                                $("#file-3").fileinput({
                                                    allowedFileExtensions: ['jpg', 'png', 'gif'],
                                                    showUpload: false,
                                                    showCaption: false,
                                                    browseClass: "btn btn-primary btn-lg",
                                                    fileType: "any",
                                                    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
                                                });
                                            </script>


                                        </div>
                                        
                                    </div>

                                    <script>
                                        $(function () {
                                            $('#myTab a:last').tab('show')
                                        })
                                    </script>
                                    <br/>

                                    <div class="form-group" id="carregaturma">

                                    </div>

                                     <input type="hidden" name="idusuario" value="<?= $id ?>"/>
                                   

                                     <div class="text-center pull-center">  <button type="submit" class="btn btn-primary" id="carregar"  >Cadastrar</button></div>
                                </form> <br/>
                                <div id="resposta"></div> 

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

                            </div>

                            <script type="text/javascript">
                               
                                function atualizaforum() {
                                    jq.get('Selects/carregaforum.php', function (resultado) {
                                        jq('#listaforum').html(resultado);

                                    });
                                }
                                var jq = $.noConflict();
                                jq(document).ready(function () {
                                    atualizaforum();
                                    jq('#cadastroF').submit(function () {
                                        jq('#carregar').attr('disabled', true);
                                        jq("#carregar").html('Carregando...');
                                        jq.ajax({
                                            type: "POST",
                                            url: "Inserts/cadastroF.php",
                                            data: new FormData(this),
                                            processData: false,
                                            contentType: false,
                                            success: function (data)
                                            {   atualizaforum();
                                                jq('#resposta').html(data);
                                                jq('#carregar').attr('disabled', false);
                                                jq("#carregar").html('Cadastrar');
                                            }
                                        });

                                        return false;

                                    });
                                    
                                     atualiza();
                                    function atualiza() {
                                        jq.get('Selects/carregaturma.php', function (resultado) {
                                            jq('#carregaturma').html(resultado);

                                        });
                                    }

 jq("#busca").keypress(function () {
                                    var pesquisa = jq('#busca').val();

                                    jq.get('Selects/carregaforum.php?pesquisa=' + pesquisa, function (resultado) {
                                        jq('#listaforum').html(resultado);

                                    });

                                });

                                });
                            </script>

                        </div>
                    </div>
                </div>
                <!--             Fim modal...-->
                <hr/>
                
 <div class="text-right pull-right"><label>Buscar aqui: </label> <input  type="text" id="busca" value=""  /> </div>

     
                 <h2>Tópicos<small> Listando</small></h2>
                <div id="listaforum">


                </div>
                
                
                
            </div>

        </div>
    </div>
</div>

<?php
include_once 'rodape.php';
