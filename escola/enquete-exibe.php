<?php
$menu = 3;
include_once 'cabecalho.php';


if (isset($_SESSION['idenquete'])) {
    $idedit = $_SESSION['idenquete'];
    $objEnquetes = new Enquetes();
    $objEnquetes->id = $idedit;
    $item = $objEnquetes->retornarunico();
    $_SESSION["idturma"] = $item->idturma;
    $_SESSION["tipooutro"] = $item->tipo;
}


if ($_POST['idedit']) {

    $_SESSION['idenquete'] = $_POST['idedit'];
    $objEnquetes = new Enquetes();
    $objEnquetes->id = $_SESSION['idenquete'];
    $item = $objEnquetes->retornarunico();

    $_SESSION["idturma"] = $item->idturma;
    $_SESSION["tipooutro"] = $item->tipo;
} else {
    header("location:enquetes.php");
}
?>
<div class="content-wrapper">
    <div class="container">
        
        
        <script language="Javascript">

var jq = $.noConflict();

jq(document).ready(function () {
            jq('#excluir').click(function () {
            var resposta = confirm("Você realmente deseja excluir essa Enquete?");
            if (resposta == true) {
                jq('#excluir').attr('disabled', true);
                var dados = jq('#excluirE').serialize();


                jq.ajax({
                    type: "POST",
                    url: "Delets/enquete-del.php",
                    data: dados,
                    success: function (data)
                    {
                        jq("#excluir").html('Excluindo...');
                        jq("#excluir").html('Excluído');
                        jq( "#excluiresp").empty();
                        jq('#excluiresp').html(data);

                        
                    }
                });
            } else {
                jq('#excluir').attr('disabled', false);
                jq("#excluir").html('Excluir');
            }

            return false;


        });
        
});



        </script>
        
        
        <div id="excluiresp">
        
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Enquete: <?= $item->titulo ?>  <?php
if ($item->status == 0) {
    echo "<span class='label label-danger'>Offline</span>";
} else {
    echo "<span class='label label-success'>Online</span>";
}
?></h4>
            </div>

        </div>

        <a href="enquetes.php"><button type="button" class="btn btn-default">Voltar</button></a>

        <button id="excluir" class="btn btn-large btn-danger" >Excluir</button> 

        <form id="excluirE"><input type="hidden" name="idedit" value="<?= $idedit; ?>"></form>

        <p></p>
        <div class="alert alert-warning alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>ATENÇÃO!</strong> Tenha em mente que se você excluir uma Enquete todas as perguntas ligadas a ela também serão excluídas automaticamente.</div>

        <hr/>
        <div class="row">
            <div class="col-md-12">

                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#mostraoculta" aria-expanded="false" aria-controls="mostraoculta" title="Neste botão você atualiza facilmente as informações de sua enquete.">
                    <span class="glyphicon glyphicon-sort" aria-hidden="true" title="Mostrar/Ocultar"></span> Edição de Enquete
                </button>
                <div class="collapse" id="mostraoculta">
                    <div class="well">
                        <form id="editaE" >
                            <div class="form-group">
                                <label for="titulo">Título:</label>
                                <input type="text" name="titulo" class="form-control" id="titulo" value="<?= $item->titulo ?>" placeholder="Digite seu Nome..." required="" />
                            </div>
                            <div class="form-group">
                                <label for="descricao">Descrição:</label> <br/>
                                <textarea name="descricao" id="descricao" required="" class="form-control"><?= $item->genero ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="data">Data:</label>
                                <input type="date" name="data" class="form-control" id="data" value="<?= $item->data ?>" placeholder="" required="" />
                                <input type="hidden" name="idusuario" value="<?= $item->idusuario; ?>"/>
                            </div>

                            <div class="form-group" id="carregaturma">

                            </div>


                            <input type="hidden" name="id" value="<?= $item->id; ?>"/>
                            <div class="form-group">
                                <label for="tipo">Status:</label>
                                <select name="status" required="">
                                    <option value="0" <?php
                    if ($item->status == 0) {
                        echo "selected='selected'";
                    }
?>>Offline</option>
                                    <option value="1" <?php
                                            if ($item->status == 1) {
                                                echo "selected='selected'";
                                            }
                                            ?>>Online</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary" id="carregar"  >Atualizar</button>
                        </form> <br/>
                        <div id="resposta"></div> 
                    </div>
                </div>

                <hr/>




                <button class="btn btn-warning" type="button" data-toggle="collapse" data-target="#mostraocultaperguntas" aria-expanded="false" aria-controls="mostraocultaperguntas" title="Aqui você pode cadastrar perguntas e respostas para essa enquete">
                    <span class="glyphicon glyphicon-sort" aria-hidden="true" title="Mostrar/Ocultar"></span> Perguntas dessa Enquete
                </button>
                <div class="collapse" id="mostraocultaperguntas">
                    <div class="well">

                        <form id="cadastroP" > 
                            <input type="text" name="titulo" id="titulo" placeholder="Digite uma pergunta para essa enquete" required="" style="width: 50%;" />
                            <input type="hidden" name="idenquete" value="<?= $item->id; ?>"/>
                            <button type="submit" id="carregar2" class="btn btn-success">Incluir</button> <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Cadastrar Respostas</button>

                            <div id="respostaperguntas"></div> 
                        </form>     


                        <hr/>
                        <div class="alert alert-warning alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <strong>ATENÇÃO!²</strong> Tenha em mente que se você excluir uma pergunta todas as opções ligadas a ela também serão excluídas automaticamente.</div>
                        <div id="respostapergu"></div>
                        <div id="listaperguntas"></div>
                    </div>

                </div>

                <hr/>


                <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#visualizarenquete" aria-expanded="false" aria-controls="mostraocultaperguntas" title="Veja como está ficando essa enquete">
                    <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true" title="Mostrar/Ocultar"></span> Preview Enquete
                </button>
                <div class="collapse" id="visualizarenquete">
                    <div class="well">

                        <div id="previewenquete"></div>
                    </div>

                </div>



            </div>


            <script type="text/javascript">

                function atualizaperguntas() {
                    jq.get('Selects/carregaperguntas.php', function (resultado) {
                        jq('#listaperguntas').html(resultado);

                    });
                }
                function visualizaenquete() {
                    jq.get('Selects/visualizaenquete.php?idenquete=<?= $item->id ?>', function (resultado) {
                        jq('#previewenquete').html(resultado);

                    });
                }

                jq(document).ready(function () {
                    jq('#editaE').submit(function () {
                        var dados = jq(this).serialize();
                        jq('#carregar').attr('disabled', true);
                        jq("#carregar").html('Carregando...');
                        jq.ajax({
                            type: "POST",
                            url: "Updates/editaE.php",
                            data: dados,
                            success: function (data)
                            {
                                jq('#resposta').html(data);
                                jq('#carregar').attr('disabled', false);
                                jq("#carregar").html('Atualizar');
                            }
                        });

                        return false;

                    });

                    atualiza();
                    function atualiza() {
                        jq.get('Selects/carreturmaupdate.php', function (resultado) {
                            jq('#carregaturma').html(resultado);

                        });
                    }

                    visualizaenquete();

                });


                jq(document).ready(function () {
                    jq('#cadastroP').submit(function () {
                        var dados = jq(this).serialize();
                        jq('#carregar2').attr('disabled', true);
                        jq("#carregar2").html('Carregando...');
                        jq.ajax({
                            type: "POST",
                            url: "Inserts/cadastroP.php",
                            data: dados,
                            success: function (data)
                            {
                                atualizaperguntas();
                                visualizaenquete()
                                jq('#respostaperguntas').html(data);
                                jq('#carregar2').attr('disabled', false);
                                jq("#carregar2").html('Incluir');
                            }
                        });

                        return false;

                    });


                });

                jq(document).ready(function () {
                    atualizaperguntas();
                    jq("#data").datepicker({
                        dateFormat: 'dd/mm/yy',
                        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
                        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                        nextText: 'Próximo',
                        prevText: 'Anterior'
                    });
                });
            </script>

        </div>
        </div>
    </div>
</div>

<?php
include_once 'rodape.php';
