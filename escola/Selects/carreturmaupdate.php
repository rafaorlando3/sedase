<?php
session_start();
include_once '../../class/Carrega.class.php';
$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
$idturma = isset($_SESSION['idturma']) ? $_SESSION['idturma'] : '';
$tipo = isset($_SESSION['tipooutro']) ? $_SESSION['tipooutro'] : '';
?>           
<script src="assets/js/jquery-3.1.0.js"></script>

<div class="alert alert-info alert-dismissible fade in" role="alert"><strong>Dicas:</strong> Você pode deixar essa funcionalidade pública, basta selecionar a opção "Nenhuma turma (Para o Público)" no campo <strong>Turma</strong>. No botão <strong>"[+]"</strong> você pode cadastrar novas turmas.</div>


<label for="turma">Turma:</label>
<?php
$objTurma = new Turma();
$listaturma = $objTurma->listar("where idusuario=$id order by grau ASC");
if ($listaturma != null) {
    
    echo "<select id='idturma' name='idturma' >";
    foreach ($listaturma as $itemturma) {
        ?>
        <option value="<?= $itemturma->id ?>" <?php if ($itemturma->id == $idturma) {echo "selected='selected'";}?> >
        <?= "Turma: ".$itemturma->nome." - Ano: ".$itemturma->ano." - Grau: ".$itemturma->grau ?>
        </option>
        

        <?php
    }
    
    ?>
        
        <option value="null" <?php if ($idturma==null) {echo "selected='selected'";}?>> Nenhuma turma (Para o Público)</option>
</select>   
<button type="button" id="turmaadd" class="btn btn-default" title="Adicionar uma nova turma">+</button>


<div id="div2" class="form-group">
                                <label for="tipo">Disponível em meio:</label>
                                <select name="tipo" required="">
                                    
                                    <option value="1" <?php
                                    if ($tipo == 1) {
                                        echo "selected='selected'";
                                    }
?>>Privado para Alunos</option>
                                    <option value="2" <?php
                                    if ($tipo == 2) {
                                        echo "selected='selected'";
                                    }
?>>Público para todos</option>
                                </select>
                                
                            </div>


        <?php
        
} else {
    
    ?>
<select name='idturma' id='idturma'>
            <option value="null"> Nenhuma turma (Para o Público)</option>
</select>
<label class='label label-warning'>Nenhuma turma vinculada a você</label> <button type="button" id="turmaadd" class="btn btn-default" title="Adicionar uma nova turma">+</button>
 
<div id="div2" class="form-group">
                                        <label for="tipo">Disponível em meio:</label>
                                        <select name="tipo" id="tipo" required="">
                                            <option value="2">Público para todos</option></select>
                                        </select>
</div>
  
    <?php
}
?>

<div id="cadastroturma"></div>  


<script type="text/javascript">
    var j = $.noConflict();
    j(document).ready(function () {
        j("#turmaadd").click(function () {
            j.get('turma-add.php', function (resultado2) {
                j('#cadastroturma').html(resultado2);

            });
        });
        
        
    jq( "#idturma" ).change(function() {
    var grau = this.value;
    if(grau==="null"){
      var conteudo='<label for="tipo">Disponível em meio:</label><select name="tipo" id="tipo" required=""><option value="2">Público para todos</option></select>';  
      jq('#div2').html(conteudo);  
    }
    else{
       var conteudo='<label for="tipo">Disponível em meio:</label><select name="tipo" id="tipo" required=""> <option value="1">Privado para Alunos</option></select>';  
      jq('#div2').html(conteudo); 
    }

   });
   
   });
</script>


