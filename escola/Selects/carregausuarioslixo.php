<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');

//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);

if (isset($_SESSION["nome"], $_SESSION["iduser"], $_SESSION["tipo"])) {
    $id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
    $nome = isset($_SESSION['nome']) ? $_SESSION['nome'] : '';
    $tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';
} else {
    echo "<script>window.location.href ='index.php';</script>";
}
?>


<div class="table-responsive">
    <table class="table table-striped" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Nascimento</th>
                <th>Este usuário é</th>
                <th>Cidade</th>
                <th>Escola</th>
                <th>Aprovar</th>
                <th>Recusar</th>
            </tr>
        </thead>
        <tbody>

            <?php
            $mensagem = "";
            if ($tipo == 1) {
                $comp = "";
            } elseif ($tipo == 2) {
                $comp = "where tipo=1 ";
            }
            $objUsuarios = new Bk_Usuarios();
            $lista = $objUsuarios->listar($comp);
            if ($lista != null) {

                foreach ($lista as $item) {
                    ?>


                    <tr>
                        <td><?= $item->nome; ?></td>
                        <td><?= date("d/m/Y", strtotime($item->data)) ?></td>
                        <td>
                            <?php
                            if ($item->tipo == 1) {
                                echo "<label class='label label-warning'>Administrador</label>";
                            } elseif ($item->tipo == 2) {
                                echo "<label class='label label-success'>Professor</label>";
                            } else {
                                echo "<label class='label label-info'>Aluno</label>";
                            }
                            ?>
                        </td>
                        <td><?= $item->cidade; ?></td>
                        <td>Escola</td>
                        <td> <form id='solicitacao<?= $item->id ?>'> <input type="hidden" name="id" value="<?= $item->id; ?>"/> <input type="hidden" name="restaurar" value=""/> <button id="carregar" type="submit"  class="btn btn-xs btn-success"  >Restaurar</button></form>  </td>
                        <td><form id='solicitacao2<?= $item->id ?>'> <input type="hidden" name="id" value="<?= $item->id; ?>"/> <input type="hidden" name="excluir" value=""/><button id="carregar2" type="submit"  class="btn btn-xs btn-danger"  >Excluir</button></form></td>
                    </tr>




                <script type="text/javascript">

                    jq(document).ready(function () {
                        jq('#solicitacao<?= $item->id ?>').submit(function () {
                            var dados = jq(this).serialize();
                            jq('#carregar').attr('disabled', true);
                            jq("#carregar").html('Carregando...');
                            jq.ajax({
                                type: "POST",
                                url: "Inserts/respostabkusuarios.php",
                                data: dados,
                                success: function (data)
                                {
                                    atualizausuarios();
                                    atualizausuarioslixeira();
                                    jq('#respostaexcluidos').html(data);
                                    jq('#carregar').attr('disabled', false);
                                    jq("#carregar").html('Aprovar');
                                }
                            });

                            return false;

                        });


                        jq('#solicitacao2<?= $item->id ?>').submit(function () {
                            var dados = jq(this).serialize();
                            jq('#carregar2').attr('disabled', true);
                            jq("#carregar2").html('Carregando...');
                            jq.ajax({
                                type: "POST",
                                url: "Inserts/respostabkusuarios.php",
                                data: dados,
                                success: function (data)
                                {
                                    atualizausuarios();
                                    atualizausuarioslixeira();
                                    jq('#respostaexcluidos').html(data);
                                    jq('#carregar2').attr('disabled', false);
                                    jq("#carregar2").html('Recusar');
                                }
                            });

                            return false;

                        });


                    });
                </script>




        <?php
    }
} else {
    $mensagem = "<div class='alert alert-info'>Nenhum usuário encontrado na lixeira até o momento.</div>";
}
?>   



        </tbody>
    </table>

</div>
<?= $mensagem ?>




