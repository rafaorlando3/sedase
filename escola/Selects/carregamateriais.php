<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');

$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
$nome = isset($_SESSION['nome']) ? $_SESSION['nome'] : '';
$tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';
?>


<div class="table-responsive">
    <table class="table table-striped" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="text-left">
                                <b>Título</b>
                            </th>
                            
                             <th class="text-center">
                                <b>Autor</b>
                            </th>

                            <th class="text-center">
                                <b>Tópico para</b>
                            </th>

                            <th class="text-right">
                                <b>Ação</b>
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                        if($tipo==1){
                            $comp="";
                            
                            if ($_GET['pesquisa']) {
                    $pesquisa = $_GET['pesquisa'];
                    $comp = "where titulo LIKE '%$pesquisa%' order by titulo ASC" ;
                } else {
                    $pesquisa = "";
                    $comp = "order by titulo ASC";
                }
                            
                        }
                        
                        else{
                            
                            
                            if ($_GET['pesquisa']) {
                    $pesquisa = $_GET['pesquisa'];
                    $comp = "where titulo LIKE '%$pesquisa%' and idusuario=$id or idturma is NULL order by titulo ASC" ;
                } else {
                    $pesquisa = "";
                    $comp = "where idusuario=$id or idturma is NULL order by titulo ASC";
                }
                            
                        }
                        
                       $objMateriais = new Materiais();
                        $lista = $objMateriais->listar($comp);
                        if ($lista != null) {
                            $mensagem = "";
                            $objUsuarios= new Usuarios();
                            foreach ($lista as $item) {
                                
                                $objUsuarios->id = $item->idusuario;
                                $itemuser=$objUsuarios->retornarunico();
                                
                                if($item->idturma==null){
                                    
                                   $turma="<label class='label label-info'>Público</label>";
                                }
                                else{
                                    $objTurma= new Turma();
                                    $objTurma->id=$item->idturma;
                                    $itemturma=$objTurma->retornarunico();
                                    $turma="<label class='label label-info'>$itemturma->nome</label>";
                                }
                                
                                ?>


                                <tr>
                                    <td class="text-left"> <?= $item->titulo; ?></td>
                                    <td class="text-center"> <label class='label label-success'><?=$itemuser->nome?> </label></td>
                                    <td class="text-center"> <?=$turma?> </td>
                                    <td class="text-right"><form method="post" action="materiais-edit.php"><input type="hidden" name="idedit" value="<?= $item->id ?>"/><button type="submit" class="btn btn-primary">Editar</button></form> </td>
                                </tr>


                                <?php
                            }
                        } else {
                            $mensagem = "<div class='alert alert-info'>Nenhum registro cadastrado até o momento.</div>";
                        }
                        ?>   

                    </tbody>
                    <tfoot></tfoot>



                </table>
</div>
                <?= $mensagem ?>
