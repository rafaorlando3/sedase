<?php
session_start();
include_once '../../class/Carrega.class.php';
$idenquete = $_SESSION['idenquete'];
?>

<script src="assets/js/jquery-3.1.0.js"></script>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Cadastro de Respostas</h4>
            </div>
            <div class="modal-body">

                <div class="alert alert-warning alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>ATENÇÃO!³</strong> Prudência se você precisar excluir alguma opção.</div>
                <p></p>
                <label for="pergunta">Selecione a Pergunta:</label> <br/>
                <select name="pergunta" id="pergunta" >
                    <option value="selecione" >Selecione uma Pergunta</option>
                    <?php
                    $objPerguntas = new Perguntas();
                    $lista = $objPerguntas->listar("where idenquetes=" . $idenquete . "");
                    if ($lista != null) {
                        $mensagem = "";
                        foreach ($lista as $itempergunta) {
                            ?>

                            <option value="<?= $itempergunta->id; ?>"><?= $itempergunta->titulo; ?></option> 

                            <?php
                        }
                    } else {
                        $mensagem = "<div class='alert alert-info'>Nenhum registro cadastrado até o momento.</div>";
                    }
                    ?>   

                </select>
                <?= $mensagem ?>

                <hr>

                <label for="pergunta">Insira uma opção:</label> <br/>
                <form id="cadastroO" >      
                    <div class="form-group">
                        <label for="nome">Título:</label>
                        <input type="text" name="titulo" class="form-control" id="nome" placeholder="Digite o título para esta opção..." required="" />
                    </div>
                    <input type="hidden" name="idpergunta" id="idpergunta" value=""/>
                    <button type="submit" id="carregar3" class="btn btn-success">Incluir</button>


                    <div id="respostaopcoes"></div> 
                </form>


                <div id="respostaopcao"></div> 
                <hr/>
                <h4 class="modal-title" id="myModalLabel">Listando opções para a pergunta selecionada.</h4>

                <div id="listaopcoes"></div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

            </div>


        </div>
    </div>
</div>
<!--             Fim modal...-->



<div class="table-responsive">
    <table class="table table-striped" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>
                    <b>Título</b>
                </th>
                <th>
                    <b>Ação</b>
                </th>

            </tr>
        </thead>
        <tbody>
            <?php
            $objPerguntas2 = new Perguntas();
            $lista2 = $objPerguntas2->listar("where idenquetes=" . $idenquete . "");
            if ($lista2 != null) {
                $mensagem = "";

                foreach ($lista2 as $itempergunta) {
                    ?>



                    <tr>
                        <td> <?= $itempergunta->titulo; ?></td>
                        <td> 

                            <form id="excluirperg<?= $itempergunta->id; ?>">
                                <input type="hidden" value="<?= $itempergunta->id; ?>" name="idpergunta"/>
                                <button type="submit" id="carregarexcluir" class="btn btn-danger" title="Excluir">Excluir</button>
                            </form>



                            <script type="text/javascript">

                                $(document).ready(function () {
                                    $('#excluirperg<?= $itempergunta->id ?>').submit(function () {
                                        var dados = $(this).serialize();
                                        $('#carregarexcluir').attr('disabled', true);
                                        $("#carregarexcluir").html('Carregando...');
                                        $.ajax({
                                            type: "POST",
                                            url: "Delets/pergunta-del.php",
                                            data: dados,
                                            success: function (data)
                                            {   visualizaenquete2();
                                                atualizaperguntas();
                                                $('#respostapergu').html(data);
                                                $('#excluirperg').attr('disabled', false);
                                                $("#excluirperg").html('Excluir');
                                            }
                                        });

                                        return false;

                                    });
                                });


                            </script>




                        </td>
                    </tr>


                    <?php
                }
            } else {
                $mensagem = "<div class='alert alert-info'>Nenhum registro cadastrado até o momento.</div>";
            }
            ?>   

        </tbody>
        <tfoot></tfoot>



    </table>

</div>
<?= $mensagem ?>

<script type="text/javascript">
  var jqa = $.noConflict();
  function visualizaenquete2() {
                    jqa.get('Selects/visualizaenquete.php?idenquete=<?=$idenquete?>', function (resultado) {
                        jqa('#previewenquete').html(resultado);

                    });
                }
    
    
    
    var valor;
    jqa(document).ready(function () {
        jqa('#carregar3').attr('disabled', true);
        jqa('#pergunta').on('change', function () {
            valor = this.value;
            jqa.ajax({
                type: 'post',
                url: 'Selects/carregaopcoes.php',
                data: {
                    get_option: valor
                },
                success: function (response) {
                    jqa('#carregar3').attr('disabled', false);
                    //document.getElementById("listaopcoes").innerHTML = response;
                    jqa('#listaopcoes').html(response);
                    if (valor === "selecione") {
                        jqa('#carregar3').attr('disabled', true);
                    }
                    jqa('#idpergunta').val(valor);
                }
            });

        });
    });

    jqa(document).ready(function () {
        jqa('#cadastroO').submit(function () {
            var dados = jqa(this).serialize();
            jqa('#carregar3').attr('disabled', true);
            jqa("#carregar3").html('Carregando...');
            jqa.ajax({
                type: "POST",
                url: "Inserts/cadastroO.php",
                data: dados,
                success: function (data)
                {
                    jqa('#respostaopcoes').html(data);
                    jqa('#carregar3').attr('disabled', false);
                    jqa("#carregar3").html('Incluir');

                    jqa.ajax({
                        type: 'post',
                        url: 'Selects/carregaopcoes.php',
                        data: {
                            get_option: valor
                        },
                        success: function (response) {
                            visualizaenquete2();
                            jqa('#listaopcoes').html(response);
                            
                        }
                    });




                }
            });

            return false;
        });


    });


</script>

