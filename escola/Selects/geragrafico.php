<script src="../assets/js/Chart.bundle.js"></script> 
<script src="../assets/js/utils.js"></script> 

  <div class="row">
                <div class="text-center">

                    <div class="text-center" >
                        <h4 class="text-center">Grafico exemplo</h4>
                        <canvas id="grafico"> </canvas>
     
                    </div>
                    
                      <div class="row">
                <div class="text-center">

                    <div class="text-center" >
                        <h4 class="text-center">Grafico exemplo 2</h4>
                        <canvas id="grafico2"> </canvas>
     
                    </div> 


<script>
            var graficos = {
                type: 'pie',
                data: {
                    datasets: [{
                            data: [
                                1,
                                2,
                                3
                            ],
                            backgroundColor: [
                                window.chartColors.red,
                                window.chartColors.orange,
                                window.chartColors.yellow
                            ],
                            label: 'Dataset 1'
                        }],
                    labels: [
                        "Administrador",
                        "Professor",
                        "Aluno"
                    ]
                },
                options: {
                    responsive: true
                }
            };
           
                var ctxgraf = document.getElementById("grafico").getContext("2d");
                window.myPie = new Chart(ctxgraf, graficos);
                
                var ctxgraf2 = document.getElementById("grafico2").getContext("2d");
                window.myPie = new Chart(ctxgraf2, graficos);
                
           

        </script>
