<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');

$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
$nome = isset($_SESSION['nome']) ? $_SESSION['nome'] : '';
$tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';
$idturma = isset($_GET['idturma']) ? $_GET['idturma'] : '';

if ($_GET['idturma'] != NULL) {
    ?>


    <div class="table-responsive">
        <table class="table table-striped" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th class="text-left">
                        <b>Aluno</b>
                    </th>

                    <th class="text-center">
                        <b>Email/Matrícula</b>
                    </th>
                    <th class="text-center">
                        <b>Status</b>
                    </th>

                    <th class="text-right">
                        <b>Ação</b>
                    </th>

                </tr>
            </thead>
            <tbody>
                <?php
                $objUsuarios = new Usuarios();
                $lista = $objUsuarios->listarNVinculados();
                if ($lista != null) {
                    $objTurma = new Turma();
                    $objTurma->id = $idturma;
                    $itemturma = $objTurma->retornarunico();
                    foreach ($lista as $item) {

                        $botaovinc = '<form id="vincula' . $item->id . '" ><input type="hidden" name="idusuario" value="' . $item->id . '"/><input type="hidden" name="idturma" value="' . $idturma . '"/> <input type="hidden" name="nome" value="' . $itemturma->nome . '"/>  <button type="submit" id="botvincula" class="btn btn-success">Vincular</button></form>';
                        $label = '<label class="label label-warning">Sem vínculo.</label>';
                        ?>
                        <tr>
                            <td class="text-left"> <?= $item->nome; ?></td>
                            <td class="text-center"> <?= $item->email; ?></td>
                            <td class="text-center"> <?= $label; ?></td>
                            <td class="text-right"><?= $botaovinc ?></td>
                        </tr>

                    <script type="text/javascript">

                        jq(document).ready(function () {
                            jq('#vincula<?= $item->id ?>').submit(function () {
                                var dados = jq(this).serialize();
                                jq('#botvincula').attr('disabled', true);
                                jq("#botvincula").html('Carregando...');
                                jq.ajax({
                                    type: "POST",
                                    url: "Inserts/cadastroV.php",
                                    data: dados,
                                    success: function (data)
                                    {
                                        jq('#respostavinculacao').html(data);
                                        jq('#botvincula').attr('disabled', true);
                                        jq("#botvincula").html('Vinculado');

                                        mostrar();
                                        function mostrar() {
                                            jq.get('Selects/vinculalunos.php?idturma=' + <?= $idturma ?>, function (resultado) {
                                                jq('#respostavincula').html(resultado);

                                            });

                                            jq.get('Selects/desvincula.php?idturma=' + <?= $idturma ?>, function (resultado) {
                                                jq('#respostavincula2').html(resultado);

                                            });
                                        }

                                    }
                                });

                                return false;

                            });

                        });
                    </script>
                    <?php
                }
            } else {
                $mensagem = "<div class='alert alert-info'>Nenhum aluno para vincular até o momento.</div>";
            }
            ?>   

            </tbody>
            <tfoot></tfoot>



        </table>

    </div>
    <?= $mensagem ?>



    <?php
} else {
    echo "<div class='alert alert-info'>Selecione uma turma.</div>";
}

