<?php
include_once '../../class/Carrega.class.php';
session_start();
?> 

<?php
if (isset($_POST['get_option'])) {

    $_SESSION['idpergunta'] = $_POST['get_option'];
    $idpergunta = $_SESSION['idpergunta'];
    $idenquete = $_SESSION['idenquete'];
    ?>


<div class="table-responsive">
    <table class="table table-striped" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th class="text-left">
                        <b>Título</b>
                    </th>
                    <th class="text-right">
                        <b>Ação</b>
                    </th>

                </tr>
            </thead>
            <tbody>
                <?php
                $objOpcoes = new Opcoes();
                $listaopcoes = $objOpcoes->listar("where idperguntas=" . $idpergunta . "");
                if ($listaopcoes != null) {
                    $mensagem2 = "";
                    foreach ($listaopcoes as $itemopcao) {
                        ?>



                        <tr>
                            <td class="text-left"> <?= $itemopcao->texto; ?></td>
                            <td class="text-right"> 

                                <form id='excluiropc<?= $itemopcao->id; ?>'>
                                    <input type="hidden" value="<?= $itemopcao->id; ?>" name="idopcao"/>
                                    <button type="submit" id="carregarexcluiro" class="btn btn-danger" title="Excluir">Excluir</button>
                                </form>    
                            </td>
                        </tr>

                    <script type="text/javascript">
  function visualizaenquete3() {
                    jqa.get('Selects/visualizaenquete.php?idenquete=<?=$idenquete?>', function (resultado) {
                        jqa('#previewenquete').html(resultado);

                    });
                }
    
                        jqa(document).ready(function () {
                            jqa('#excluiropc<?= $itemopcao->id; ?>').submit(function () {
                                var dados = jqa(this).serialize();
                                jqa('#carregarexcluiro').attr('disabled', true);
                                jqa("#carregarexcluiro").html('Carregando...');
                                jqa.ajax({
                                    type: "POST",
                                    url: "Delets/opcao-del.php",
                                    data: dados,
                                    success: function (data)
                                    {
                                        jqa('#respostaopcao').html(data);
                                        jqa('#carregarexcluiro').attr('disabled', false);
                                        jqa("#carregarexcluiro").html('Excluir');


                                        jqa.ajax({
                                            type: 'post',
                                            url: 'Selects/carregaopcoes.php',
                                            data: {
                                                get_option: <?= $idpergunta; ?>
                                            },
                                            success: function (response) {
                                                visualizaenquete3();
                                                jqa('#listaopcoes').html(response);
                                                
                                            }
                                        });

                                    }
                                });

                                return false;

                            });
                        });

                    </script>
                    <?php
                }
            } else {
                $mensagem2 = "<div class='alert alert-info'>Nenhum registro encontrado.</div>";
            }
        }
        ?>   

        </tbody>
        <tfoot></tfoot>



    </table>

</div>

<?= $mensagem2 ?>
