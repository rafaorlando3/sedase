<?php
session_start();
include_once '../../class/Carrega.class.php';
$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
$tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';
?>           
<script src="assets/js/jquery-3.1.0.js"></script>

<select name="idturma" id='idturma'>
                                <option value="selecione">Selecione</option>
                                <?php
                                if($tipo==1){
                                    $filtro="";
                                } else{
                                    $filtro="where idusuario=$id";
                                }
                                
                                $objTurma = new Turma();
                                $listaturma = $objTurma->listar($filtro);
                                if ($listaturma != null) {
                                    foreach ($listaturma as $itemturma) {
                                        ?>
                                        <option value="<?= $itemturma->id ?>"><?= "Turma: " . $itemturma->nome . " - Ano: " . $itemturma->ano . " - Grau: " . $itemturma->grau ?></option>


                                        <?php
                                    }
                                    ?>
                                </select>

                                <?php
                            } else {
                                ?>
                                <div class='alert alert-info'>Nenhuma Turma encontrada até o momento.</div>

                                <?php
                            }
                            ?>  
<script>
jq(document).ready(function () {
        jq('#idturma').on('change', function () {
            var valor = this.value;

            if (valor === "selecione") {
                jq('#respostavincula').html("Selecione uma turma válida.");
                jq('#respostavincula2').html("Selecione uma turma válida.");
            } else {

                mostrar();
                function mostrar() {
                    jq.get('Selects/vinculalunos.php?idturma=' + valor, function (resultado) {
                        jq('#respostavincula').html(resultado);

                    });

                    jq.get('Selects/desvincula.php?idturma=' + valor, function (resultado) {
                        jq('#respostavincula2').html(resultado);

                    });

                }
            }

        });                                    
 });

</script>