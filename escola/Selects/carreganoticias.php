<?php
include_once '../../class/Carrega.class.php';
?>

<div class="table-responsive">
    <table class="table table-striped" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="text-left">
                                <b>Título</b>
                            </th>

                            <th class="text-center">
                                <b>Data</b>
                            </th>

                            <th class="text-right">
                                <b>Ação</b>
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                        if ($_GET['pesquisa']) {
                    $pesquisa = $_GET['pesquisa'];
                    $comp = "where titulo LIKE '%$pesquisa%' order by titulo ASC" ;
                } else {
                    $pesquisa = "";
                    $comp = "order by titulo ASC";
                }
                        
                        
                        $objNoticias = new Noticias();
                        $lista = $objNoticias->listar($comp);
                        if ($lista != null) {
                            $mensagem = "";

                            foreach ($lista as $item) {
                                ?>


                                <tr>
                                    <td class="text-left"> <?= $item->titulo; ?></td>
                                    <td class="text-center"> <?= date("d/m/Y", strtotime($item->data)) ?></td>
                                    <td class="text-right"><form method="post" action="noticia-edit.php"><input type="hidden" name="idedit" value="<?= $item->id ?>"/><button type="submit" class="btn btn-primary">Editar</button></form> </td>
                                </tr>


                                <?php
                            }
                        } else {
                            $mensagem = "<div class='alert alert-info'>Nenhum registro cadastrado até o momento.</div>";
                        }
                        ?>   

                    </tbody>
                    <tfoot></tfoot>



                </table>
</div>
                <?= $mensagem ?>
