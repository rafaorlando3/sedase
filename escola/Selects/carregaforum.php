<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');

$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
$nome = isset($_SESSION['nome']) ? $_SESSION['nome'] : '';
$tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';

 $objUsuarios = new Usuarios();
 $objUsuarios->id = $id;
 $itemuser = $objUsuarios->retornarunico();


?>

<div class="table-responsive">
    <table class="table table-striped" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="text-center">
                                <b>Título</b>
                            </th>
                            
                            
                            <th class="text-center">
                                <b>Autor</b>
                            </th>

                            <th class="text-center">
                                <b>Data</b>
                            </th>
                            
                            <th class="text-center">
                                <b>Tópico para</b>
                            </th>
                            
                            <th class="text-center">
                                <b>Ação</b>
                            </th>
                            <th class="text-center">
                                <b>Discussão</b>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                       $objForum = new Forum();
                       
                         if($tipo==1){
                            $comp="";
                            
                            if ($_GET['pesquisa']) {
                    $pesquisa = $_GET['pesquisa'];
                    $comp = "where titulo LIKE '%$pesquisa%' order by titulo ASC" ;
                } else {
                    $pesquisa = "";
                    $comp = "order by titulo ASC";
                }
                            
                        }
                        
                        else{
                            
                            
                            if ($_GET['pesquisa']) {
                    $pesquisa = $_GET['pesquisa'];
                    $comp = "where idusuario=$id or idturma is NULL and titulo LIKE '%$pesquisa%' order by titulo ASC" ;
                } else {
                    $pesquisa = "";
                    $comp = "where idusuario=$id or idturma is NULL order by titulo ASC";
                }
                            
                        }
                      
                        $lista = $objForum->listar($comp);
                        if ($lista != null) {
                            $mensagem = "";

                            foreach ($lista as $item) {
                                $objUsuarios= new Usuarios();
                                $objUsuarios->id = $item->idusuario;
                                $itemuser=$objUsuarios->retornarunico();
                                
                                if($item->idturma==null){
                                    
                                   $turma="<label class='label label-info'>Público</label>";
                                }
                                else{
                                    $objTurma= new Turma();
                                    $objTurma->id=$item->idturma;
                                    $itemturma=$objTurma->retornarunico();
                                    $turma="<label class='label label-info'>$itemturma->nome</label>";
                                }
                                
                                ?>


                                <tr>
                                    <td class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center"> <?= $item->titulo; ?></td>
                                    <td class="text-center"><label class='label label-success'> <?= $itemuser->nome; ?></label></td>
                                    <td class="text-center"> <?= date("d-m-Y",strtotime($item->data)); ?></td>
                                    <td class="text-center"> <?=$turma?></td>
                                  <td class="text-center">  <?php if($item->idusuario==$id || $tipo==1){  ?><form method="post" action="forum-edit.php"><input type="hidden" name="idedit" value="<?= $item->id ?>"/><button type="submit" class="btn btn-primary">Editar</button><?php } else{ echo "<label class='label label-danger'>Você nao pode editar este tópico</label>"; }?></td>
                                <td class="text-center"> </form> <form method="post" action="forumenter.php"><input type="hidden" name="idedit" value="<?= $item->id ?>"/><button type="submit" class="btn btn-success">Entrar</button></form></td>
                               
                                </tr>


                                <?php
                            }
                        } else {
                            $mensagem = "<div class='alert alert-info'>Nenhum registro cadastrado até o momento.</div>";
                        }
                        ?>   

                    </tbody>
                    <tfoot></tfoot>



                </table>
</div>
                <?= $mensagem ?>
