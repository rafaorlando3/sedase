<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');

//resumo de usuários    
function quantUsu($tipo,$ativo) {
        $sql = "select count(id) from usuarios where tipo=$tipo and ativo='$ativo'";
        $resultado = pg_query($sql);
        $retorno = pg_fetch_row($resultado);
        
        if($retorno[0]==NULL){
          $ultimo=0;  
        }
        else{
        
        $ultimo=$retorno[0];
        
        }

        return $ultimo;
 
    }
    
// resumo de Turmas
function quantTurmas($grau) {
    $sql = "select count(id) from turma where grau=$grau";
        $resultado = pg_query($sql);
        $retorno = pg_fetch_row($resultado);
        
        if($retorno[0]==NULL){
          $ultimo=0;  
        }
        else{
        
        $ultimo=$retorno[0];
        
        }

        return $ultimo;
 
    }
    
    // resumo de Enquetes
function quantEnquetes($online) {
    $sql = "select count(id) from enquetes where status=$online";
        $resultado = pg_query($sql);
        $retorno = pg_fetch_row($resultado);
        
        if($retorno[0]==NULL){
          $ultimo=0;  
        }
        else{
        
        $ultimo=$retorno[0];
        
        }

        return $ultimo;
 
    }
    
// resumo de Materiais
function quantMateriais($condicao) {
    $sql = "select count(id) from materiais $condicao";
        $resultado = pg_query($sql);
        $retorno = pg_fetch_row($resultado);
        
        if($retorno[0]==NULL){
          $ultimo=0;  
        }
        else{
        
        $ultimo=$retorno[0];
        
        }

        return $ultimo;
 
    }
    
// resumo do Fórum
function quantForum($condicao) {
    $sql = "select count(id) from forum $condicao";
        $resultado = pg_query($sql);
        $retorno = pg_fetch_row($resultado);
        
        if($retorno[0]==NULL){
          $ultimo=0;  
        }
        else{
        
        $ultimo=$retorno[0];
        
        }

        return $ultimo;
 
    }

// resumo do Respostas Fórum
function quantRespostas($condicao) {
    $sql = "select count(id) from respostas $condicao";
        $resultado = pg_query($sql);
        $retorno = pg_fetch_row($resultado);
        
        if($retorno[0]==NULL){
          $ultimo=0;  
        }
        else{
        
        $ultimo=$retorno[0];
        
        }

        return $ultimo;
 
    }