<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');
//
//ini_set('display_errors', 1);
//ini_set('display_startup_erros', 1);
//error_reporting(E_ALL);


if (isset($_SESSION["nome"], $_SESSION["iduser"], $_SESSION["tipo"])) {
    $id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
    $nome = isset($_SESSION['nome']) ? $_SESSION['nome'] : '';
    $tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';
} else {
    echo "<script>window.location.href ='index.php';</script>";
}
?>

<div class="table-responsive">

    <table class="table table-striped" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="text-left">
                    <b>Nome</b>
                </th>

                <th class="text-center">
                    <b>Grau</b>
                </th>

                <th class="text-center">
                    <b>Ano</b>
                </th>

                <th class="text-right">
                    <b>Ação</b>
                </th>

            </tr>
        </thead>
        <tbody>
            <?php
            $mensagem = "";

            $objTurmas = new Turma();

            if ($tipo == 1) {

                if ($_GET['pesquisa']) {
                    $pesquisa = $_GET['pesquisa'];
                    $complemento = "where nome LIKE '%$pesquisa%'";
                } else {
                    $pesquisa = "";
                    $complemento = "";
                }
            } else {
                if ($_GET['pesquisa']) {
                    $pesquisa = $_GET['pesquisa'];
                    $complemento = "where nome LIKE '%$pesquisa%'  and idusuario=$id order by grau ASC";
                } else {
                    $pesquisa = "";
                    $complemento = "where idusuario=$id order by grau ASC";
                }
            }

            $lista = $objTurmas->listar($complemento);
            if ($lista != null) {

                foreach ($lista as $item) {
                    ?>


                    <tr>
                        <td class="text-left"> <?= $item->nome; ?></td>
                        <td class="text-center"> <?php
                    if ($item->grau == 1) {
                        echo "Ensino Fundamental";
                    } else {
                        echo "Ensino Médio";
                    }
                    ?></td>
                        <td class="text-center"> <?= $item->ano . "º"; ?></td>
                        <td class="text-right"><form method="post" action="turma-edit.php"><input type="hidden" name="idedit" value="<?= $item->id ?>"/><button type="submit" class="btn btn-primary">Editar</button></form> </td>
                    </tr>


        <?php
    }
} else {
    $mensagem = "<div class='alert alert-info'>Nenhum registro cadastrado até o momento.</div>";
}
?>   

        </tbody>
        <tfoot></tfoot>



    </table>

</div>
<?= $mensagem ?>
