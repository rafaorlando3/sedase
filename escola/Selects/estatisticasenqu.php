<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);
error_reporting(E_ALL);

echo '<script src="assets/js/Chart.bundle.js"></script>'; 
echo '<script src="assets/js/utils.js"></script>';

if (isset($_GET['enq'])) {

    $idenquete = $_GET['enq'];

    $objEnquetes = new Enquetes();
    $objEnquetes->id = $idenquete;
    $itemenquete = $objEnquetes->retornarunico();

     //começo dos dados da enquete, começando com o calculo da quantidade de usuários que responderam
            $quantEnqueusers = "SELECT COUNT(DISTINCT idusuario) AS quantidade FROM respenquete where idenquete=$itemenquete->id";
            $resultadousers = pg_query($quantEnqueusers);
            $retorno = pg_fetch_row($resultadousers);

            if ($retorno[0] == NULL) {
                $totalenqusers = 0;
            } else {

                $totalenqusers = $retorno[0];
            }
  
    echo " <h3 class='text-center'> Quantidade de Usuários que responderam essa enquete:<strong style='color:green;'>$totalenqusers</strong></h3> <hr/>";

    $objRespenquetes = new Respenquetes();
    $lista = $objRespenquetes->listar2("where idenquete=$itemenquete->id");
    if ($lista != null) {
        $objPerguntas = new Perguntas();
        $objOpcoes = new Opcoes();
        
        foreach ($lista as $item) {
            $objPerguntas->id = $item->idpergunta;
            $itempergunta = $objPerguntas->retornarunico();
                       
            //Contando quantas opções foram votadas para essa pergunta
            $quantopcao = "select count(idopcao) from respenquete where idpergunta=$itempergunta->id";
            $resultadoopc = pg_query($quantopcao);
            $retorno = pg_fetch_row($resultadoopc);

            if ($retorno[0] == NULL) {
                $totalopcao = 0;
            } else {
                $totalopcao = $retorno[0];
            }
            
           

            ?>

            <div class="row">
                <div class="text-center">

                    <div class="col-md-12 pull-center" >
                        <h4 class="text-center"><?= $itempergunta->titulo ?></h4>
                        <canvas id="grafico<?=$item->idpergunta ?>" style="width:70%" > </canvas>
     
                    </div> 
                    
                    <?php
            $listopcao=$objRespenquetes->listaropcao("where idpergunta=$itempergunta->id group by idopcao");
            
            //Contando quantas opções existem para essa pergunta
            $contaopc = "SELECT count(idopcao) FROM respenquete where idpergunta=$itempergunta->id";
            echo $contaopc;
            $resultopc = pg_query($contaopc);
            $retornoopc = pg_fetch_row($resultopc);

            if ($retornoopc[0] == NULL) {
                $totalopcaocont = 0;
            } else {
                $totalopcaocont = $retornoopc[0];
            }
            
            $geragraficoDatasetes[$totalopcaocont]=0;
            $geranomeslabels[$totalopcaocont]=0;
            $pos=0;
            foreach ($listopcao as $itemresp) {
              $objOpcoes->id=$itemresp->idopcao;
              $itemopcao = $objOpcoes->retornarunico();
   
             $geragraficoDatasetes[$pos]= $itemresp->cont;
             $geranomeslabels[$pos]=$itemopcao->texto;
             
              
             $pos++;
            } 
            
            $resultregistros= $totalopcaocont;
                        ?>

                </div>
            </div>

            <hr/>
<script>
var <?="graficos".$item->idpergunta?>= {
                type: 'pie',
                data: {
                    datasets: [{
                            data: [
                               <?php 
                               for($i=0;$i<$resultregistros;$i++){
                                   echo $geragraficoDatasetes[$i].",";
                               }
                               ?>
                            ],
                            backgroundColor: [
                                window.chartColors.red,
                                window.chartColors.orange,
                                window.chartColors.yellow
                            ],
                            label: 'Dataset 1'
                        }],
                    labels: [
                        <?php 
                               for($i=0;$i<$resultregistros;$i++){
                                   echo "'$geranomeslabels[$i]',";
                               }
                               ?>
                    ]
                },
                options: {
                    responsive: true
                }
            };
            
                var <?="nome".$item->idpergunta?> = document.getElementById("grafico<?=$item->idpergunta?>").getContext("2d");
                window.myPie = new Chart(<?="nome".$item->idpergunta?>, <?="graficos".$item->idpergunta?>);
           

        </script>


            <?php
        }
    }
}
