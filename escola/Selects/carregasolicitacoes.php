<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');

ini_set('display_errors', 0);
ini_set('display_startup_erros', 0);
error_reporting(E_ALL);

if (isset($_SESSION["nome"], $_SESSION["iduser"], $_SESSION["tipo"])) {
    $id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
    $nome = isset($_SESSION['nome']) ? $_SESSION['nome'] : '';
    $tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';
} else {
    echo "<script>window.location.href ='index.php';</script>";
}
?>

<div class="table-responsive">
    <table class="table table-striped" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="text-left">Nome</th>
                <th class="text-center">Nascimento</th>
                <th class="text-center">Este usuário é</th>
                <th class="text-center">Cidade</th>
                <th class="text-center">Escola</th>
                <th class="text-right">Aprovar</th>
                <th class="text-right">Recusar</th>
            </tr>
        </thead>
        <tbody>

            <?php
            $mensagem = "";
            
            if($_GET['pesquisa']){
                                $pesquisa = $_GET['pesquisa'];
                                $comp = "where nome LIKE '%$pesquisa%' or email LIKE '%$pesquisa%' and ativo=FALSE order by nome ASC";
                                
                            }
                        else{
                            $pesquisa="";
                            $comp = "where ativo=FALSE order by nome ASC";
                        }
            

            $objUsuarios = new Usuarios();
            $lista = $objUsuarios->listar($comp);
            if ($lista != null) {

                foreach ($lista as $item) {
                    ?>


                    <tr>
                        <td class="text-left"><?= $item->nome; ?></td>
                        <td class="text-center"><?= date("d/m/Y", strtotime($item->data)) ?></td>
                        <td class="text-center">
                            <?php
                            if ($item->tipo == 2) {
                                echo "<label class='label label-success'>Professor</label>";
                            } else {
                                echo "<label class='label label-info'>Aluno</label>";
                            }
                            ?>
                        </td>
                        <td class="text-center"><?= $item->cidade; ?></td>
                        <td class="text-center">Escola</td>
                        <td class="text-right"> <form id='solicitacao<?= $item->id ?>'> <input type="hidden" name="id" value="<?= $item->id; ?>"/> <input type="hidden" name="aprovado" value=""/> <button id="carregar" type="submit"  class="btn btn-xs btn-success"  >Aprovar</button></form>  </td>
                        <td class="text-right"><form id='solicitacao2<?= $item->id ?>'> <input type="hidden" name="id" value="<?= $item->id; ?>"/> <input type="hidden" name="recusado" value=""/><button id="carregar2" type="submit"  class="btn btn-xs btn-danger"  >Recusar</button></form></td>
                    </tr>

                    
                    <script type="text/javascript">

    $(document).ready(function () {
        $('#solicitacao<?= $item->id ?>').submit(function () {
            var dados = $(this).serialize();
            $('#carregar').attr('disabled', true);
            $("#carregar").html('Carregando...');
            $.ajax({
                type: "POST",
                url: "Inserts/respostasolicitacao.php",
                data: dados,
                success: function (data)
                {
                    atualizausuarios();
                    $('#resposta').html(data);
                    $('#carregar').attr('disabled', false);
                    $("#carregar").html('Aprovar');
                }
            });

            return false;

        });


        $('#solicitacao2<?= $item->id ?>').submit(function () {
            var dados = $(this).serialize();
            $('#carregar2').attr('disabled', true);
            $("#carregar2").html('Carregando...');
            $.ajax({
                type: "POST",
                url: "Inserts/respostasolicitacao.php",
                data: dados,
                success: function (data)
                {
                    atualizausuarios();
                    $('#resposta').html(data);
                    $('#carregar2').attr('disabled', false);
                    $("#carregar2").html('Recusar');
                }
            });

            return false;

        });


    });
</script>


                    
                    
                    <?php
                }
            } else {
                $mensagem = "<div class='alert alert-info'>Nenhum pedido de cadastro até o momento.</div>";
            }
            ?>   



        </tbody>
    </table>
</div>
<?= $mensagem ?>






