<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');

if (isset($_SESSION["nome"],$_SESSION["iduser"],$_SESSION["tipo"])) {   
$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
$nome = isset($_SESSION['nome']) ? $_SESSION['nome'] : '';
$tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';

 $objUsuarios = new Usuarios();
 $objUsuarios->id = $id;
 $itemuser = $objUsuarios->retornarunico();

    
} else {
echo "<script>window.location.href ='index.php';</script>";
}

?>


<!-- Modal -->
                <div class="modal fade" id="modalfrafico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Estatísticas</h4>
                            </div>
                            <div class="modal-body">

                                
                                <div id="graficos"></div> 

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

                            </div>



                        </div>
                    </div>
                </div>
                <!--             Fim modal...-->
                
                
       


<div class="table-responsive">
    <table class="table table-striped" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="text-left">
                                <b>Título</b>
                            </th>

                            <th class="text-center">
                                <b>Data</b>
                            </th>
                            
                             <th class="text-center">
                                <b>Autor</b>
                            </th>

                            <th class="text-center">
                                <b>Enquete para</b>
                            </th>
                            
                            
                            <th class="text-center">
                                <b>Status</b>
                            </th>

                            <th class="text-right">
                                <b>Ações</b>
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                        if($tipo==1){
                            if ($_GET['pesquisa']) {
                    $pesquisa = $_GET['pesquisa'];
                    $comp = "where titulo LIKE '%$pesquisa%' order by titulo ASC" ;
                } else {
                    $pesquisa = "";
                    $comp = "";
                }
                        }
                        else{
                             if ($_GET['pesquisa']) {
                    $pesquisa = $_GET['pesquisa'];
                    $comp = "where titulo LIKE '%$pesquisa%' and idusuario=$id order by titulo ASC";
                } else {
                    $pesquisa = "";
                    $comp = "where idusuario=$id order by titulo ASC";
                }
                        }
                        
                        $objEnquetes = new Enquetes();
                        $lista = $objEnquetes->listar($comp);
                        if ($lista != null) {
                            $mensagem = "";
                            $objUsuarios= new Usuarios();
                            foreach ($lista as $item) {
                                $objUsuarios->id = $item->idusuario;
                                $itemuser=$objUsuarios->retornarunico();
                                
                                if($item->idturma==null){
                                    
                                   $turma="<label class='label label-info'>Público</label>";
                                }
                                else{
                                    $objTurma= new Turma();
                                    $objTurma->id=$item->idturma;
                                    $itemturma=$objTurma->retornarunico();
                                    $turma="<label class='label label-info'>$itemturma->nome</label>";
                                }
                                
                                ?>


                                <tr>
                                    <td class="text-left"> <?= $item->titulo; ?></td>
                                    <td class="text-center"> <?= date("d/m/Y", strtotime($item->data)); ?></td>
                                    <td class="text-center"><label class='label label-success'><?=$itemuser->nome?> </label></td>
                                    <td class="text-center"> <?=$turma?></td>
                                    <td class="text-center"> <?php if($item->status==0){echo "<span class='label label-danger'>Offline</span>";} else{ echo "<span class='label label-success'>Online</span>";} ?></td>
                                    <td class="text-right"><form method="post" action="enquete-exibe.php"><input type="hidden" name="idedit" value="<?= $item->id ?>"/><button type="submit" class="btn btn-primary">Explorar</button> <button type="button" id="grafico<?= $item->id ?>" data-toggle="modal" data-target="#modalfrafico" class="btn btn-warning">Exibir Gráficos</button></form> </td>
                                </tr>
                                
                                
                                <script type="text/javascript">
                jq('#grafico<?= $item->id ?>').click(function () {
                    
                    mostrar();
                    function mostrar() {
                        jq.get('Selects/estatisticasenqu.php?enq=' + <?=$item->id?>, function (resultado) {
                            jq('#graficos').html(resultado);

                        });
                    }

                });
        </script>
                                


                                <?php
                            }
                        } else {
                            $mensagem = "<div class='alert alert-info'>Nenhum registro cadastrado até o momento.</div>";
                        }
                        ?>   

                    </tbody>
                    <tfoot></tfoot>



                </table>
    
</div>
                <?= $mensagem ?>
