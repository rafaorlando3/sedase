<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');

ini_set('display_errors', 0);
ini_set('display_startup_erros', 0);
error_reporting(E_ALL);


if (isset($_SESSION["nome"],$_SESSION["iduser"],$_SESSION["tipo"])) {   
$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
$nome = isset($_SESSION['nome']) ? $_SESSION['nome'] : '';
$tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';
    
} else {
echo "<script>window.location.href ='index.php';</script>";
}



?>


<div class="table-responsive">
             
    <table class="table table-striped" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="text-left">
                                <b>Nome</b>
                            </th>

                            <th class="text-center">
                                <b>Tipo</b>
                            </th>

                            <th class="text-center">
                                <b>Email/Matrícula</b>
                            </th>

                            <th class="text-right">
                                <b>Ação</b>
                            </th>

                        </tr>
                    </thead>
                    
                     <tfoot>
            <tr>
                            <th class="text-left">
                                <b>Nome</b>
                            </th>

                            <th class="text-center">
                                <b>Tipo</b>
                            </th>

                            <th class="text-center">
                                <b>Email/Matrícula</b>
                            </th>

                            <th class="text-right">
                                <b>Ação</b>
                            </th>

                        </tr>
        </tfoot>
                    
                    <tbody>
                        <?php
                        $mensagem = "";
                        if ($tipo == 1) {
                            if($_GET['pesquisa']){
                                $pesquisa = $_GET['pesquisa'];
                                $comp = "where ativo=TRUE and tipo<>1 and nome LIKE '%$pesquisa%' or email LIKE '%$pesquisa%'";
                                
                            }
                        else{
                            $pesquisa="";
                            $comp = "where ativo=TRUE and tipo<>1";
                        }
                            
                        } elseif($tipo == 2){
                            if($_GET['pesquisa']){
                                $pesquisa = $_GET['pesquisa'];
                                $comp = "where ativo=TRUE and tipo=0 LIKE %'$pesquisa'% and nome LIKE '%$pesquisa%' or email LIKE '%$pesquisa%' ";    
                            }
                        else{
                            $pesquisa="";
                            $comp = "where ativo=TRUE and tipo=0";
                        }
                            
                            
                        }
                    
                        $objUsuarios = new Usuarios();
                        $lista = $objUsuarios->listar($comp);
                        if ($lista != null) {

                            foreach ($lista as $item) {
                                ?>


                                <tr>
                                    <td class="text-left"> <?= $item->nome; ?></td>
                                    <td class="text-center"> <?php
                            if ($item->tipo == 1) {
                                echo "<label class='label label-warning'>Administrador</label>";
                            } elseif ($item->tipo == 2) {
                                echo "<label class='label label-success'>Professor</label>";
                            } else {
                                echo "<label class='label label-info'>Aluno</label>";
                            }
                            ?>
                                    
                                    </td>
                                    <td class="text-center"> <?= $item->email; ?></td>
                                    <td class="text-right"><form method="post" action="usuario-edit.php"><input type="hidden" name="idedit" value="<?= $item->id ?>"/><button type="submit" class="btn btn-primary">Editar</button></form> </td>
                                </tr>


                                <?php
                            }
                        } else {
                            $mensagem = "<div class='alert alert-info'>Nenhum registro cadastrado até o momento.</div>";
                        }
                        ?>   

                    </tbody>
                    <tfoot></tfoot>



                </table>
    
    </div>
                <?= $mensagem ?>

