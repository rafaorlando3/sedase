<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');

$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
$nome = isset($_SESSION['nome']) ? $_SESSION['nome'] : '';
$tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';
$idturma = isset($_GET['idturma']) ? $_GET['idturma'] : '';

if ($_GET['idturma'] != NULL) {
    ?>


    <div class="table-responsive">
        <table class="table table-striped" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th class="text-left">
                        <b>Aluno</b>
                    </th>

                    <th class="text-center">
                        <b>Email/Matrícula</b>
                    </th>
                    <th class="text-center">
                        <b>Turma</b>
                    </th>

                    <th class="text-right">
                        <b>Ação</b>
                    </th>

                </tr>
            </thead>
            <tbody>
                <?php
                $objPertence = new Pertence();
                $lista = $objPertence->listar("where idturma=$idturma");
                if ($lista != null) {
                    $objTurma = new Turma();
                    
                    foreach ($lista as $item) {
                        $objUsuarios = new Usuarios();
                        $objUsuarios->id = $item->idusuario;
                        $itemusuario = $objUsuarios->retornarunico();
                        $objTurma->id = $item->idturma;
                        $itemturma = $objTurma->retornarunico();
                        $botaovinc = '<form id="desvincula' . $item->id . '" > <input type="hidden" name="idpertence" value="' . $item->id . '"/> <button type="submit" id="botdesv" class="btn btn-danger">Desvincular</button></form>';
                        $label = '<label class="label label-success">'.$itemturma->nome.'</label>';
                        
                        ?>
                        <tr>
                            <td class="text-left"> <?= $itemusuario->nome; ?></td>
                            <td class="text-center"> <?= $itemusuario->email; ?></td>
                            <td class="text-center"> <?= $label; ?></td>
                            <td class="text-right"><?= $botaovinc ?></td>
                        </tr>

                    <script type="text/javascript">

                        jq(document).ready(function () {
                            jq('#desvincula<?= $item->id ?>').submit(function () {
                                var dados = jq(this).serialize();
                                jq('#botdesv').attr('disabled', true);
                                jq("#botdesv").html('Carregando...');
                                jq.ajax({
                                    type: "POST",
                                    url: "Delets/pertence-del.php",
                                    data: dados,
                                    success: function (data)
                                    {
                                        jq('#respostavinculacao').html(data);
                                        mostrar();
                                        function mostrar() {
                                            jq.get('Selects/vinculalunos.php?idturma=' + <?= $idturma ?>, function (resultado) {
                                                jq('#respostavincula').html(resultado);

                                            });
                                            
                                            jq.get('Selects/desvincula.php?idturma=' + <?= $idturma ?>, function (resultado) {
                                                    jq('#respostavincula2').html(resultado);

                                                });
                                        }
                                    }
                                });

                                return false;

                            });



                        });
                    </script>
            <?php
        }
    } else {
        $mensagem = "<div class='alert alert-info'>Nenhum aluno para desvincular até o momento.</div>";
    }
    ?>   

            </tbody>
            <tfoot></tfoot>



        </table>

    </div>
    <?= $mensagem ?>



    <?php
} else {
    echo "<div class='alert alert-info'>Selecione uma turma.</div>";
}