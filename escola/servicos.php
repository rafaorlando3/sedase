<?php
$menu=5;
include_once 'cabecalho.php';

?>
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Prestação de Serviços</h4>

            </div>

        </div>
        <div class="row">
            <div class="col-md-12">

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                    Cadastrar novo Serviço
                </button>

                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Cadastro de Serviço</h4>
                            </div>
                            <div class="modal-body">

                                <form id="cadastroS" >
                                    <div class="form-group">
                                        <label for="titulo">Título:</label>
                                        <input type="text" name="titulo" class="form-control" id="titulo" placeholder="Digite o Título..." required="" />
                                    </div>
                                    <div class="form-group">
                                        <label for="conteudo">Conteúdo:</label> <br/>
                                        <textarea name="conteudo" id="servicos" required="" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="categoria">Categoria:</label>
                                        <select class="form-control" name="categoria">
                                            <option value="0">Informações</option>
                                            <option value="1">Denúncias</option>
                                            <option value="2">Dicas</option>
                                        </select>    
                                    </div>

                                    <ul class="nav nav-tabs" role="tablist" id="myTab">
                                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Vazio</a></li>
                                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Foto</a></li>
                                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Youtube</a></li>
                                        <li role="presentation"><a href="#outro" aria-controls="outro" role="tab" data-toggle="tab">Anexo</a></li>

                                    </ul>

                                    <div class="tab-content">
                                        <link href="assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
                                        <script src="assets/js/fileinput.js" type="text/javascript"></script>
                                        <script src="assets/js/fileinput.min.js" type="text/javascript"></script>
                                        <div role="tabpanel" class="tab-pane active" id="home">Não cadastrar nenhum tipo de arquivo. </div>
                                        <div role="tabpanel" class="tab-pane" id="profile">
                                            <div class="form-group">
                                                <label>Selecione a Foto:</label>
                                                <input name="imagem" id="file-3" type="file" />
                                            </div>

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="messages"><input type="text" class="form-control" name="video" placeholder="Insira aqui o ID do vídeo do Youtube"/></div>

                                        <div role="tabpanel" class="tab-pane" id="outro">
                                            <div class="form-group">
                                                <label>Selecione o anexo:</label>
                                                <input name="anexo" id="file-4" type="file" />
                                            </div>
                                        </div>

                                        <script>

                                            $("#file-3").fileinput({
                                                allowedFileExtensions: ['jpg', 'png', 'gif'],
                                                showUpload: false,
                                                showCaption: false,
                                                browseClass: "btn btn-primary btn-lg",
                                                fileType: "any",
                                                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
                                            });

                                            $("#file-4").fileinput({
                                                allowedFileExtensions: ['pdf', 'doc', 'docx','odt','txt'],
                                                showUpload: false,
                                                showCaption: false,
                                                browseClass: "btn btn-primary btn-lg",
                                                fileType: "any",
                                                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
                                            });
                                        </script>
                                    </div>

                                    <script>
                                        $(function () {
                                            $('#myTab a:last').tab('show')
                                        })
                                    </script>
                                    <br/>


                                    <input type="hidden" name="idusuario" value="<?= $id ?>"/>


                                    <button type="submit" class="btn btn-primary" id="carregar"  >Cadastrar</button>
                                </form> <br/>
                                <div id="resposta"></div> 

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

                            </div>

                            <script type="text/javascript">
                                var jq = $.noConflict();

                                function atualizaservicos() {
                                    jq.get('Selects/carregaservicos.php', function (resultado) {
                                        jq('#listaservicos').html(resultado);

                                    });
                                }
                         
                                jq(document).ready(function () {
                                      atualizaservicos();
                                    jq('#cadastroS').submit(function () {
                                        
                                        jq('#carregar').attr('disabled', true);
                                        jq("#carregar").html('Carregando...');
                                        
                                        for (instance in CKEDITOR.instances) {
                                            CKEDITOR.instances[instance].updateElement();
                                        }
                                        
                                        
                                        jq.ajax({
                                           type: "POST",
                                            url: "Inserts/cadastroS.php",
                                            data: new FormData(this),
                                            processData: false,
                                            contentType: false,
                                            success: function (data)
                                            {
                                                atualizaservicos();
                                                jq('#resposta').html(data);
                                                jq('#carregar').attr('disabled', false);
                                                jq("#carregar").html('Cadastrar');
                                            }
                                        });

                                        return false;

                                    });
                                    
                                     jq("#busca").keypress(function () {
                                    var pesquisa = jq('#busca').val();

                                    jq.get('Selects/carregaservicos.php?pesquisa=' + pesquisa, function (resultado) {
                                        jq('#listaservicos').html(resultado);

                                    });

                                });

                            CKEDITOR.replace('servicos');
                                });
                                
                            </script>

                        </div>
                    </div>
                </div>
                <!--             Fim modal...-->
                <hr/>
                
                 <div class="text-right pull-right"><label>Buscar aqui: </label> <input  type="text" id="busca" value=""  /> </div>

                <h2>Serviços<small> Listando</small></h2>
                <div id="listaservicos">


                </div>
            </div>

        </div>
    </div>
</div>

<?php
include_once 'rodape.php';
