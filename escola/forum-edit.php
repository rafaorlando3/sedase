<?php
$menu = 7;
include_once 'cabecalho.php';

if ($_POST['idedit']) {

    $idedit = $_POST['idedit'];
    $objForum = new Forum();
    $objForum->id = $idedit;
    $item = $objForum->retornarunico();
    $_SESSION["idturma"] = $item->idturma;
    $_SESSION["tipooutro"] = $item->tipo;
} else {
    header("location:forum.php");
}
?>
<div class="content-wrapper">
    <div class="container">
        
        <script language="Javascript">
var jq = $.noConflict();

jq(document).ready(function () {
            jq('#excluir').click(function () {
            var resposta = confirm("Você realmente deseja excluir esse Tópico?");
            if (resposta == true) {
                jq('#excluir').attr('disabled', true);
                var dados = jq('#excluirF').serialize();


                jq.ajax({
                    type: "POST",
                    url: "Delets/forum-del.php",
                    data: dados,
                    success: function (data)
                    {
                        jq("#excluir").html('Excluindo...');
                        jq("#excluir").html('Excluído');
                        jq( "#excluiresp").empty();
                        jq('#excluiresp').html(data);
                    }
                });
            } else {
                jq('#excluir').attr('disabled', false);
                jq("#excluir").html('Excluir');
            }

            return false;


        });
        
});



        </script>        
        
        <div id="excluiresp">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Tópico: <?= $item->titulo ?></h4>  
            </div>

        </div>

        <button id="excluir" class="btn btn-large btn-danger" >Excluir</button> 

        <form id="excluirF"><input type="hidden" name="idedit" value="<?= $idedit; ?>"></form>

       
        <hr/>
        <div class="row">
            <div class="col-md-12">     
                <form id="editaF" >
                    <div class="form-group">
                        <label for="titulo">Título:</label>
                        <input type="text" name="titulo" class="form-control" id="titulo" placeholder="Digite o Título..." value="<?= $item->titulo; ?>" required="" />
                    </div>
                    <div class="form-group">
                        <label for="conteudo">Conteúdo:</label> <br/>
                        <textarea name="descricao" id="conteudo" required="" class="form-control"><?= $item->texto; ?></textarea>
                    </div>
                    <div class="form-group" id="carregaturma">

                    </div>

                    <ul class="nav nav-tabs" role="tablist" id="myTab">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Vazio</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Foto</a></li>

                    </ul>

                    <div class="tab-content">
                        <link href="assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
                        <script src="assets/js/fileinput.js" type="text/javascript"></script>
                        <script src="assets/js/fileinput.min.js" type="text/javascript"></script>
                        <div role="tabpanel" class="tab-pane active" id="home">Não atualizar nenhum arquivo. </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="form-group">
                                <label>Selecione a Foto:</label>
                                <input name="imagem" id="file-3" type="file" />
                                <p></p>
                                <?php
                                if ($item->imagem != "" || $item->imagem != NULL) {
                                    ?>

                                    Imagem Atual: <img src="../uploads/imagens/<?= $item->imagem ?>" width="20%"alt="..." class="img-thumbnail">

                                    <?php
                                }
                                ?>


                            </div>

                        </div>


                        <script>

            $("#file-3").fileinput({
                allowedFileExtensions: ['jpg', 'png', 'gif'],
                showUpload: false,
                showCaption: false,
                browseClass: "btn btn-primary btn-lg",
                fileType: "any",
                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
            });
                        </script>
                    </div>

                    <script>
                        $(function () {
                            $('#myTab a:last').tab('show')
                        })
                    </script>
                    <br/>
                    <div class="form-group">
                        <label for="conteudo">Tópico Concluído?: </label>
                   Sim <input type="radio" name="resolvido" value="t" <?php if($item->resolvido=="t"){echo "checked";}?> /> - Não <input type="radio" name="resolvido" value="f" <?php if($item->resolvido=="f"){echo "checked";}?> />

                    </div>

                    <input type="hidden" name="idusuario" value="<?= $item->idusuario; ?>"/>
                    <input type="hidden" name="id" value="<?= $item->id; ?>"/>


                    <button type="submit" class="btn btn-primary" id="carregar"  >Atualizar</button>
                </form> <br/>

                <div id="resposta"></div> 



                <script type="text/javascript">
                    
                    jq(document).ready(function () {
                        jq('#editaF').submit(function () {
                            jq('#carregar').attr('disabled', true);
                            jq("#carregar").html('Carregando...');
                            jq.ajax({
                                type: "POST",
                                url: "Updates/editaF.php",
                                data: new FormData(this),
                                processData: false,
                                contentType: false,
                                success: function (data)
                                {
                                    jq('#resposta').html(data);
                                    jq('#carregar').attr('disabled', false);
                                    jq("#carregar").html('Atualizar');
                                }
                            });

                            return false;

                        });


                        atualiza();
                        function atualiza() {
                            jq.get('Selects/carreturmaupdate.php', function (resultado) {
                                jq('#carregaturma').html(resultado);

                            });
                        }


                    });

                </script>


            </div>

        </div>
        </div>
        <a href="forum.php"><button type="button" class="btn btn-default">Voltar</button></a>
    </div>
</div>

<?php
include_once 'rodape.php';
