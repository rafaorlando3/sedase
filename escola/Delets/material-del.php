<?php
include_once '../../class/Carrega.class.php';

$objMaterial = new Materiais();
if (isset($_POST["idedit"])) {

    $cod = $_POST["idedit"];

    $objMaterial->id = $cod;
    $item = $objMaterial->retornarunico();

    $excluiImagem = "../../uploads/imagens/" . $item->imagem . "";
    if (file_exists($excluiImagem)) {
        unlink($excluiImagem);
    }

    $excluiAnexo = "../../uploads/anexos/" . $item->anexo . "";
    if (file_exists($excluiAnexo)) {
        unlink($excluiAnexo);
    }

    $objMaterial->excluir();

    echo "<div class='alert alert-success'> Material excluído com Sucesso! <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
} else {

    echo "<div class='alert alert-danger'>Erro ao excluir. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
}
