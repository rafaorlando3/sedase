<?php

include_once '../../class/Carrega.class.php';

$objPerguntas = new Perguntas();
if (isset($_POST["idpergunta"])) {

    $cod = $_POST["idpergunta"];

    $objPerguntas->id = $cod;
    $objPerguntas->excluir();

   echo "<div class='alert alert-danger'> Pergunta excluída com Sucesso!  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";

} else {

    echo "<div class='alert alert-danger'>Erro ao tentar excluir essa Pergunta.  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
}
