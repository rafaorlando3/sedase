<?php
include_once '../../class/Carrega.class.php';

$objServicos = new Pservicos();
if (isset($_POST["idedit"])) {

    $cod = $_POST["idedit"];

    $objServicos->id = $cod;
    $item = $objServicos->retornarunico();

    $excluiImagem = "../../uploads/imagens/" . $item->imagem . "";
    if (file_exists($excluiImagem)) {
        unlink($excluiImagem);
    }

    $excluiAnexo = "../../uploads/anexos/" . $item->anexo . "";
    if (file_exists($excluiAnexo)) {
        unlink($excluiAnexo);
    }

    $objServicos->excluir();

    echo "<div class='alert alert-success'> Serviço excluído com Sucesso! <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
} else {

    echo "<div class='alert alert-danger'>Erro ao excluir. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
}
