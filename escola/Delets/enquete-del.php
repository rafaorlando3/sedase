<?php session_start();?>
<?php
include_once '../../class/Carrega.class.php';

$objEnquetes = new Enquetes();
if (isset($_POST["idedit"])) {

    $cod = $_POST["idedit"];

    $objEnquetes->id = $cod;
    $objEnquetes->excluir();

    echo "<a href='enquetes.php'><button type='button' class='btn btn-default'>Voltar</button></a> <hr/> <div class='alert alert-success'> Enquete excluída com Sucesso!  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
    //header("Location:../enquetes.php");
} else {

    echo "<a href='enquetes.php'><button type='button' class='btn btn-default'>Voltar</button></a> <hr/> <div class='alert alert-danger'>Erro ao excluir.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
    //header("Location:../enquetes.php");
}