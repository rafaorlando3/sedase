<?php session_start();?>
<?php
include_once '../../class/Carrega.class.php';

$objUsuarios = new Usuarios();
if (isset($_POST["idedit"])) {

    $cod = $_POST["idedit"];

    $objUsuarios->id = $cod;
    $objUsuarios->excluir();

   echo " <div class='alert alert-success'> Usuário excluído com Sucesso!  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
    
} else {

    echo "<div class='alert alert-danger'>Erro ao excluir.  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";

}
