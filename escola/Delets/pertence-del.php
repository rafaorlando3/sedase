<?php

include_once '../../class/Carrega.class.php';

$objPertence = new Pertence();
if (isset($_POST["idpertence"])) {

    $cod = $_POST["idpertence"];

    $objPertence->id = $cod;
    $objPertence->excluir();

   echo "<div class='alert alert-danger'> Vinculação desfeita com sucesso!  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";

} else {

    echo "<div class='alert alert-danger'>Erro ao tentar desfazer vinculação.  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
}
