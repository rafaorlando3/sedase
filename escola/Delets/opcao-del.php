<?php

include_once '../../class/Carrega.class.php';

$objOpcao = new Opcoes();
if (isset($_POST["idopcao"])) {

    $cod = $_POST["idopcao"];

    $objOpcao->id = $cod;
    $objOpcao->excluir();

   echo "<div class='alert alert-danger'> Opção excluída com Sucesso!  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";

} else {

    echo "<div class='alert alert-danger'>Erro ao tentar excluir essa Opção.  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
}
