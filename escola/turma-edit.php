<?php
$menu = 2;
include_once 'cabecalho.php';


if ($_POST['idedit']) {

    $idedit = $_POST['idedit'];
    $objTurma = new Turma();
    $objTurma->id = $idedit;
    $item = $objTurma->retornarunico();
} else {
    header("location:turmas.php");
}
?>
<div class="content-wrapper">
    <div class="container">
        
        
        <script language="Javascript">
var jq = $.noConflict();

jq(document).ready(function () {
            jq('#excluir').click(function () {
            var resposta = confirm("Você realmente deseja excluir essa Turma?");
            if (resposta == true) {
                jq('#excluir').attr('disabled', true);
                var dados = jq('#excluirT').serialize();


                jq.ajax({
                    type: "POST",
                    url: "Delets/turma-del.php",
                    data: dados,
                    success: function (data)
                    {
                        jq("#excluir").html('Excluindo...');
                        jq("#excluir").html('Excluído');
                        jq( "#excluiresp").empty();
                        jq('#excluiresp').html(data);

                       
                    }
                });
            } else {
                jq('#excluir').attr('disabled', false);
                jq("#excluir").html('Excluir');
            }

            return false;


        });
        
});



        </script>        
        
        <div id="excluiresp">

        
        
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Turma <?= $item->nome . "- $escola" ?></h4>  
            </div>

        </div>

              
        <button id="excluir" class="btn btn-large btn-danger" >Excluir</button> 

        <form id="excluirT"><input type="hidden" name="idedit" value="<?= $idedit; ?>"></form>

       
   
        <div class="row">
            <div class="col-md-12">
                <form id="editaT" >
                    <div class="form-group">
                        <label for="nome">Nome:</label>
                        <input type="text" name="nome" class="form-control" id="nome" value="<?= $item->nome ?>" placeholder="Digite seu Nome..." required="" />
                    </div>

                    <div class="form-group">
                        <label for="grau">Grau:</label>
                        <select name="grau" id="grau" required=""> 
                            <option value="1"  <?php
                            if ($item->grau == 1) {
                                echo "selected='selected'";
                            }
                            ?>>Ensino Fundamental</option> 
                            <option value="2" <?php
                            if ($item->grau == 2) {
                                echo "selected='selected'";
                            }
                            ?> >Ensino Médio</option>

                        </select>

                    </div>

                    <div id="div1" class="form-group">
                        <label for="ano">Ano:</label>
                        <select name="ano" required=""> 
                            <option value="1"  <?php
                            if ($item->ano == 1) {
                                echo "selected='selected'";
                            }
                            ?>>1º</option> 
                            <option value="2" <?php
                            if ($item->ano == 2) {
                                echo "selected='selected'";
                            }
                            ?> >2º</option>
                            <option value="3"  <?php
                            if ($item->ano == 3) {
                                echo "selected='selected'";
                            }
                            ?>>3º</option> 
                            <option value="3" <?php
                            if ($item->ano == 3) {
                                echo "selected='selected'";
                            }
                            ?> >3º</option>
                            <option value="4"  <?php
                            if ($item->ano == 4) {
                                echo "selected='selected'";
                            }
                            ?>>4º</option> 
                            <option value="5" <?php
                            if ($item->ano == 5) {
                                echo "selected='selected'";
                            }
                            ?> >5º</option>
                            <option value="6"  <?php
                            if ($item->ano == 6) {
                                echo "selected='selected'";
                            }
                            ?>>6º</option> 
                            <option value="7" <?php
                            if ($item->ano == 7) {
                                echo "selected='selected'";
                            }
                            ?> >7º</option>
                            <option value="8"  <?php
                            if ($item->ano == 8) {
                                echo "selected='selected'";
                            }
                            ?>>8º</option> 
                            <option value="9" <?php
                            if ($item->ano == 9) {
                                echo "selected='selected'";
                            }
                            ?> >9º</option>
                        </select>
                    </div>
                    <input type="hidden" name="idturma" value="<?= $idedit ?>"/>
                    <input type="hidden" name="idusuario" value="<?= $id ?>"/>
                    <input type="hidden" name="idescola" value="<?= $idescola ?>"/>
                    <button type="submit" class="btn btn-primary" id="carregar"  >Atualizar</button>
                </form>  <br/>
                <div id="resposta"></div> 



                <script type="text/javascript">

                    jq(document).ready(function () {
                        jq('#editaT').submit(function () {
                            var dados = jq(this).serialize();
                            jq('#carregar').attr('disabled', true);
                            jq("#carregar").html('Carregando...');
                            jq.ajax({
                                type: "POST",
                                url: "Updates/editaT.php",
                                data: dados,
                                success: function (data)
                                {
                                    jq('#resposta').html(data);
                                    jq('#carregar').attr('disabled', false);
                                    jq("#carregar").html('Atualizar');
                                }
                            });

                            return false;

                        });


                        jq("#grau").change(function () {
                            var grau = this.value;
                            if (grau === '1') {
                                var conteudo = '<label for="ano">Ano:</label> <select name="ano" required=""><option value="1">1º</option><option value="2">2º</option><option value="3">3º</option><option value="4">4º</option><option value="5">5º</option><option value="6">6º</option><option value="7">7º</option><option value="8">8º</option><option value="9">9º</option></select>';
                                jq('#div1').html(conteudo);
                            } else if (grau === '2') {
                                var conteudo = '<label for="ano">Ano:</label> <select name="ano" required=""><option value="1">1º</option><option value="2">2º</option><option value="3">3º</option></select>';
                                jq('#div1').html(conteudo);
                            }

                        });

                    });
                </script>


            </div>

        </div>
        </div>
        
        <a href="turmas.php"><button type="button" class="btn btn-default">Voltar</button></a>
    </div>
</div>

<?php
include_once 'rodape.php';
