 
<script type="text/javascript" src="js/validacao.js"></script>
<footer id="footer">
    <div class="container">
        <section>
            <article class="col-1">
                <h3>Contato</h3>
                <ul>
                    <li class="address"><a href="#"><?=$endereco?></a></li>
                    <li class="mail"><a href="#"><?=$email?></a></li>
                    <li class="phone last"><a href="#"><?=$telefone?></a></li>
                </ul>
            </article>
            <article class="col-2">
                <h3>Fórum - Novos Tópicos</h3>
                <ul>

                    <?php
                    $objForum = new Forum();

                    $lista = $objForum->listar("where idturma is NULL order by id DESC LIMIT 5");
                    if ($lista != null) {
                        $mensagem = "";

                        foreach ($lista as $item) {
                            ?>


                            <li><a href="forum/inicio.php"><?= $item->titulo; ?></a></li>
                            <?php
                        }
                    }
                        ?>
                </ul>
            </article>
            <article class="col-3">
                <h3>Redes Sociais</h3>
                <ul>
                    <li class="facebook"><a href="#">Facebook</a></li>
                    <li class="google-plus"><a href="#">Google+</a></li>
                    <li class="twitter"><a href="#">Twitter</a></li>
                    <li class="pinterest"><a href="#">Pinterest</a></li>
                </ul>
            </article>

        </section>
        <p class="copy">Copyright 2017 Sistema SEDASE. Todos os Direitos Reservados.</p>
    </div>
    <!-- / container -->
</footer>
<!-- / footer -->

<div id="fancy"> 
    <?php 
    
    if($nome!=NULL){ 
    echo "<h2>Olá, ".$nome2[0].". O que você deseja fazer?</h2> <br/>";
    
    ?>
    
    <div class="btn-holder">
    <a <?php if($tipo==0){echo "href='aluno/inicio.php'";} elseif($tipo==1 || $tipo==2){ echo "href='escola/inicio.php'";} ?> ><button class="btn blue" id="carregalogin" type="button">Ir para meu SEDASE</button></a>
    <br/><br/>
    <a href='forum/inicio.php'><button class="btn blue" id="carregalogin" type="button">Ir para o Fórum Público</button></a>
    <br/><br/>
    <a href='acoes/logout.php'><button class="btn blue" id="carregalogin" type="button">Sair</button></a>
    
    </div>
    <?php
    
    }
    else{ 
        ?>
    
  
     <h2>Realizar Login</h2>   
    <form id="login">
        <div class="center-block">
            <fieldset class="name"><input name="email" placeholder="Digite e-mail ou matricula" type="text" value="" required=""></fieldset>
            <fieldset class="name"><input name="senha" placeholder="Digite sua senha..." type="password" value="" required=""></fieldset>

        </div>
        
        <div class="btn-holder">
           <div id="respostalogin"> <button class="btn blue" id="carregalogin" type="submit">Entrar</button> </div>
            
            <br/>
            <p><a href="#fancy3" class="get-contact" id="">Esqueci minha senha</a></p>
            <p><a href="#fancy2" class="get-contact">Cadastrar-me</a></p>
        </div>
    </form> 
   
   <?php
     }
?>
    
</div>


<div id="fancy2">

    <h2>Cadastro de Usuário</h2>
    <form id="cadastroU" >

        <fieldset class="name" title="Informe seu Nome:"><input type="text" name="nome"  id="nome" placeholder="Digite seu Nome..." required="" /></fieldset>
        <fieldset class="question" title="Informe sua data de nascmento:"><input type="text" name="data"  id="data" onkeydown="Mascara(this, Data);" onkeypress="Mascara(this, Data);" onkeyup="Mascara(this, Data);" placeholder="Digite seu nascimento no formato: 00/00/0000" required="" maxlength="10" /></fieldset>
        <fieldset class="question" title="Qual o seu Estado?">
            <select name="estado" id="estado"required=""> 
                <option value="AC">Acre</option> 
                <option value="AL">Alagoas</option> 
                <option value="AM">Amazonas</option> 
                <option value="AP">Amapá</option> 
                <option value="BA">Bahia</option> 
                <option value="CA">Ceará</option> 
                <option value="DF">Distrito Federal</option> 
                <option value="ES">Espírito Santo</option> 
                <option value="GO">Goiás</option> 
                <option value="MA">Maranhão</option> 
                <option value="MT">Mato Grosso</option> 
                <option value="MS">Mato Grosso do Sul</option> 
                <option value="MG">Minas Gerais</option> 
                <option value="PA">Pará</option> 
                <option value="PB">Paraíba</option> 
                <option value="PR">Paraná</option> 
                <option value="PE">Pernambuco</option> 
                <option value="PI">Piauí</option> 
                <option value="RJ">Rio de Janeiro</option> 
                <option value="RN">Rio Grande do Norte</option> 
                <option value="RO">Rondônia</option> 
                <option value="RS" selected="selected">Rio Grande do Sul</option> 
                <option value="RR">Roraima</option> 
                <option value="SC">Santa Catarina</option> 
                <option value="SE">Sergipe</option> 
                <option value="SP">São Paulo</option> 
                <option value="TO">Tocantins</option> 
            </select>

        </fieldset>

        <fieldset class="question" title="Digite sua Cidade">  
            <input type="text" name="cidade"  id="cidade" placeholder="Digite sua Cidade..." required="" />
        </fieldset>

        <fieldset class="name"  title="Selecione o seu Gênero">
            <select name="genero" id="genero" required=""> 
                <option value="Feminino">Feminino</option> 
                <option value="Masculino">Masculino</option>
                <option value="Trans Feminino">Trans Feminino</option>
                <option value="Trans Masculino">Trans Masculino</option>
                <option value="Não Binário">Não Binário</option>
                <option value="Outro">Outro</option>
            </select>

        </fieldset>


        <fieldset class="mail" title="Informe seu E-mail:">    
            <input type="email" name="email"  id="email" placeholder="Digite seu Email..." required="" />
        </fieldset>
        <fieldset class="question" title="Informe sua Senha">  
            <input type="password" id="senha" onkeydown="Mascara(this, validarSenha);" onkeypress="Mascara(this, validarSenha);" onkeyup="Mascara(this, validarSenha);" name="senha"  id="senha" placeholder="Digite sua senha..." required="" />
        </fieldset>
        <fieldset class="question" title="Redigite sua Senha">  
            <input type="password" onkeydown="Mascara(this, validarSenha);" onkeypress="Mascara(this, validarSenha);" onkeyup="Mascara(this, validarSenha);" name="senha2"  id="senha" placeholder="Confirme sua senha..." required=""/>
        </fieldset>



        <div  id="carregaturma">

        </div>

        <fieldset class="question" title="O que você é:">
            <select name="tipo" id="tipo" required="">

                <option value="0">Aluno</option>
                <option value="2">Professor</option>
            </select>
        </fieldset>
        <div class="btn-holder">
            <button class="btn blue" id="carregar" type="submit">Solicitar Cadastro</button>
            <br/><br/>
            <p><a href="#fancy" class="get-contact" id="">Realizar Login</a></p>
        </div>
    </form>

    <br/> 
</div>


<div id="fancy3">

    <h2>Redefinir Senha</h2>
    <form id="esquecisenha">
        <div class="center-block">
            <fieldset class="name">
                <input name="email" placeholder="Digite seu e-mail" type="email" value="" required="">
            </fieldset>
        </div>
        <div class="btn-holder">
            <div id="respostasenha"> 
                <button class="btn blue" id="carregaesqueci" type="submit">Enviar</button> 
            </div>
             <br/>
    <p><a href="#fancy" class="get-contact" id="">Realizar Login</a></p>
    <p><a href="#fancy2" class="get-contact">Cadastrar-me</a></p>
        </div>
    </form>
   
</div>



<script type="text/javascript">
    $(document).ready(function () {
        $('#cadastroU').submit(function () {
            var dados = $(this).serialize();
            $('#carregar').attr('readonly', true);
            $("#carregar").html('Carregando...');
            $.ajax({
                type: "POST",
                url: "acoes/cadastroUsite.php",
                data: dados,
                success: function (data)
                {
                    $('#resposta').html(data);
                    $('#carregar').attr('readonly', false);
                    $("#carregar").html('Cadastrar');
                }
            });

            return false;

        });
        
        
        $('#login').submit(function () {           
            $('#carregalogin').attr('readonly', true);
            $('#respostalogin').html("<div class='aviso'>Verificando...</div>");

            var dados = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "acoes/login.php",
                data: dados,
                success: function (data)
                { 
                  $('#respostalogin').html(data);
                }
            });

            return false;


        });
        
    
    $('#esquecisenha').submit(function () {
            
            $('#respostasenha').html("<div class='aviso'>Verificando...</div>");

            var dados = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "acoes/esquecisenha.php",
                data: dados,
                success: function (data)
                { 
                  $('#respostasenha').html(data);
                }
            });

            return false;


        });
    
    });

</script>


<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script>window.jQuery || document.write("<script src='js/jquery-1.11.1.min.js'>\x3C/script>")</script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>

</body>
</html>