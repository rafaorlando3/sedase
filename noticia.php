<?php
include_once 'cabecalho.php';
include_once 'class/Noticias.class.php';
include_once 'testemes.php';

if($_GET['data']){
    $objNoticias = new Noticias();
    $objNoticias->id=$_GET['data'];
    $item=$objNoticias->retornarunico();
    
    $objUsuarios = new Usuarios();
    $objUsuarios->id = $item->idusuario;
    $itemuser = $objUsuarios->retornarunico();
}
else{
    
    header("Location:noticias.php");
}

?>
	
		<div class="divider"></div>
	
	<div class="content">
		<div class="container">

			<div class="main-content">
				<h1>Notícia: <?=$item->titulo?></h1>
				<section class="posts-con">
                                     <?php
                   
                                ?>
                                    
					<article>
						<div class="current-date">
							<p><?php qualmes($mesagora);?></p>
							<p class="date"><?=$dianoticia?></p>
						</div>
						<div class="info">
                                                 
                                                        <p class="info-line"><span class="time">Tags: <a href="#"><?=$item->tags?></a> </span> Por <?=$itemuser->nome?></p>
							<br/>
                                                       <?php
                                                        
                                                         if($item->video!=NULL){
                                                        
                                                        ?>
                                                      <h3>Vídeo:</h3>
                                                        <div id="ytplayer"></div> 

<script>
  // Load the IFrame Player API code asynchronously.
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/player_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  // Replace the 'ytplayer' element with an <iframe> and
  // YouTube player after the API code downloads.
  var player;
  function onYouTubePlayerAPIReady() {
    player = new YT.Player('ytplayer', {
      height: "100%",
      width: "100%",
      videoId: '<?=$item->video?>'
    });
  }
</script>
                                                        
                                                        <?php
                                                         }
                                                        ?>
                                                        <p><?=$item->conteudo?></p>
                                                        
                                                        
                                                                 <?php
                                                        if($item->imagem!=NULL){
                                                        ?>
                                                        <img src="uploads/imagens/<?=$item->imagem?>" width="80%" />
                                                        <?php
                                                        }
                                                        ?>
						</div>
                                            
                                    
                                            
                                            
					</article>
                                    
                                    
                  
					
				</section>
			</div>
			
		
	
		</div>
		<!-- / container -->
	</div>

<?php 

include_once 'rodape.php';
