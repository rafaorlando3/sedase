<?php
include_once 'cabecalho.php';
include_once 'class/Noticias.class.php';
include_once 'testemes.php';

?>
	
		<div class="divider"></div>
	
	<div class="content">
		<div class="container">

			<div class="main-content">
				<h1>Sobre o SEDASE</h1>
				<section class="posts-con">
                                     <?php
                   
                                ?>
                                    
					<article>
						
						<div class="info">

             
                                                    <p>O <b>Sistema de Ensino da Sexualidade na Escola (SEDASE)</b> que tem como principal objetivo auxiliar professores e alunos de escolas públicas na difusão do assunto sobre sexualidade, bem como sua importância, os riscos que
podem acontecer com informações errôneas e seus impactos causados na comunidade.</p>
                                                    <br/>
                                                        <p>Para isso, o sistema/site <b>SEDASE</b> que conta com funcionalidades que ajudarão na metodologia a ser aplicada na temática da sexualidade. Os professores e alunos poderão utilizar enquetes para debates, participarão de fóruns de discussão sobre o tema, causando um grande impacto positivo para o esclarecimento de muitas dúvidas que assolam os adolescentes de comunidades carentes.</p>
                                                        
                                                        
                                                    
						</div>
                                            
                                    
                                            
                                            
					</article>
                                    
                                    
                  
					
				</section>
			</div>
			
		
	
		</div>
		<!-- / container -->
	</div>

<?php 

include_once 'rodape.php';
