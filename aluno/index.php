<?php
$msgerro = "";
$cadastro = "";
include_once '../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');

ini_set('display_errors', 0);
ini_set('display_startup_erros', 0);
error_reporting(E_ALL);

if(isset($_SESSION["tipo"])){
    $tipo=$_SESSION["tipo"];
    if($tipo==1){
    header("location:../escola/inicio.php");
    }
    
    }

$objEscola = new Escola();
$existe = $objEscola->listar();

if ($existe != null) {
    $cadastro = TRUE;
    
    foreach ($existe as $item) {
        $escola = $item->nome;
        $codescola = $item->codigo;
        $idescola = $item->id;
        $email = $item->email;
        $endereco = $item->endereco;
        $telefone = $item->telefone;
    }

    if (isset($_POST["email"], $_POST["senha"])) {


        $login = pg_escape_string($_POST["email"]);
        $senha = pg_escape_string($_POST["senha"]);
        $vsenha=base64_encode($senha);


        $objUsuarios = new Usuarios();
        $retorno = $objUsuarios->listar("where email ='$login' and senha='$vsenha' and tipo=0 and ativo=TRUE");

        if ($retorno != null) {

            foreach ($retorno as $item) {

                session_start();
                $_SESSION["iduser"] = $item->id;
                $_SESSION["nome"] = $item->nome;
                $_SESSION["tipo"] = $item->tipo;

                echo "<script language='javascript'> document.location.href='inicio.php'; </script>";
            }
        } else {
            $msgerro = "<div class='alert alert-danger'>Login ou Senha inválidos.</div>";
        }
    }
} else {
    $cadastro = FALSE;
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <![endif]-->
        <title>SEDASE - ADM</title>
        <!-- BOOTSTRAP CORE STYLE  -->
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FONT AWESOME ICONS  -->
        <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLE  -->
        <link href="assets/css/style.css" rel="stylesheet" />
        <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        
         <script src="assets/js/jquery-3.1.0.js"></script>
    </head>
    <body>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <strong>Email: </strong><?=$email?>
                        &nbsp;&nbsp;
                        <strong>Contato: </strong><?=$telefone?>
                    </div>

                </div>
            </div>
        </header>
        <!-- HEADER END-->
        <div class="navbar navbar-inverse set-radius-zero">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">

                        <img src="assets/img/logo.png" />
                    </a>

                </div>

                <div class="left-div">
                    <div class="user-settings-wrapper">
                        <ul class="nav">

                            <li class="dropdown">
                                <!--                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                                                <span class="glyphicon glyphicon-user" style="font-size: 25px;"></span>
                                                            </a>-->
                                <div class="dropdown-menu dropdown-settings">
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img src="assets/img/64-64.jpg" alt="" class="img-rounded" />
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">Rafael Orlando </h4>
                                            <h5>Developer</h5>

                                        </div>
                                    </div>
                                    <hr />
                                    
                                    <hr />
                                    <a href="#" class="btn btn-info btn-sm">/a>&nbsp; <a href="#" class="btn btn-danger btn-sm"></a>

                                </div>
                            </li>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- LOGO HEADER END-->
        
         <section class="menu-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="navbar-collapse collapse ">
                            <ul id="menu-top" class="nav navbar-nav navbar-right">
                                
                                
                                 <li><a href="../" >Site</a></li>

                                </ul>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- MENU SECTION END-->
        
        
        <!-- MENU SECTION END-->
        <div class="content-wrapper">
            <div class="container">

<?php
if ($cadastro == TRUE) {
    ?>

                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="page-head-line">Realize o login para entra no sistema SEDASE </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">

                            <h4> Informe seu <strong>Login:</strong></h4>
                            <br />
    <?= $msgerro; ?>
                            <form method="post">
                                <label>Email: </label>
                                <input type="text" name="email" class="form-control" required="" />
                                <label>Senha:  </label>
                                <input type="password" name="senha" class="form-control" required="" />
                                <hr />
                                <input  type="submit" value="Entrar" class="btn btn-info"/>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <div class="alert alert-info">
                                Para acesso a essa área você precisa ser Aluno com o login e senha já cadastrados no Sistema. Não fez seu cadastro? Clique <a href="../index.php" target="_blank"><strong>aqui</strong></a>.
                                <br />
                                <strong> Caso não consiga realizar o login:</strong>
                                <ul>
                                    <li>
                                        Contate um Administrador do sistema pelo e-mail <strong><?=$email?></strong>.
                                    </li>
                                    <li>
                                        Recupere sua senha através do "Esqueci minha senha".
                                    </li>
                                    
                                </ul>

                            </div>
                            
                        </div>

                    </div>

    <?php
} elseif ($cadastro == FALSE) {
    ?>

                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="page-head-line"> Configuração - Cadastro de Escola (Obrigatório)</h4>
                        </div>
                    </div>
                <div id="conteudop" class="row" style="text-align: center;">
                        <div class="col">

                            <h4> Informe os dados da sua <strong>Escola:</strong></h4>
                            <br />

                            <form id="cadastroC">
                                
                                <div class="form-group"><label>Nome: </label>
                                    <input type="text" name="nome" class="form-control" required="" />
                           
                                </div> 
                              
                                <div class="form-group">
                                    <label>Código da Escola:  </label>
                                    <input type="text" name="codigo" class="form-control" required="" />

                                </div>  
                                
                                
                                <div class="form-group">
                                    <label for="estado">Estado:</label>
                                    <select name="estado" required=""> 
                                        <option value="AC">Acre</option> 
                                        <option value="AL">Alagoas</option> 
                                        <option value="AM">Amazonas</option> 
                                        <option value="AP">Amapá</option> 
                                        <option value="BA">Bahia</option> 
                                        <option value="CA">Ceará</option> 
                                        <option value="DF">Distrito Federal</option> 
                                        <option value="ES">Espírito Santo</option> 
                                        <option value="GO">Goiás</option> 
                                        <option value="MA">Maranhão</option> 
                                        <option value="MT">Mato Grosso</option> 
                                        <option value="MS">Mato Grosso do Sul</option> 
                                        <option value="MG">Minas Gerais</option> 
                                        <option value="PA">Pará</option> 
                                        <option value="PB">Paraíba</option> 
                                        <option value="PR">Paraná</option> 
                                        <option value="PE">Pernambuco</option> 
                                        <option value="PI">Piauí</option> 
                                        <option value="RJ">Rio de Janeiro</option> 
                                        <option value="RN">Rio Grande do Norte</option> 
                                        <option value="RO">Rondônia</option> 
                                        <option value="RS" selected="selected">Rio Grande do Sul</option> 
                                        <option value="RR">Roraima</option> 
                                        <option value="SC">Santa Catarina</option> 
                                        <option value="SE">Sergipe</option> 
                                        <option value="SP">São Paulo</option> 
                                        <option value="TO">Tocantins</option> 
                                    </select>

                                </div>
                                
                                <div class="form-group">

                                    <label>Cidade:  </label>
                                    <input type="text" name="cidade" class="form-control" required="" />

                                </div> 
                                
                                
                                <div class="form-group">

                                    <label>Endereço:  </label>
                                    <input type="text" name="endereco" class="form-control" required="" />
                                </div> 
                               
                                <div class="form-group">

                                    <label>Telefones:  </label>
                                    <textarea name="telefone" class="form-control" required="" ></textarea>

                                </div> 

                                <div class="form-group">

                                    <label>E-mail da Escola:  </label>
                                    <input type="email" name="email" class="form-control" required="" />

                                </div>
                                
                                <h3>O Usuário administrador será a Escola cadastrada, informe a senha para acesso:</h3>
                            
                                  <div class="form-group">

                                    <label>Senha:  </label>
                                    <input type="password" name="senha" class="form-control" required="" />

                                </div>
                                
                                <div class="form-group">

                                    <label> Confirme a senha:  </label>
                                    <input type="password" name="senha2" class="form-control" required="" />

                                </div>

                                <button type="submit" class="btn btn-success" id="carregarescola"  >Salvar</button>
                            </form>
                            
                            <div id="respostaescola"></div>
                        </div>


                    </div>

                


<script type="text/javascript">
    var jq = $.noConflict();    
    jq(document).ready(function () {
   
        jq('#cadastroC').submit(function () {
            var dados = jq(this).serialize();
            jq('#carregarescola').attr('disabled', true);
            jq("#carregarescola").html('Carregando...');
            jq.ajax({
                type: "POST",
                url: "Inserts/cadastroC.php",
                data: dados,
                success: function (data)
                {  
                    jq('#respostaescola').html(data);
                    jq('#carregarescola').attr('disabled', false);
                    jq("#carregarescola").html('Cadastrar');
                    
                    document.getElementById("conteudop").innerHTML="";
                    document.getElementById("conteudop").innerHTML="<div class='alert alert-success'> Escola cadastrada com sucesso. <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></div> <br/> <h4>Aguarde...</h4><br/><img src='assets/img/ajax-loader.gif' width='40%' title='carregando...' />";
                    window.setInterval(function() {document.location.href='index.php';}, 5000);
                }
            });

            return false;

        });

        });
        
        
        
        
        

</script>







    <?php
}
?>




            </div>
        </div>
        <!-- CONTENT-WRAPPER SECTION END-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        &copy; 2016 SEDASE | Desenvolvido por: <a href="#" >Rafael Orlando</a>
                    </div>

                </div>
            </div>
        </footer>
        <!-- FOOTER SECTION END-->
        <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
        <!-- CORE JQUERY SCRIPTS -->
        <script src="assets/js/jquery-1.11.1.js"></script>
        <!-- BOOTSTRAP SCRIPTS  -->
        <script src="assets/js/bootstrap.js"></script>
    </body>
</html>