<?php
session_start();
include_once '../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');
ini_set('display_errors', 0);
ini_set('display_startup_erros', 0);
error_reporting(E_ALL);

if (isset($_SESSION["nome"], $_SESSION["iduser"], $_SESSION["tipo"])) {
    $id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
    $nome = isset($_SESSION['nome']) ? $_SESSION['nome'] : '';
    $tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';
    $pnome=explode(" ", $nome);
    $objUsuarios = new Usuarios();
    $objUsuarios->id = $id;
    $itemuser = $objUsuarios->retornarunico();
  if($tipo>0){
    header("location:../escola/inicio.php");
    }

    $objEscola = new Escola();
$lista = $objEscola->listar();
if ($lista != null) {

    foreach ($lista as $item) {
        $escola = $item->nome;
        $codescola = $item->codigo;
        $idescola = $item->id;
        $email = $item->email;
        
    }
}

$objPertence = new Pertence();
$objPertence->idusuario=$id;
$itempertence = $objPertence->retornarunicoAluno();

$objTurma = new Turma();
$objTurma->id=$itempertence->idturma;
$itemturma = $objTurma->retornarunico();

$minhaturma="$itemturma->nome - Grau: $itemturma->grau º - Ano: $itemturma->ano º";

  
} else {
    echo "<script>window.location.href ='index.php';</script>";
}


?>
﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <![endif]-->
        <title>SEDASE - ADM</title>
        <!-- BOOTSTRAP CORE STYLE  -->
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FONT AWESOME ICONS  -->
        <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLE  -->
        <link href="assets/css/style.css" rel="stylesheet" />
        
        <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="assets/js/jquery-3.1.0.js"></script>

        <link rel="stylesheet" href="assets/css/redmond/jquery-ui-1.10.1.custom.css" />
        <script src="assets/js/jquery-ui.js" type="text/javascript"></script>
        <script src="../ckeditor/ckeditor.js"></script>
        <script src="../ckeditor/samples/js/sample.js"></script>



    </head>
    <body>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                 <strong><?= $escola; ?> </strong>&nbsp;&nbsp;&nbsp;&nbsp;
                        <strong>Email: </strong><?= $email; ?>
                        &nbsp;&nbsp;
                        <strong>Minha Turma: </strong> <?= $minhaturma; ?>
                        &nbsp;&nbsp;
                        <strong>Bem Vindo(a),  <?= $pnome[0]; ?> </strong> &nbsp;&nbsp; <a style="color: white;" data-toggle="modal" data-target="#usernow" href="#"><span class="glyphicon glyphicon-cog" title="Editar Meus dados"></span> </a>&nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp; <a style="color: white;" href="logout.php"><span class="glyphicon glyphicon-off" title="Sair"></span></a>

                    </div>

                </div>
            </div>
        </header>
        <!-- HEADER END-->
        <div class="navbar navbar-inverse set-radius-zero">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="inicio.php">

                        <img src="assets/img/logo.png" />
                    </a>

                </div>

                <div class="left-div">
                    <div class="user-settings-wrapper">
                        <ul class="nav">

                            <li class="dropdown">
                                <!--                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                                                <span class="glyphicon glyphicon-user" style="font-size: 25px;"></span>
                                                            </a>-->
                                 <div class="dropdown-menu dropdown-settings">
                                    <div class="media">
                                        <a class="media-left" href="#">
                                            <img src="assets/img/64-64.jpg" alt="" class="img-rounded" />
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"> </h4>
                                            <h5></h5>

                                        </div>
                                    </div>
                                    <hr />
                                    <h5><strong></strong></h5>
                                    
                                    <hr />
                                    <a href="#" class="btn btn-info btn-sm"></a>&nbsp; <a href="" class="btn btn-danger btn-sm"></a>

                                </div>
                            </li>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- LOGO HEADER END-->
        <section class="menu-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="navbar-collapse collapse ">
                            <ul id="menu-top" class="nav navbar-nav navbar-right">
                                <li><a <?php
                        if ($menu == 1) {
                            echo'class="menu-top-active"';
                        }
?>  href="inicio.php">Início</a></li>
                                <li><a <?php
                        if ($menu == 2) {
                            echo'class="menu-top-active"';
                        }
?>  href="materiais.php">Materiais</a></li>

                                
                                <li><a <?php
                                    if ($menu == 3) {
                                        echo'class="menu-top-active"';
                                    }
                                    ?> href="enquetes.php">Enquetes</a></li>
                                
                              
                                <li><a <?php
                                        if ($menu == 7) {
                                            echo'class="menu-top-active"';
                                        }
                                        ?>  href="forum.php">Forum</a></li>
                                
                                 <li><a href="../" target="_blank">Site</a></li>

                                </ul>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- MENU SECTION END-->

        <div class="modal fade" id="usernow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Minhas Informações</h4>
                    </div>
                    <div class="modal-body">


                        <form id="editaUnow" >
                            <div class="form-group">
                                <label for="nome">Nome:</label>
                                <input type="text" name="nome" class="form-control" id="nome" value="<?= $itemuser->nome ?>" placeholder="Digite seu Nome..." required="" />
                            </div>

                            <div class="form-group">
                                <label for="data">Nascimento:</label>
                                <input type="date" name="data" class="form-control" id="datanow" minlength="10" maxlength="10" onkeydown="Mascara(this, Data);" onkeypress="Mascara(this, Data);" onkeyup="Mascara(this, Data);" value="<?= date("d/m/Y", strtotime($itemuser->data)) ?>" placeholder="" required="" />
                            </div>

                            <div class="form-group">
                                <label for="estado">Estado:</label>
                                <select name="estado" required=""> 
                                    <option value="AC" <?php
                                            if ($itemuser->estado == "AC") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Acre</option> 
                                    <option value="AL" <?php
                                            if ($itemuser->estado == "AL") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Alagoas</option> 
                                    <option value="AM" <?php
                                            if ($itemuser->estado == "AM") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Amazonas</option> 
                                    <option value="AP" <?php
                                            if ($itemuser->estado == "AP") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Amapá</option> 
                                    <option value="BA" <?php
                                            if ($itemuser->estado == "BA") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Bahia</option> 
                                    <option value="CA" <?php
                                            if ($itemuser->estado == "CA") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Ceará</option> 
                                    <option value="DF" <?php
                                            if ($itemuser->estado == "DF") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Distrito Federal</option> 
                                    <option value="ES" <?php
                                            if ($itemuser->estado == "ES") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Espírito Santo</option> 
                                    <option value="GO" <?php
                                            if ($itemuser->estado == "GO") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Goiás</option> 
                                    <option value="MA" <?php
                                            if ($itemuser->estado == "MA") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Maranhão</option> 
                                    <option value="MT" <?php
                                            if ($itemuser->estado == "MT") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Mato Grosso</option> 
                                    <option value="MS" <?php
                                            if ($itemuser->estado == "MS") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Mato Grosso do Sul</option> 
                                    <option value="MG" <?php
                                            if ($itemuser->estado == "MG") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Minas Gerais</option> 
                                    <option value="PA" <?php
                                            if ($itemuser->estado == "PA") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Pará</option> 
                                    <option value="PB" <?php
                                            if ($itemuser->estado == "PB") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Paraíba</option> 
                                    <option value="PR" <?php
                                            if ($itemuser->estado == "PR") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Paraná</option> 
                                    <option value="PE" <?php
                                            if ($itemuser->estado == "PE") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Pernambuco</option> 
                                    <option value="PI" <?php
                                            if ($itemuser->estado == "PI") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Piauí</option> 
                                    <option value="RJ" <?php
                                            if ($itemuser->estado == "RJ") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Rio de Janeiro</option> 
                                    <option value="RN" <?php
                                            if ($itemuser->estado == "RN") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Rio Grande do Norte</option> 
                                    <option value="RO" <?php
                                            if ($itemuser->estado == "RO") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Rondônia</option> 
                                    <option value="RS" <?php
                                            if ($itemuser->estado == "RS") {
                                                echo "selected='selected'";
                                            }
                                            ?> >Rio Grande do Sul</option> 
                                    <option value="RR" <?php
                                            if ($itemuser->estado == "RR") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Roraima</option> 
                                    <option value="SC" <?php
                                            if ($itemuser->estado == "SC") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Santa Catarina</option> 
                                    <option value="SE" <?php
                                            if ($itemuser->estado == "SE") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Sergipe</option> 
                                    <option value="SP" <?php
                                            if ($itemuser->estado == "SP") {
                                                echo "selected='selected'";
                                            }
                                            ?>>São Paulo</option> 
                                    <option value="TO" <?php
                                    if ($itemuser->estado == "TO") {
                                        echo "selected='selected'";
                                    }
                                    ?>>Tocantins</option> 
                                </select>

                            </div>

                            <div class="form-group">
                                <label for="genero">Gênero:</label>
                                <select name="genero" required=""> 
                                    <option value="Feminino"  <?php
                                            if ($itemuser->genero == "Feminino") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Feminino</option> 
                                    <option value="Masculino" <?php
                                            if ($itemuser->genero == "Masculino") {
                                                echo "selected='selected'";
                                            }
                                            ?> >Masculino</option>
                                    <option value="Trans Feminino" <?php
                                            if ($itemuser->genero == "Trans Feminino") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Trans Feminino</option>
                                    <option value="Trans Masculino" <?php
                                            if ($itemuser->genero == "Trans Masculino") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Trans Masculino</option>
                                    <option value="Não Binário"  <?php
                                            if ($itemuser->genero == "Não Binário") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Não Binário</option>
                                    <option value="Outro" <?php
                                            if ($itemuser->genero == "Outro") {
                                                echo "selected='selected'";
                                            }
                                            ?>>Outro</option>
                                </select>

                            </div>
                            <div class="form-group">
                                <label for="cidade">Cidade:</label>
                                <input type="text" name="cidade" class="form-control" id="cidade" value="<?= $itemuser->cidade ?>" placeholder="Digite seu Cidade..." required="" />
                            </div>


                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" name="email" class="form-control" id="email" value="<?= $itemuser->email ?>" placeholder="Digite seu Email..." required="" />
                            </div>

                            <div class="form-group">
                                <label for="senha">Senha:</label>
                                <input type="password" name="senha" class="form-control" id="email" value="<?= base64_decode($itemuser->senha) ?>" placeholder="Digite seu Email..." required="" />
                            </div>

                            <input type="hidden" name="tipo" value="<?= $itemuser->tipo ?>" />
                            <input type="hidden" name="idusuario" value="<?= $itemuser->id ?>"/>
                            <input type="hidden" name="ativo" value="TRUE"/>
                            <div class="text-center pull-center"><button type="submit" class="btn btn-success" id="carregarnow"  >Salvar Alterações</button></div>
                        </form>
                        <div id="respostanow"></div>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">

            $('#editaUnow').submit(function () {
                var dados = $(this).serialize();
                $('#carregarnow').attr('disabled', true);
                $("#carregarnow").html('Carregando...');
                $.ajax({
                    type: "POST",
                    url: "Updates/editaUnow.php",
                    data: dados,
                    success: function (data)
                    {
                        $('#respostanow').html(data);
                        $('#carregarnow').attr('disabled', false);
                        $("#carregarnow").html('Atualizar');
                    }
                });

                return false;

            });

  /*Função Pai de Mascaras*/
        function Mascara(o,f){
                v_obj=o
                v_fun=f
                setTimeout("execmascara()",1)
        }
        
        /*Função que Executa os objetos*/
        function execmascara(){
                v_obj.value=v_fun(v_obj.value)
        }
        
        /*Função que Determina as expressões regulares dos objetos*/
        function leech(v){
                v=v.replace(/o/gi,"0")
                v=v.replace(/i/gi,"1")
                v=v.replace(/z/gi,"2")
                v=v.replace(/e/gi,"3")
                v=v.replace(/a/gi,"4")
                v=v.replace(/s/gi,"5")
                v=v.replace(/t/gi,"7")
                return v;
        }
       
             /*Função que padroniza DATA*/
        function Data(v){
                v=v.replace(/\D/g,""); 
                v=v.replace(/(\d{2})(\d)/,"$1/$2"); 
                v=v.replace(/(\d{2})(\d)/,"$1/$2"); 
                return v;
        }

 /*Função que padroniza DATA*/
        function Data(v){
                v=v.replace(/\D/g,""); 
                v=v.replace(/(\d{2})(\d)/,"$1/$2"); 
                v=v.replace(/(\d{2})(\d)/,"$1/$2"); 
                return v;
        }
        </script>