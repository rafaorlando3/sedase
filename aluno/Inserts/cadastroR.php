<?php
include_once '../../class/Carrega.class.php';
session_start();
date_default_timezone_set("Brazil/East");

if (isset($_POST["respostas"], $_POST["idforum"], $_POST["idusuario"])) {

    $respostas = htmlspecialchars_decode($_POST["respostas"]);

    $objRespostas = new Respostas();
    $objRespostas->texto = $respostas;
    $objRespostas->idforum = pg_escape_string($_POST["idforum"]);
    $objRespostas->idusuario = pg_escape_string($_POST["idusuario"]);
    $objRespostas->melhoresposta = 'f';
    $objRespostas->citacao = null;
    $objRespostas->data = date('Y-m-d H:i');
    $objRespostas->inserir();


    $objCurtirR = new CurtirR();
    $objCurtirR->contagem = 0;
    $objCurtirR->curtiu = 'f';
    $objCurtirR->data = date('Y-m-d H:i');
    $objCurtirR->idresposta = $objRespostas->retornaUltimo();
    $objCurtirR->idusuario = pg_escape_string($_POST["idusuario"]);
    $objCurtirR->inserir();


    echo "<div class='alert alert-success'> Publicado! :) Se sua resposta não aparecer nesta página, navegue nos links de paginação para encontrá-la. <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>";
}