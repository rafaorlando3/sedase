<?php
$menu = 1;
include_once 'cabecalho.php';
?>

<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Início</h4>

            </div>

        </div>

               <!-- Modal Exibe tudo -->
<div class="modal fade" id="dinamico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Atividades</h4>
            </div>
            <div class="modal-body">


                <div id="respostadinamica"></div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

            </div>


        </div>
    </div>
</div>
<!--             Fim modal...-->


        <div id="dinamicocont">


        </div> 
        


        <!--            <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                This is a simple admin template that can be used for your small project or may be large projects. This is free for personal and commercial use.
                            </div>
                        </div>
        
                    </div>
                    <div class="row">
                         <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="dashboard-div-wrapper bk-clr-one">
                                <i  class="fa fa-venus dashboard-div-icon" ></i>
                                <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
          </div>
                                   
        </div>
                                 <h5>Simple Text Here </h5>
                            </div>
                        </div>
                         <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="dashboard-div-wrapper bk-clr-two">
                                <i  class="fa fa-edit dashboard-div-icon" ></i>
                                <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
          </div>
                                   
        </div>
                                 <h5>Simple Text Here </h5>
                            </div>
                        </div>
                         <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="dashboard-div-wrapper bk-clr-three">
                                <i  class="fa fa-cogs dashboard-div-icon" ></i>
                                <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
          </div>
                                   
        </div>
                                 <h5>Simple Text Here </h5>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="dashboard-div-wrapper bk-clr-four">
                                <i  class="fa fa-bell-o dashboard-div-icon" ></i>
                                <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
          </div>
                                   
        </div>
                                 <h5>Simple Text Here </h5>
                            </div>
                        </div>
        
                    </div>
                   
                    <div class="row">
                        <div class="col-md-6">
                              <div class="notice-board">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                   Active  Notice Panel 
                                        <div class="pull-right" >
                                            <div class="dropdown">
          <button class="btn btn-success dropdown-toggle btn-xs" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
            <span class="glyphicon glyphicon-cog"></span>
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Refresh</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Logout</a></li>
          </ul>
        </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                       
                                        <ul >
                                           
                                             <li>
                                                    <a href="#">
                                             <span class="glyphicon glyphicon-align-left text-success" ></span> 
                                                          Lorem ipsum dolor sit amet ipsum dolor sit amet
                                                         <span class="label label-warning" > Just now </span>
                                                    </a>
                                            </li>
                                             <li>
                                                  <a href="#">
                                             <span class="glyphicon glyphicon-info-sign text-danger" ></span>  
                                                  Lorem ipsum dolor sit amet ipsum dolor sit amet
                                                  <span class="label label-info" > 2 min chat</span>
                                                    </a>
                                            </li>
                                             <li>
                                                  <a href="#">
                                             <span class="glyphicon glyphicon-comment  text-warning" ></span>  
                                                  Lorem ipsum dolor sit amet ipsum dolor sit amet
                                                  <span class="label label-success" >GO ! </span>
                                                    </a>
                                            </li>
                                            <li>
                                                  <a href="#">
                                             <span class="glyphicon glyphicon-edit  text-danger" ></span>  
                                                  Lorem ipsum dolor sit amet ipsum dolor sit amet
                                                  <span class="label label-success" >Let's have it </span>
                                                    </a>
                                            </li>
                                           </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="panel-footer">
                                        <a href="#" class="btn btn-default btn-block"> <i class="glyphicon glyphicon-repeat"></i> Just A Small Footer Button</a>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="text-center alert alert-warning">
                                <a href="#" class="btn btn-social btn-facebook">
                                    <i class="fa fa-facebook"></i>&nbsp; Facebook</a>
                                <a href="#" class="btn btn-social btn-google">
                                    <i class="fa fa-google-plus"></i>&nbsp; Google</a>
                                <a href="#" class="btn btn-social btn-twitter">
                                    <i class="fa fa-twitter"></i>&nbsp; Twitter </a>
                                <a href="#" class="btn btn-social btn-linkedin">
                                    <i class="fa fa-linkedin"></i>&nbsp; Linkedin </a>
                            </div>
                             
                            <hr />
                             
                        </div>
                        <div class="col-md-6">
                            <div class="alert alert-danger">
                                This is a simple admin template that can be used for your small project or may be large projects. This is free for personal and commercial use.
                            </div>
                            <hr />
                             <div class="Compose-Message">               
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                Compose New Message 
                            </div>
                            <div class="panel-body">
                                
                                <label>Enter Recipient Name : </label>
                                <input type="text" class="form-control" />
                                <label>Enter Subject :  </label>
                                <input type="text" class="form-control" />
                                <label>Enter Message : </label>
                                <textarea rows="9" class="form-control"></textarea>
                                <hr />
                                <a href="#" class="btn btn-warning"><span class="glyphicon glyphicon-envelope"></span> Send Message </a>&nbsp;
                              <a href="#" class="btn btn-success"><span class="glyphicon glyphicon-tags"></span>  Save To Drafts </a>
                            </div>
                            <div class="panel-footer text-muted">
                                <strong>Note : </strong>Please note that we track all messages so don't send any spams.
                            </div>
                        </div>
                             </div>
                        </div>
                    </div>-->
    </div>
</div>
<!-- CONTENT-WRAPPER SECTION END-->

<script type="text/javascript">

    function atualizadinamico() {
        $.get('Selects/iniciocont.php', function (resultado) {
            $('#dinamicocont').html(resultado);

        });
    }
    atualizadinamico();
    
</script>


<?php
include_once 'rodape.php';
