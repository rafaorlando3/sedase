<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');
$tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';
if (isset($_GET['idresposta'], $_GET['idusuario'])) {

    $idresposta = $_GET['idresposta'];
    $idforum = $_SESSION['idforum'];
    $idusuario = $_GET['idusuario'];

    $objRespostas = new Respostas();
    $objRespostas->id = $idresposta;
    $itemresposta = $objRespostas->retornarunico();

    $objUsuarios = new Usuarios();
    $objUsuarios->id = $idusuario;
    $itemuser = $objUsuarios->retornarunico();
}
?>


<h4 class="modal-title" id="myModalLabel">Editando a postagem de <label class='label label-success'><?= $itemuser->nome ?></label> que disse:</h4>
<p></p>        
<div class="alert alert-warning alert-dismissible fade in" role="alert"><?= "&ldquo;" . strip_tags($itemresposta->texto) . "&rdquo;" ?> <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
        <span aria-hidden='true'>&times;</span>
    </button></div>
<hr/>
<form id="editaR">
    <div class="form-group">
        <textarea name="respostas" id="editor3" class="form-control"><?= $itemresposta->texto; ?></textarea>
    </div>
    
    <?php
    if($tipo==1){
    ?>
    <div class="form-group">
        <label>Melhor resposta?</label>
        Sim <input type="radio" value="t" name="melhoresposta" <?php if($itemresposta->melhoresposta=='t'){echo "checked";}?>/> - 
        Não <input type="radio" value="f" name="melhoresposta" <?php if($itemresposta->melhoresposta=='f'){echo "checked";}?>/>
    </div>
    <?php 
    
        }else{
    
    ?>
    
    <input type="hidden" name="melhoresposta" value="<?=$itemresposta->melhoresposta;?>" />
    <?php
        }
    ?>

    <input type="hidden" name="idforum" id="idforum" value="<?= $idforum; ?>"/>
    <input type="hidden" name="idresposta" id="idresposta3" value="<?= $idresposta; ?>"/>
    <input type="hidden" name="idusuario" id="idusuario" value="<?= $idusuario; ?>"/>

    <div class="row">
        <div class=" text-center">

            <button type="button" class="btn btn-primary" id="carregarR2">Atualizar</button>

        </div>
    </div>

    <div id="respostaR2"></div>

</form>

<hr/>

<div class="row">
    <div class="col-sm-offset-5 col-sm-2 text-center">
        <button id="excluir" class="btn btn-large btn-danger" >Excluir</button>

    </div>
</div>
<p></p>
<div id="excluiresp"></div>

<form id="excluirR"><input type="hidden" name="idresposta" value="<?= $itemresposta->id ?>"></form>

<script type="text/javascript">
    CKEDITOR.replace('editor3');
    jq(document).ready(function () {

        jq('#carregarR2').click(function () {
            jq('#carregarR2').attr('disabled', true);

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            var dados = jq('#editaR').serialize();
            jq.ajax({
                type: "POST",
                url: "Updates/editaR.php",
                data: dados,
                success: function (data)
                {
                    jq("#carregarR2").html('Atualizando...');
                    jq("#carregarR2").html('Atualizar');
                    jq('#carregarR2').attr('disabled', false);
                    jq('#respostaR2').html(data);

                    atualizaedita();



                }
            });

            return false;


        });



        jq('#excluir').click(function () {
            var resposta = confirm("Você realmente deseja excluir essa resposta?");
            if (resposta == true) {
                jq('#excluir').attr('disabled', true);
                jq('#carregarR2').attr('disabled', true);
                var dados = jq('#excluirR').serialize();


                jq.ajax({
                    type: "POST",
                    url: "Delets/resposta-del.php",
                    data: dados,
                    success: function (data)
                    {
                        jq("#excluir").html('Excluindo...');
                        jq("#excluir").html('Excluído');
                        jq('#excluiresp').html(data);

                        atualizaedita();
                    }
                });
            } else {
                jq('#excluir').attr('disabled', false);
                jq("#excluir").html('Excluir');
            }

            return false;


        });



    });

    function atualizaedita() {


        jq.get('Selects/carregarespostas.php', function (resultado) {
            jq('#carregarespostas').html(resultado);

        });
    }

</script>