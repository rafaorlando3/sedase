<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');
$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
$objPertence = new Pertence();
$objPertence->idusuario = $id;
$itempertence= $objPertence->retornarunicoAluno();
?>
<p class="bg-primary text-center"><strong>Destaques Fórum</strong></p>

<?php
                    $objForum = new Forum();
                    $comp = "where idturma=$itempertence->idturma and tipo=1  order by titulo ASC LIMIT 3" ;      
                        $lista = $objForum->listar($comp);
                        if ($lista != null) {
                            $mensagem = "";
                            foreach ($lista as $item) {  
                                ?>

<div class="panel panel-primary" title="Fórum Destaques">
                <div class="panel-heading">

                    <label><?= $item->titulo ?></label> 
                    <label style='float: right;clear: both;'>

                        <?= date("d-m-Y", strtotime($item->data)) ?> às <?= date("H:i:s", strtotime($item->data)) ?>
                    </label>

                </div>
                <div class="panel-body">
                    <?= $item->texto; ?>
                    
                    <label class="text-right pull-right">

                        <form method="post" action="forumenter.php"><input type="hidden" name="idedit" value="<?= $item->id ?>"/><button type="submit" class="btn btn-success">Entrar</button></form>
                    </label>
                </div>
            </div>
          
 <?php
                            }
                        } else {
                            $mensagem = "<div class='alert alert-info'>Nenhum tópico disponível até o momento.</div>";
                        }
                        ?>   


