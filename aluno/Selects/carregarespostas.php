<?php
include_once '../../class/Carrega.class.php';
session_start();

$idforum = $_SESSION['idforum'];
$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
$tipo = isset($_SESSION['tipo']) ? $_SESSION['tipo'] : '';
$paginasessao = isset($_SESSION['pagina']) ? $_SESSION['pagina'] : '';
$citacao = "";
$objRespostas = new Respostas();
$itensppagina = 7;
$pagina = intval($paginasessao);
$numerototalbanco = $objRespostas->retornatotal($idforum);
$numppagina = ceil($numerototalbanco / $itensppagina);
$contar = 0;
?>

<?php
$complemento = "where idforum=$idforum order by melhoresposta DESC, data LIMIT $itensppagina OFFSET $pagina*$itensppagina";
$lista = $objRespostas->listar($complemento);
if ($lista != null) {
    $mensagem = "";
    $corjanela = 'class="panel panel-primary"';
    ?>

    <div class="row">
        <div class="text-right">

            <nav>
                <ul class="pagination">
                    <li>
                        <a href="?pagina=0" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <?php
                    for ($i = 0; $i < $numppagina; $i++) {
                        $estilop = "";
                        if ($pagina == $i) {
                            $estilop = 'class="active"';
                        } else {
                            $estilop = "";
                        }
                        ?>
                        <li <?= $estilop; ?>><a href="?pagina=<?= $i ?>"><?php echo $i + 1; ?></a></li>
                    <?php } ?>
                    <li>
                        <a href="?pagina=<?= $numppagina - 1 ?>" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

    <?php
    foreach ($lista as $item) {
        $objCurtirR = new CurtirR();
        $objCurtirR->idresposta = $item->id;
        $itemcurtir = $objCurtirR->retornarunico();

        $objForum = new Forum();
        $objForum->id = $idforum;
        $itemforum = $objForum->retornarunico();

        $objUsuarios = new Usuarios();
        $objUsuarios->id = $item->idusuario;
        $itemuser = $objUsuarios->retornarunico();

        if ($item->idusuario == $id || $tipo == 1) {

           if($tipo==1){
                $botaoResolvido = '<hr/> <button type="button" class="btn btn-warning" id="melhor' . $item->id . '" title="Marcar como Melhor resposta">
                            <span class="glyphicon glyphicon-ok-circle text-center" aria-hidden="true" ></span>
                            Melhor
                        </button> ';
           }
           else{
              $botaoResolvido = ""; 
           }

            if ($item->melhoresposta == 'f') {

                if ($corjanela == 'class="panel panel-default"') {

                    $botaoedicao = ' - <a href="#" id="editar' . $item->id . '" data-toggle="modal" data-target="#dinamico" style="color:black;"><span class="glyphicon glyphicon-edit" aria-hidden="true" title="Editar">Editar</span></a>';
                } else {

                    $botaoedicao = ' - <a href="#" id="editar' . $item->id . '" data-toggle="modal" data-target="#dinamico" style="color:white;"><span class="glyphicon glyphicon-edit" aria-hidden="true" title="Editar">Editar</span></a>';
                }
            } else {
                $botaoedicao = ' - <a href="#" id="editar' . $item->id . '" data-toggle="modal" data-target="#dinamico" style="color:green;"><span class="glyphicon glyphicon-edit" aria-hidden="true" title="Editar">Editar</span></a>';
            }
        } else {
            $botaoResolvido = "";
            $botaoedicao = "";
        }

        if ($item->citacao != NULL) {
            $objRespostas->id = $item->citacao;
            $retornaResposta = $objRespostas->retornarunico();
            $objUsuarios->id = $retornaResposta->idusuario;
            $retornaitemuser=$objUsuarios->retornarunico();

            $citacao = '<div class="alert alert-warning alert-dismissible fade in" role="alert" style="font-size:12px; font-style: oblique;">' . $retornaitemuser->nome . ' disse: &ldquo; ' . strip_tags($retornaResposta->texto) . ' &rdquo;</div>';
        } else {
            $citacao = "";
        }
        if ($item->melhoresposta == 't') {
            ?>

            <div class="panel panel-success" title="Melhores respostas">
                <div class="panel-heading">

                    <label><?= $itemuser->nome ?> <span class="glyphicon glyphicon-ok-sign" aria-hidden="true" title="Melhor Resposta"> </span> <?= $botaoedicao; ?></label> 
                    <label style='float: right;clear: both;'>


                        <a href="#" id="curtir<?= $item->id ?>" style="color:green;" ><span class="glyphicon glyphicon-thumbs-up col-md-2 text-center" aria-hidden="true" title="Curtir"> 
                                <span id="retorno<?= $item->id ?>"><?= $objCurtirR->retornatotal($item->id); ?></span>  </span></a>


                        <span class="glyphicon glyphicon-share-alt col-md-4 text-center" aria-hidden="true" title="Responder"> 
                            <a href="#" id="responder<?= $item->id ?>" data-toggle="modal" data-target="#dinamico" style="color:green;" > Responder </a></span>

                        <?= date("d-m-Y", strtotime($item->data)) ?> às <?= date("H:i:s", strtotime($item->data)) ?>




                    </label>

                </div>
                <div class="panel-body">


                    <?= $citacao; ?>
                    <?= $item->texto; ?>
                </div>
            </div>


            <?php
        }

        if ($item->melhoresposta == 'f') {

            if ($corjanela == 'class="panel panel-default"') {
                $cor = "black";
            } else {
                $cor = "white";
            }
            ?>



            <div <?= $corjanela; ?>>
                <div class="panel-heading">

                    <label><?= $itemuser->nome ?>  <?= $botaoedicao; ?></label> 
                    <label style='float: right;clear: both;'>


                        <a href="#" id="curtir<?= $item->id ?>" style="color:<?= $cor ?>;" ><span class="glyphicon glyphicon-thumbs-up col-md-2 text-center" aria-hidden="true" title="Curtir"> 
                                <span id="retorno<?= $item->id ?>"><?= $objCurtirR->retornatotal($item->id); ?></span>  </span></a>


                        <span class="glyphicon glyphicon-share-alt col-md-4 text-center" aria-hidden="true" title="Responder"> 
                            <a id="responder<?= $item->id ?>" href="#" data-toggle="modal" data-target="#dinamico" style="color:<?= $cor ?>;" > Responder </a></span>

                        <?= date("d-m-Y", strtotime($item->data)) ?> às <?= date("H:i:s", strtotime($item->data)) ?>




                    </label>

                </div>
                <div class="panel-body">
                    <?= $citacao; ?>

                    <?= $item->texto; ?>

                    <?= $botaoResolvido ?>

                </div>
            </div>


            <?php
        }
        ?>
        <form id="enviadados<?= $item->id ?>"><input type="hidden" name="idresposta" value="<?= $item->id ?>"><input type="hidden" name="idusuario" value="<?= $id?>"></form>
        <form id="enviadadosc<?= $item->id ?>"><input type="hidden" name="idresposta" value="<?= $item->id ?>"></form>
        <script type="text/javascript">
            jq(document).ready(function () {

                jq('#responder<?= $item->id ?>').click(function () {
                    var dados = jq('#enviadados<?= $item->id ?>').serialize();

                    mostrar();
                    function mostrar() {
                        jq.get('Selects/respondendo.php?' + dados, function (resultado) {
                            jq('#respostadinamica').html(resultado);

                        });
                    }

                });


                jq('#editar<?= $item->id ?>').click(function () {
                    var dados = jq('#enviadados<?= $item->id ?>').serialize();

                    mostrar();
                    function mostrar() {
                        jq.get('Selects/edita-resp.php?' + dados, function (resultado) {
                            jq('#respostadinamica').html(resultado);

                        });
                    }

                });


                jq('#curtir<?= $item->id ?>').click(function () {
                    var dados = jq('#enviadadosc<?= $item->id ?>').serialize();

                    jq.ajax({
                        type: "POST",
                        url: "Updates/curtirR.php",
                        data: dados,
                        success: function (data)
                        {
                            jq('#retorno<?= $item->id ?>').html(data);

                        }
                    });

                    return false;

                });


              

            });
        </script>

        <?php
        if ($corjanela == 'class="panel panel-primary"' && $item->idusuario) {
            $corjanela = 'class="panel panel-default"';
        } elseif ($corjanela == 'class="panel panel-default"') {
            $corjanela = 'class="panel panel-primary"';
        }

        $contar++;
    }
    if ($contar > 6) {
        ?>

        <div class="row">
            <div class="text-center"> 

                <div class="collapse" id="visualizarespondarodape">
                    <div class="well">

                        <form id="cadastroRR">
                            <div class="form-group">
                                <textarea name="respostas" id="editor2">
                                                                			 
                                </textarea>
                            </div>
                            <input type="hidden" name="idusuario" value="<?= $id; ?>"/>
                            <input type="hidden" name="idforum" value="<?= $idforum; ?>"/>

                            <div class="row">
                                <div class=" text-center">

                                    <button type="submit" class="btn btn-primary" id="carregarR2"  >Publicar</button>

                                </div>
                            </div>
                            <div id="resposta2"></div> 
                        </form>


                    </div>

                </div>




                <button class="btn btn-success btn-lg" type="button" data-toggle="collapse" data-target="#visualizarespondarodape" aria-expanded="false" aria-controls="mostraocultaresponda" title="Responda esse tópico clicando aqui.">
                    <span class="glyphicon glyphicon-sort" aria-hidden="true" title="Mostrar/Ocultar"></span> Responder
                </button>

            </div>
        </div>
        <?php
    }
    ?>



    <div class="row">
        <div class="text-right">

            <nav>
                <ul class="pagination">
                    <li>
                        <a href="?pagina=0" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <?php
                    for ($i = 0; $i < $numppagina; $i++) {
                        $estilop = "";
                        if ($pagina == $i) {
                            $estilop = 'class="active"';
                        } else {
                            $estilop = "";
                        }
                        ?>
                        <li <?= $estilop; ?>><a href="?pagina=<?= $i ?>"><?php echo $i + 1; ?></a></li>
                    <?php } ?>
                    <li>
                        <a href="?pagina=<?= $numppagina - 1 ?>" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>


        </div>
    </div>
    <script>
        CKEDITOR.replace('editor2');

        jq('#carregarR2').click(function () {
            jq('#carregarR2').attr('disabled', true);
            jq("#carregarR2").html('publicando...');

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            var dados = jq('#cadastroRR').serialize();
            jq.ajax({
                type: "POST",
                url: "Inserts/cadastroR.php",
                data: dados,
                success: function (data)
                {
                    jq('#respostaR2').html(data);
                    atualiza();
                    function atualiza() {
                        jq.get('Selects/carregarespostas.php', function (resultado) {
                            jq('#carregarespostas').html(resultado);

                        });
                    }

                }
            });

            return false;

        });
    </script>


    <?php
} else {
    $mensagem = "<div class='alert alert-info'>Nenhuma resposta para esse tópico. Seja o primeiro a responder! :)</div>";
}
?>   

<?= $mensagem ?>







