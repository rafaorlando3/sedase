<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');
$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
$idenquete = $_GET['idenquete'];
$objEnquetes = new Enquetes();
$objEnquetes->id = $idenquete;
$itemenquete = $objEnquetes->retornarunico();

$objRespenquetes = new Respenquetes();
$listarepenquete = $objRespenquetes->listar("where idusuario=$id and idenquete=$idenquete");
if ($listarepenquete != NULL) {
    echo "<h1 class='text-center'>Você já respondeu essa Enquete! <span style='color:green;'> <strong> =) </strong></span></h1>";
} else {
    ?>
<h3 class="text-center pull-center">Enquete: <?=$itemenquete->titulo?></h3>

<div class='alert alert-info'> <?=$itemenquete->genero?> <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
<span aria-hidden='true'>&times;</span>
</button></div>

    <form id="cadastroRE">

        <?php
        $objPerguntas = new Perguntas();
        $lista = $objPerguntas->listar("where idenquetes=" . $itemenquete->id . "");
        if ($lista != null) {
            $mensagem = "";
            $i = 1;
            $p = 1;
            foreach ($lista as $itempergunta) {
                ?>
                <p>

                <h3><?= $i . " - " . $itempergunta->titulo ?></h3>
                <input type="hidden" value="<?= $itempergunta->id ?>" name="idpergunta<?=$p?>"/>

                <blockquote>

            <?php
            $objOpcoes = new Opcoes();
            $listaopcoes = $objOpcoes->listar("where idperguntas=" . $itempergunta->id . " order by texto ASC");
            if ($lista != null) {
                foreach ($listaopcoes as $itemopcao) {
                    ?>




                            <input type="radio" value="<?= $itemopcao->id ?>" name="idopcao<?= $p ?>"  required=""/> <?= $itemopcao->texto ?>


                            </p>
                    <?php
                }
            }
            ?>

                </blockquote>

                    <?php
                    $p++;
                    $i++;
                }
            } else {
                $mensagem = "<div class='alert alert-info'>Nenhum registro cadastrado até o momento.</div>";
            }
            
            ?>  <input type="hidden" name="laco" value="<?= $i ?>"/>
                <input type="hidden" name="idenquete" value="<?= $itemenquete->id ?>"/>
                <input type="hidden" name="idusuario" value="<?= $id ?>"/>
        <div class="text-center"><button type="submit" id="carregaRE" class="btn btn-success">Enviar</button></div>
    </form>

    <script>

        jq(document).ready(function () {
            jq('#cadastroRE').submit(function () {
                var dados = jq(this).serialize();
                jq('#carregaRE').attr('disabled', true);
                jq("#carregaRE").html('Carregando...');
                jq.ajax({
                    type: "POST",
                    url: "Inserts/cadastroRE.php",
                    data: dados,
                    success: function (data)
                    {
                        jq("#carregaRE").html('Pronto!');
                        jq('#respostadinamica').html(data);
                        
                    }
                });

                return false;

            });
        });

    </script>

    <?php
}
?>