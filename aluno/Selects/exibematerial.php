<?php
  include_once '../../class/Carrega.class.php';
  $idmaterial = $_GET['idmaterial'];
  $objMaterial = new Materiais();
  $objMaterial->id = $idmaterial;
  $itematerial = $objMaterial->retornarunico();
  

?>

<h3 class="text-center"><?=$itematerial->titulo?></h3>

<div class="alert alert-info">
<strong>Descrição: </strong> <?=$itematerial->descricao?>
                               

</div>

<?php
        if($itematerial->imagem!=null){
            $imagem="<div class='col-md-14'><img src='../uploads/imagens/$itematerial->imagem' class='img-responsive text-center' /> </div> <hr/> ";
        }
        else{
            $imagem="";
        }
        echo $imagem;

   if($itematerial->video!=null){
            
              
?>

<div class='col-md-14'><div id="ytplayer"></div></div> <hr/>

<script>
  // Load the IFrame Player API code asynchronously.
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/player_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  // Replace the 'ytplayer' element with an <iframe> and
  // YouTube player after the API code downloads.
  var player;
  function onYouTubePlayerAPIReady() {
    player = new YT.Player('ytplayer', {
      height: "100%",
      width: "100%",
      videoId: '<?=$itematerial->video?>'
    });
  }
</script>


<?php

   }
   else{
       echo "";
   }

   if($itematerial->anexo!=null){
       $anexo="<h5>Esse conteúdo tem um arquivo, faça o download clicando: <strong><a href='../uploads/anexos/$itematerial->anexo' target='_blank'>Baixar</a></strong></h5>";
   }
   else{
       $anexo="";
   }
   
   echo $anexo;
?>


