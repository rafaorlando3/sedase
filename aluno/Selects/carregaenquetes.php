<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');
$id = isset($_SESSION['iduser']) ? $_SESSION['iduser'] : '';
$objPertence = new Pertence();
$objPertence->idusuario = $id;
$itempertence= $objPertence->retornarunicoAluno();

?>


<div class="table-responsive">
    <table class="table table-striped" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="text-left">
                                <b>Título</b>
                            </th>

                            <th class="text-center">
                                <b>Data</b>
                            </th>
                            
                            <th class="text-right">
                                <b>Ação</b>
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                            if ($_GET['pesquisa']) {
                    $pesquisa = $_GET['pesquisa'];
                    $comp = "where idturma=$itempertence->idturma and tipo=1 and status=1 and titulo LIKE '%$pesquisa%' order by titulo ASC" ;
                } else {
                    $pesquisa = "";
                    $comp = "where idturma=$itempertence->idturma and tipo=1 and status=1 order by titulo ASC";
                }
                        
                        
                        
                        $objEnquetes = new Enquetes();
                        $lista = $objEnquetes->listar($comp);
                        if ($lista != null) {
                            $mensagem = "";

                            foreach ($lista as $item) {
                                ?>


                                <tr>
                                    <td class="text-left"> <?= $item->titulo; ?></td>
                                    <td class="text-center"> <?= date("d/m/Y", strtotime($item->data)); ?></td>
                                    
                                    <td class="text-right"><button type="button" data-toggle="modal" data-target="#dinamico" id="mostrar<?= $item->id ?>" class="btn btn-warning">Exibir</button> </td>
                                </tr>

                                
                                <script type="text/javascript">
            jq(document).ready(function () {

                jq('#mostrar<?= $item->id ?>').click(function () {
                    mostrar();
                    function mostrar() {
                        jq.get('Selects/exibeenquete.php?idenquete=<?=$item->id?>', function (resultado) {
                            jq('#respostadinamica').html(resultado);

                        });
                    }

                });

            });
        </script>
                                

                                <?php
                            }
                        } else {
                            $mensagem = "<div class='alert alert-info'>Nenhuma enquete para você até o momento.</div>";
                        }
                        ?>   

                    </tbody>
                    <tfoot></tfoot>



                </table>
    
</div>
                <?= $mensagem ?>
