<?php
session_start();
include_once '../../class/Carrega.class.php';
date_default_timezone_set('America/Sao_Paulo');
if (isset($_GET['idresposta'], $_GET['idusuario'])) {

    $idresposta = $_GET['idresposta'];
    $idforum = $_SESSION['idforum'];
    $idusuario = $_GET['idusuario'];

    $objRespostas = new Respostas();
    $objRespostas->id = $idresposta;
    $itemresposta = $objRespostas->retornarunico();

    $objUsuarios = new Usuarios();
    $objUsuarios->id = $idusuario;
    $itemuser = $objUsuarios->retornarunico();
}
?>


<h4 class="modal-title" id="myModalLabel">Respondendo à <label class='label label-success'><?= $itemuser->nome ?></label> que disse:</h4>
<p></p>        
<div class="alert alert-warning alert-dismissible fade in" role="alert"><?= "&ldquo;" . strip_tags($itemresposta->texto) . "&rdquo;" ?> <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
        <span aria-hidden='true'>&times;</span>
    </button></div>
<hr/>
<form id="cadastroR2">
    <div class="form-group">
        <textarea name="respostas" id="editor3" class="form-control"></textarea>
    </div>

    <input type="hidden" name="idforum" id="idforum" value="<?= $idforum; ?>"/>
    <input type="hidden" name="idresposta" id="idresposta3" value="<?= $idresposta; ?>"/>
    <input type="hidden" name="idusuario" id="idusuario" value="<?= $idusuario; ?>"/>

    <div class="row">
        <div class="col-sm-offset-5 col-sm-2 text-center">

            <button type="button" class="btn btn-primary" id="carregarR2">Publicar</button>

        </div>
    </div>

    <div id="respostaR2"></div>

</form>

<script type="text/javascript">
    CKEDITOR.replace('editor3');
    jq(document).ready(function () {

        jq('#carregarR2').click(function () {
            jq('#carregarR2').attr('disabled', true);
            jq("#carregarR2").html('publicando...');

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            var dados = jq('#cadastroR2').serialize();
            jq.ajax({
                type: "POST",
                url: "Inserts/cadastroR2.php",
                data: dados,
                success: function (data)
                {
                    jq("#carregarR2").html('Publicando...');
                    jq("#carregarR2").html('Publicado');
                    jq('#respostaR2').html(data);
                    
                    atualiza();
                    
                     function atualiza() {

            
            jq.get('Selects/carregarespostas.php', function (resultado) {
                jq('#carregarespostas').html(resultado);

            });
        }
                    
                }
            });

            return false;


        });

    });
</script>