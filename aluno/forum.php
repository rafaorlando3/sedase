<?php
$menu=7;
include_once 'cabecalho.php';

?>
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Forum - Tópicos da minha Turma</h4>

            </div>

        </div>
        <div class="row">
            <div class="text-center pull-center">
            <a href="../forum/inicio.php" target="_blank">  <button type="button" class="btn btn-warning btn-lg " data-toggle="modal" >
               Ir para Fórum Público
            </button>
            </a>
            </div>
            <div class="col-md-12">

           
                <hr/>
                
 <div class="text-right pull-right"><label>Buscar aqui: </label> <input  type="text" id="busca" value=""  /> </div>

     
                 <h2>Tópicos<small> Listando</small></h2>
                <div id="listaforum">


                </div>
                
                
                
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
                               
                                function atualizaforum() {
                                    jq.get('Selects/carregaforum.php', function (resultado) {
                                        jq('#listaforum').html(resultado);

                                    });
                                }
                                var jq = $.noConflict();
                                jq(document).ready(function () {
                                    atualizaforum();
                                 

                                    jq("#busca").keypress(function () {
                                    var pesquisa = jq('#busca').val();

                                    jq.get('Selects/carregaforum.php?pesquisa=' + pesquisa, function (resultado) {
                                        jq('#listaforum').html(resultado);

                                    });

                                });

                                });
                            </script>

<?php
include_once 'rodape.php';
